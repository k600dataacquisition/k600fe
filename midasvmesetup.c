/*
 * =====================================================================================
 *
 *       Filename:  midasvmesetup.c
 *
 *    Description:  Setups VME frontend and configures odb with setup information.
 *    				1.) Finds the VME modules (CAEN) in the crate.
 *    				2.) Saves the Modules setup to odb/sql-db(psql).
 *    				3.) Allows configuration of modules via odb.
 *    				4.) is an addon for any FE.
 *
 *        Version:  1.0
 *        Created:  05/27/2013 10:37:33 AM
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <sys/time.h>

#include "midasvmesetup.h"
#include "mvmestd.h"		//Contains def. for vme ( DWORD,WORD, etc )

int qdccounter = 0;
int tdccounter = 0;
int adccounter = 0;
int sclrcounter = 0;
int fpgacounter = 0;

int i_g;
int i_counter;

void addchecker(DWORD baseaddress)
{
	if( i_g < NUM_MOD_CRATE){
		ba[i_g++] = baseaddress;
		i_counter++;
	}

}


int readBoardID(DWORD baseaddress)
{

	WORD B_MSB,B_MID,B_LSB,BID;

 	mvme_set_am(myvme, MVME_AM_A32);
	mvme_set_dmode(myvme, MVME_DMODE_D16);
	
	B_MSB = (WORD)mvme_read_value( myvme, baseaddress+0x8036 );
//	printf(" MSB : 0x%x \n",B_MSB );
	B_MID = mvme_read_value( myvme, baseaddress+0x803A );
//	printf(" MID : 0x%x \n",B_MID );
	B_LSB = mvme_read_value( myvme, baseaddress+0x803E );
//	printf(" LSB : 0x%x \n",B_LSB );
	BID = ((B_MSB & 0x00ff )<< 12) | ((B_MID & 0x00ff) << 8) | (B_LSB & 0x00ff);

	return BID;
}

int readBoardIDTDC(DWORD baseaddress)
{

	WORD B_MSB,B_MID,B_LSB,BID;

 	mvme_set_am(myvme, MVME_AM_A32);
	mvme_set_dmode(myvme, MVME_DMODE_D16);
	
	B_MSB = (WORD)mvme_read_value( myvme, baseaddress+0x4034 );
//	printf(" MSB : 0x%x \n",B_MSB );
	B_MID = mvme_read_value( myvme, baseaddress+0x4038 );
//	printf(" MID : 0x%x \n",B_MID );
	B_LSB = mvme_read_value( myvme, baseaddress+0x403C );
//	printf(" LSB : 0x%x \n",B_LSB );
	BID = ((B_MSB & 0x00ff )<< 12) | ((B_MID & 0x00ff) << 8) | (B_LSB & 0x00ff);

	return BID;
}


int clashchecker()
{

	int i,j;

	DWORD checkq,checker;

	for(i=0;i < i_counter;i++){
		checkq = ba[i];	
		for(j=0;j< i_counter;j++){
			checker = ba[j];

			//printf("checkq[%d]: 0x%X , checker[%d: 0x%X \n",i,checkq,j,checker);
			if( checkq == checker && (i!=j)){

				printf("\n Base address 0x%X (checkq)[%d] is clashing with Base Address 0x%X (checker)[%d] , i_counter [%d]  !!!!! \n",checkq,i,checker,j,i_counter);
				return 1;
			}
		}
	}

	return 0;
}


int checkforQDC(DWORD base_addy)
{
		DWORD reg = 0x1000;
     	DWORD value = 0xfffffff;
		DWORD base = base_addy+reg;
		
		value = mvme_read_value( myvme, base );
		int boardid = readBoardID(base_addy);
		if( boardid == 792 ){
		//if( ((value>>8)&0xf) == 0x9 && ((value)&0xf) == 0x4 ){
				printf(" Found QDC Module [%d]  \n",++qdccounter);
				printf(" \t BOARDID: %d \n",boardid);
				printf(" \t Base Address: 0x%08x \n",base_addy);
				printf(" \t Firmware Rev: %d.%d \n",(value>>8&0xf),(value&0xf));
				return 1;
		}
	

	return 0;
}

int checkforADC(DWORD base_addy)
{
		DWORD reg = 0x1000;
   	  	DWORD value = 0xfffffff;
		DWORD base = base_addy+reg;
		//printf("\t\t Checking ADC @ [0x%x] \n",base_addy);	
		value = mvme_read_value( myvme, base );
		int boardid = readBoardID(base_addy);
		//printf("\t\t\t returned checked reg value: 0x%x , boardid %d \n",value,boardid);
		if( boardid == 785) {
		//if( ((value>>8)&0xf) == 0x9 && ((value)&0xf) == 0x4){
				printf(" Found ADC Module [%d]  \n",++adccounter);
				printf(" \t BOARDID: %d \n",boardid);
				printf(" \t Base Address: 0x%08x \n",base_addy);
				printf(" \t Firmware Rev: %d.%d \n",(value>>8&0xf),(value&0xf));
				return 1;
		}
	

	return 0;
}


int checkforSCLR(DWORD base_addy)
{
		DWORD reg = 0x1132;
     	DWORD value = 0xfffffff;
		DWORD base = base_addy+reg;
		
		value = mvme_read_value( myvme, base );
		int boardid = readBoardID(base_addy);
		if( boardid == 830) {
		//if( ((value>>4)&0xf) == 0x0 && ((value)&0xf) == 0x4){
				printf(" Found Scalar Module [%d]  \n",++sclrcounter);
				printf(" \t BOARDID: %d \n",boardid);
				printf(" \t Base Address: 0x%08x \n",base_addy);
				printf(" \t Firmware Rev: %d.%d \n",(value>>4&0xf),(value&0xf));
				return 1;
		}
	

	return 0;
}


int checkforTDC(DWORD base_addy)
{
		//printf("\t Checking for TDC @ base[0x%x] \n",base_addy);
		DWORD reg = 0x1026;
     	DWORD value = 0xfffffff;
		DWORD base = base_addy+reg;
		//printf("\t\t TDC register Check [0x%x] \n",base);	
		value = mvme_read_value( myvme, base );
		//printf("\t\t Value returned [0x%x] \n",value );
		int boardid = readBoardIDTDC(base_addy);
		if( boardid == 1190 ){
		//if( ((value>>4)&0xf) == 0x0 && ((value)&0xf) == 0x5){
				printf(" Found TDC Module [%d]  \n",++tdccounter);
				printf(" \t BOARDID: %d \n",boardid);
				printf(" \t Base Address: 0x%08x \n",base_addy);
				printf(" \t Firmware Rev: %d.%d \n",(value>>4&0xf),(value&0xf));
				return 1;
		}

	return 0;
}

int checkforFPGA(DWORD base_addy)
{
		DWORD reg = 0x8138;
     	DWORD value = 0xfffffff;
		DWORD base = base_addy+reg;
		
		value = mvme_read_value( myvme, base );
		int boardid = readBoardID(base_addy);
		if( boardid == 1495 ){
		//if( ((value)&0xf) == 0x5){
				printf(" Found FPGA Module [%d]  \n",++fpgacounter);
				printf(" \t BOARDID: %d \n",boardid);
				printf(" \t Base Address: 0x%08x \n",base_addy);
				printf(" \t Firmware Rev: %d.%d \n",(value>>8&0xf),(value&0xf));
				return 1;
		}

	return 0;
}

int ModChecks(DWORD base)
{

	if(checkforQDC(base) ){
		addchecker(base);
		return 1;
	}

	if(checkforADC(base) ){
		addchecker(base);
		return 1;
	}
	if(checkforTDC(base) ){
		addchecker(base);
		return 1;
	}

	if(checkforSCLR(base)){
		addchecker(base);
		return 1;
	}

	if(checkforFPGA(base) ){
		addchecker(base);
		return 1;
	}


	return 0;
}










