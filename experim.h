/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Sat Nov  7 16:25:13 2015

\********************************************************************/

#define EXP_EDIT_DEFINED

typedef struct {
  BOOL      pedestals_run;
  INT       frontend_mode;
  INT       debug_adc_rb;
  INT       debug_tdc_rb;
  INT       debug_qdc_rb;
  INT       debug_vme_timers;
  char      description[256];
  BOOL      debug_run;
  char      target[32];
  char      current[32];
  char      shifter_name[32];
} EXP_EDIT;

#define EXP_EDIT_STR(_name) const char *_name[] = {\
"[.]",\
"Pedestals run = BOOL : n",\
"Frontend Mode = INT : 0",\
"Debug ADC RB = INT : 0",\
"Debug TDC RB = INT : 0",\
"Debug QDC RB = INT : 0",\
"Debug VME Timers = INT : 0",\
"description = STRING : [256] test run",\
"Debug_Run = BOOL : y",\
"Target = STRING : [32] source",\
"Current = STRING : [32] none",\
"Shifter Name = STRING : [32] Legion",\
"",\
NULL }

#ifndef EXCL_GLOBAL

#define GLOBAL_PARAM_DEFINED

typedef struct {
  float     misswires;
  float     z_x1x2;
  float     x_x1x2;
  float     min_x_wires;
  float     min_u_wires;
  float     max_u_wires;
  float     max_x_wires;
  float     lut_x1_offset;
  float     lut_x2_offset;
  float     lut_u2_offset;
  float     lut_u1_offset;
  float     x1_1st_wire_chan;
  float     x2_1st_wire_chan;
  float     u1_1st_wire_chan;
  float     u2_1st_wire_chan;
  float     x1_last_wire_chan;
  float     x2_last_wire_chan;
  float     u1_last_wire_chan;
  float     u2_last_wire_chan;
} GLOBAL_PARAM;

#define GLOBAL_PARAM_STR(_name) const char *_name[] = {\
"[.]",\
"misswires = FLOAT : 2",\
"z_x1x2 = FLOAT : 251",\
"x_x1x2 = FLOAT : 374",\
"min_x_wires = FLOAT : 3",\
"min_u_wires = FLOAT : 3",\
"max_u_wires = FLOAT : 9",\
"max_x_wires = FLOAT : 10",\
"lut_x1_offset = FLOAT : 20",\
"lut_x2_offset = FLOAT : -300",\
"lut_u2_offset = FLOAT : -80",\
"lut_u1_offset = FLOAT : -50",\
"x1_1st_wire_chan = FLOAT : 8",\
"x2_1st_wire_chan = FLOAT : 510",\
"u1_1st_wire_chan = FLOAT : 301",\
"u2_1st_wire_chan = FLOAT : 801",\
"x1_last_wire_chan = FLOAT : 208",\
"x2_last_wire_chan = FLOAT : 708",\
"u1_last_wire_chan = FLOAT : 443",\
"u2_last_wire_chan = FLOAT : 943",\
"",\
NULL }

#endif

#ifndef EXCL_GATES

#define GATES_PARAM_DEFINED

typedef struct {
  float     x1_driftt_low;
  float     x1_driftt_hi;
  float     x2_driftt_low;
  float     x2_driftt_hi;
  float     u2_driftt_low;
  float     u2_driftt_hi;
  float     u1_driftt_low;
  float     u1_driftt_hi;
  float     lowtof;
  float     hitof;
  float     lowpad1;
  float     lowpad2;
  float     hipad1;
  float     hipad2;
  float     thetafp_lo;
  float     thetafp_hi;
  float     y_lo;
  float     y_hi;
} GATES_PARAM;

#define GATES_PARAM_STR(_name) const char *_name[] = {\
"[.]",\
"x1_driftt_low = FLOAT : 6100",\
"x1_driftt_hi = FLOAT : 8100",\
"x2_driftt_low = FLOAT : 6100",\
"x2_driftt_hi = FLOAT : 8100",\
"u2_driftt_low = FLOAT : 6100",\
"u2_driftt_hi = FLOAT : 8100",\
"u1_driftt_low = FLOAT : 6100",\
"u1_driftt_hi = FLOAT : 8100",\
"lowtof = FLOAT : 2000",\
"hitof = FLOAT : 4000",\
"lowpad1 = FLOAT : 400",\
"lowpad2 = FLOAT : 200",\
"hipad1 = FLOAT : 4000",\
"hipad2 = FLOAT : 4000",\
"thetafp_lo = FLOAT : 34",\
"thetafp_hi = FLOAT : 35",\
"y_lo = FLOAT : -40",\
"y_hi = FLOAT : 40",\
"",\
NULL }

#endif

#ifndef EXCL_SIS3302

#define SIS3302_PARAM_DEFINED

typedef struct {
  struct {
    INT       clock;
    INT       lemoin1enable;
    INT       lemoin2enable;
    INT       lemoin3enable;
    INT       mcamode;
    struct {
      INT       lemoinmode;
      char      lemoin1[256];
      char      lemoin2[256];
      char      lemoin3[256];
      char      lemoin4[32];
    } lemoin;
    struct {
      INT       lemooutmode;
      char      lemoout1[256];
      char      lemoout2[256];
      char      lemoout3[256];
      char      lemoout4[32];
    } lemoout;
    char      clocktype[256];
    DWORD     baseaddress;
    DWORD     modversionmajor;
    DWORD     modversionminor;
    DWORD     modversion;
  } general;
  INT       channel;
  struct {
    struct {
      INT       peakingtime[8];
      INT       sumg[8];
      INT       decimation[8];
      INT       pretriggerdelay[4];
      INT       triggergatelength[4];
      INT       cfdmode;
      char      cfdmodetype[32];
      struct {
        INT       threshold[8];
        INT       gtmode[8];
        INT       extthreshold[8];
        INT       disabletriggerout[8];
        INT       extthresholdmode[8];
      } triggerthreshold;
      struct {
        INT       internaltrigpulselength[8];
        INT       internalgatelength[8];
        INT       internaltriggerdelay[8];
      } internalsetup;
    } triggersetup;
    struct {
      INT       internaltrigger;
      INT       externaltrigger;
      INT       adcn_1nn;
      INT       _0khz;
      INT       adcn1nn;
      INT       adcinputinvert;
      struct {
        INT       adc1invertbit;
        INT       adc1internaltriggerenable;
        INT       adc1externaltriggerenable;
        INT       adc1internalgateenable;
        INT       adc1externalgateenable;
        INT       adc1nplus1nngateenable;
        INT       adc1nminus1nngateenable;
        INT       adc1nplus1nntriggerenable;
        INT       adc1nminus1nntriggerenable;
        INT       adc1trigger50enable;
        INT       adc2invertbit;
        INT       adc2internaltriggerenable;
        INT       adc2externaltriggerenable;
        INT       adc2internalgateenable;
        INT       adc2externalgateenable;
        INT       adc2nplus1nngateenable;
        INT       adc2nminus1nngateenable;
        INT       adc2nplus1nntriggerenable;
        INT       adc2nminus1nntriggerenable;
        INT       adc2trigger50enable;
      } adc12;
      struct {
        INT       adc5invertbit;
        INT       adc5internaltriggerenable;
        INT       adc5externaltriggerenable;
        INT       adc5internalgateenable;
        INT       adc5externalgateenable;
        INT       adc5nplus1nngateenable;
        INT       adc5nminus1nngateenable;
        INT       adc5nplus1nntriggerenable;
        INT       adc5nminus1nntriggerenable;
        INT       adc5trigger50enable;
        INT       adc6invertbit;
        INT       adc6internaltriggerenable;
        INT       adc6externaltriggerenable;
        INT       adc6internalgateenable;
        INT       adc6externalgateenable;
        INT       adc6nplus1nngateenable;
        INT       adc6nminus1nngateenable;
        INT       adc6nplus1nntriggerenable;
        INT       adc6nminus1nntriggerenable;
        INT       adc6trigger50enable;
      } adc56;
      struct {
        INT       adc7invertbit;
        INT       adc7internaltriggerenable;
        INT       adc7externaltriggerenable;
        INT       adc7internalgateenable;
        INT       adc7externalgateenable;
        INT       adc7nplus1nngateenable;
        INT       adc7nminus1nngateenable;
        INT       adc7nplus1nntriggerenable;
        INT       adc7nminus1nntriggerenable;
        INT       adc7trigger50enable;
        INT       adc8invertbit;
        INT       adc8internaltriggerenable;
        INT       adc8externaltriggerenable;
        INT       adc8internalgateenable;
        INT       adc8externalgateenable;
        INT       adc8nplus1nngateenable;
        INT       adc8nminus1nngateenable;
        INT       adc8nplus1nntriggerenable;
        INT       adc8nminus1nntriggerenable;
        INT       adc8trigger50enable;
      } adc78;
      struct {
        INT       adc3invertbit;
        INT       adc3internaltriggerenable;
        INT       adc3externaltriggerenable;
        INT       adc3internalgateenable;
        INT       adc3externalgateenable;
        INT       adc3nplus1nngateenable;
        INT       adc3nminus1nngateenable;
        INT       adc3nplus1nntriggerenable;
        INT       adc3nminus1nntriggerenable;
        INT       adc3trigger50enable;
        INT       adc4invertbit;
        INT       adc4internaltriggerenable;
        INT       adc4externaltriggerenable;
        INT       adc4internalgateenable;
        INT       adc4externalgateenable;
        INT       adc4nplus1nngateenable;
        INT       adc4nminus1nngateenable;
        INT       adc4nplus1nntriggerenable;
        INT       adc4nminus1nntriggerenable;
        INT       adc4trigger50enable;
      } adc34;
    } triggerenable;
    struct {
      INT       internalgate;
      INT       externalgate;
      INT       adcn_1nn;
      INT       adcn1nn;
    } gateenable;
    struct {
      INT       tau;
      INT       gatelength;
      INT       mode;
      INT       numberofenergyvalues;
      INT       peakingtime[4];
      INT       gaptime[4];
      INT       decimation[4];
    } energysetup;
    struct {
      DWORD     adc_id[8];
    } adcheader;
  } adc;
  struct {
    INT       rawdatasamplestart;
    INT       rawdatasamplelength;
    INT       energysamplelength;
    INT       energysamplestartindex1;
    INT       energysamplestartindex2;
    INT       energysamplestartindex3;
    DWORD     dac[8];
    INT       writedac;
    INT       endaddressthreshold[4];
  } raw;
  struct {
    struct {
      INT       scannhistos;
      INT       lneprescale;
      INT       autocleardisable;
      INT       scanbank2;
      INT       multiscannpreset;
    } general;
    struct {
      INT       energymultiplier;
      INT       energydivider;
      INT       energyoffset;
      INT       histogramsize;
      INT       pileupenable;
      INT       rawdatahistos;
      INT       memorywritetest;
    } channel;
  } mca;
  INT       fir_trigger_adc_counts;
  INT       flat_time;
  INT       shift_factor;
  struct {
    INT       nofrawdatawords;
    INT       rawdatasamplelength;
    INT       energymaxindex;
    INT       nofenergywords;
    INT       maxnofevents;
    INT       event_length_lwords;
    INT       energymode;
    INT       propendaddress;
    float     dac_vout_values[8];
  } infopage;
} SIS3302_PARAM;

#define SIS3302_PARAM_STR(_name) const char *_name[] = {\
"[general]",\
"clock = INT : 7",\
"LemoIn1Enable = INT : 1",\
"LemoIn2Enable = INT : 0",\
"LemoIn3Enable = INT : 1",\
"MCAMode = INT : 0",\
"",\
"[general/LemoIn]",\
"LemoInMode = INT : 1",\
"LemoIn1 = STRING : [256] Gate",\
"LemoIn2 = STRING : [256] Timestamp Clear",\
"LemoIn3 = STRING : [256] Trigger",\
"LemoIn4 = STRING : [32] ",\
"",\
"[general/LemoOut]",\
"LemoOutmode = INT : 0",\
"LemoOut1 = STRING : [256] Trigger Lemo output",\
"LemoOut2 = STRING : [256] ADCx event sampling busy",\
"LemoOut3 = STRING : [256] ADC sample logic armed ",\
"LemoOut4 = STRING : [32] ",\
"",\
"[general]",\
"ClockType = STRING : [256] Clock Second Internal 100 MHz",\
"BaseAddress = DWORD : 268435456",\
"ModVersionMajor = DWORD : 5120",\
"ModVersionMinor = DWORD : 21",\
"ModVersion = DWORD : 13058",\
"",\
"[.]",\
"Channel = INT : 0",\
"",\
"[ADC/TriggerSetup]",\
"PeakingTime = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"SumG = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"Decimation = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"PretriggerDelay = INT[4] :",\
"[0] 510",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"TriggerGateLength = INT[4] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"CFDMode = INT : 0",\
"CFDModeType = STRING : [32] ",\
"",\
"[ADC/TriggerSetup/TriggerThreshold]",\
"Threshold = INT[8] :",\
"[0] 0",\
"[1] 20",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"GTMode = INT[8] :",\
"[0] 1",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"ExtThreshold = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"DisableTriggerOut = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"ExtThresholdMode = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"",\
"[ADC/TriggerSetup/InternalSetup]",\
"internalTrigPulseLength = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"internalGateLength = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"internalTriggerDelay = INT[8] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"",\
"[ADC/TriggerEnable]",\
"internalTrigger = INT : 0",\
"externalTrigger = INT : 1",\
"ADCN_1NN = INT : 0",\
"50khz = INT : 0",\
"ADCN1NN = INT : 0",\
"ADCInputInvert = INT : 1",\
"",\
"[ADC/TriggerEnable/ADC12]",\
"ADC1InvertBit = INT : 0",\
"ADC1InternalTriggerEnable = INT : 0",\
"ADC1ExternalTriggerEnable = INT : 0",\
"ADC1InternalGateEnable = INT : 0",\
"ADC1ExternalGateEnable = INT : 0",\
"ADC1NPlus1NNGateEnable = INT : 0",\
"ADC1NMinus1NNGateEnable = INT : 0",\
"ADC1NPlus1NNTriggerEnable = INT : 0",\
"ADC1NMinus1NNTriggerEnable = INT : 0",\
"ADC1Trigger50Enable = INT : 0",\
"ADC2InvertBit = INT : 0",\
"ADC2InternalTriggerEnable = INT : 0",\
"ADC2ExternalTriggerEnable = INT : 0",\
"ADC2InternalGateEnable = INT : 0",\
"ADC2ExternalGateEnable = INT : 0",\
"ADC2NPlus1NNGateEnable = INT : 0",\
"ADC2NMinus1NNGateEnable = INT : 0",\
"ADC2NPlus1NNTriggerEnable = INT : 0",\
"ADC2NMinus1NNTriggerEnable = INT : 0",\
"ADC2Trigger50Enable = INT : 0",\
"",\
"[ADC/TriggerEnable/ADC56]",\
"ADC5InvertBit = INT : 0",\
"ADC5InternalTriggerEnable = INT : 0",\
"ADC5ExternalTriggerEnable = INT : 0",\
"ADC5InternalGateEnable = INT : 0",\
"ADC5ExternalGateEnable = INT : 0",\
"ADC5NPlus1NNGateEnable = INT : 0",\
"ADC5NMinus1NNGateEnable = INT : 0",\
"ADC5NPlus1NNTriggerEnable = INT : 0",\
"ADC5NMinus1NNTriggerEnable = INT : 0",\
"ADC5Trigger50Enable = INT : 0",\
"ADC6InvertBit = INT : 0",\
"ADC6InternalTriggerEnable = INT : 0",\
"ADC6ExternalTriggerEnable = INT : 0",\
"ADC6InternalGateEnable = INT : 0",\
"ADC6ExternalGateEnable = INT : 0",\
"ADC6NPlus1NNGateEnable = INT : 0",\
"ADC6NMinus1NNGateEnable = INT : 0",\
"ADC6NPlus1NNTriggerEnable = INT : 0",\
"ADC6NMinus1NNTriggerEnable = INT : 0",\
"ADC6Trigger50Enable = INT : 0",\
"",\
"[ADC/TriggerEnable/ADC78]",\
"ADC7InvertBit = INT : 0",\
"ADC7InternalTriggerEnable = INT : 0",\
"ADC7ExternalTriggerEnable = INT : 0",\
"ADC7InternalGateEnable = INT : 0",\
"ADC7ExternalGateEnable = INT : 0",\
"ADC7NPlus1NNGateEnable = INT : 0",\
"ADC7NMinus1NNGateEnable = INT : 0",\
"ADC7NPlus1NNTriggerEnable = INT : 0",\
"ADC7NMinus1NNTriggerEnable = INT : 0",\
"ADC7Trigger50Enable = INT : 0",\
"ADC8InvertBit = INT : 0",\
"ADC8InternalTriggerEnable = INT : 0",\
"ADC8ExternalTriggerEnable = INT : 0",\
"ADC8InternalGateEnable = INT : 0",\
"ADC8ExternalGateEnable = INT : 0",\
"ADC8NPlus1NNGateEnable = INT : 0",\
"ADC8NMinus1NNGateEnable = INT : 0",\
"ADC8NPlus1NNTriggerEnable = INT : 0",\
"ADC8NMinus1NNTriggerEnable = INT : 0",\
"ADC8Trigger50Enable = INT : 0",\
"",\
"[ADC/TriggerEnable/ADC34]",\
"ADC3InvertBit = INT : 0",\
"ADC3InternalTriggerEnable = INT : 0",\
"ADC3ExternalTriggerEnable = INT : 0",\
"ADC3InternalGateEnable = INT : 0",\
"ADC3ExternalGateEnable = INT : 0",\
"ADC3NPlus1NNGateEnable = INT : 0",\
"ADC3NMinus1NNGateEnable = INT : 0",\
"ADC3NPlus1NNTriggerEnable = INT : 0",\
"ADC3NMinus1NNTriggerEnable = INT : 0",\
"ADC3Trigger50Enable = INT : 0",\
"ADC4InvertBit = INT : 0",\
"ADC4InternalTriggerEnable = INT : 0",\
"ADC4ExternalTriggerEnable = INT : 0",\
"ADC4InternalGateEnable = INT : 0",\
"ADC4ExternalGateEnable = INT : 0",\
"ADC4NPlus1NNGateEnable = INT : 0",\
"ADC4NMinus1NNGateEnable = INT : 0",\
"ADC4NPlus1NNTriggerEnable = INT : 0",\
"ADC4NMinus1NNTriggerEnable = INT : 0",\
"ADC4Trigger50Enable = INT : 0",\
"",\
"[ADC/GateEnable]",\
"internalGate = INT : 0",\
"externalGate = INT : 0",\
"ADCN_1NN = INT : 0",\
"ADCN1NN = INT : 0",\
"",\
"[ADC/EnergySetup]",\
"Tau = INT : 0",\
"GateLength = INT : 800",\
"Mode = INT : 4",\
"NumberOfEnergyValues = INT : 0",\
"PeakingTime = INT[4] :",\
"[0] 600",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"GapTime = INT[4] :",\
"[0] 100",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"Decimation = INT[4] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"",\
"[ADC/ADCHeader]",\
"ADC_ID = DWORD[8] :",\
"[0] 2902458368",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"",\
"[RAW]",\
"RawDataSampleStart = INT : 0",\
"RawDataSampleLength = INT : 0",\
"EnergySampleLength = INT : 0",\
"EnergySampleStartIndex1 = INT : 0",\
"EnergySampleStartIndex2 = INT : 0",\
"EnergySampleStartIndex3 = INT : 0",\
"dac = DWORD[8] :",\
"[0] 38144",\
"[1] 38144",\
"[2] 38144",\
"[3] 38144",\
"[4] 38144",\
"[5] 38144",\
"[6] 38144",\
"[7] 38144",\
"writeDAC = INT : 0",\
"EndAddressThreshold = INT[4] :",\
"[0] 30",\
"[1] 70",\
"[2] 70",\
"[3] 70",\
"",\
"[MCA/General]",\
"ScanNHistos = INT : 0",\
"LNEPrescale = INT : 0",\
"AutoclearDisable = INT : 0",\
"ScanBank2 = INT : 0",\
"MultiscanNPreset = INT : 0",\
"",\
"[MCA/Channel]",\
"EnergyMultiplier = INT : 0",\
"EnergyDivider = INT : 0",\
"EnergyOffset = INT : 0",\
"HistogramSize = INT : 0",\
"PileupEnable = INT : 0",\
"RawDataHistos = INT : 0",\
"MemoryWriteTest = INT : 0",\
"",\
"[.]",\
"Fir Trigger ADC Counts = INT : 0",\
"Flat Time = INT : 0",\
"Shift Factor = INT : 16",\
"",\
"[InfoPage]",\
"nofRawDataWords = INT : 0",\
"RawDataSampleLength = INT : 0",\
"EnergyMaxIndex = INT : 2",\
"nofEnergyWords = INT : 0",\
"maxnofEvents = INT : 600",\
"event_length_lwords = INT : 6",\
"EnergyMode = INT : 4",\
"PropEndAddress = INT : 3600",\
"DAC_VOUT_VALUES = FLOAT[8] :",\
"[0] 1.640625",\
"[1] 1.640625",\
"[2] 1.640625",\
"[3] 1.640625",\
"[4] 1.640625",\
"[5] 1.640625",\
"[6] 1.640625",\
"[7] 1.640625",\
"",\
NULL }

#endif

#ifndef EXCL_WIRECHAMBER

#define WIRECHAMBER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
} WIRECHAMBER_COMMON;

#define WIRECHAMBER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] K600SYSTEMS",\
"Type = INT : 2",\
"Source = INT : 16777215",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 50",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] k600vme1",\
"Frontend name = STRING : [32] k600fevme",\
"Frontend file name = STRING : [256] k600fevme.c",\
"Status = STRING : [256] k600fevme@k600vme1",\
"Status color = STRING : [32] #00FF00",\
"",\
NULL }

#define WIRECHAMBER_SETTINGS_DEFINED

typedef struct {
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_0;
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_1;
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_2;
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_3;
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_4;
  struct {
    INT       thresholds[16];
  } v792n_0;
  struct {
    struct {
      float     adc_threshold;
      float     misswires;
      float     max_tdc_channels;
      float     z_x1x2;
      float     x_x1x2;
      float     min_x_wires;
      float     min_u_wires;
      float     max_x_wires;
      float     max_u_wires;
      float     lut_x1_offset;
      float     lut_u1_offset;
      float     lut_x2_offset;
      float     lut_u2_offset;
      float     x1_1st_wire_chan;
      float     x1_last_wire_chan;
      float     x2_1st_wire_chan;
      float     x2_last_wire_chan;
      float     u1_1st_wire_chan;
      float     u1_last_wire_chan;
      float     u2_1st_wire_chan;
      float     u2_last_wire_chan;
    } global;
    struct {
      float     x1_driftt_low;
      float     x1_driftt_hi;
      float     x2_driftt_low;
      float     u1_driftt_low;
      float     u2_driftt_low;
      float     x2_driftt_hi;
      float     u2_driftt_hi;
      float     u1_driftt_hi;
      float     lowtof;
      float     hitof;
      float     lowpad1;
      float     lowpad2;
      float     hipad1;
      float     hipad2;
    } focalplane;
  } chambers;
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_6;
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_5;
  struct {
    INT       thresholds[32];
    INT       adccalib[136];
  } v785;
  struct {
    INT       thresholds[32];
    INT       thresholdsn[16];
  } v785thres;
  INT       numtdc;
} WIRECHAMBER_SETTINGS;

#define WIRECHAMBER_SETTINGS_STR(_name) const char *_name[] = {\
"[V1190A_0]",\
"BaseAddress = DWORD : 262144",\
"ModuleType = STRING : [7] 7",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[V1190A_1]",\
"BaseAddress = DWORD : 327680",\
"ModuleType = STRING : [7] 7",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[V1190A_2]",\
"BaseAddress = DWORD : 393216",\
"ModuleType = STRING : [7] 7",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[V1190A_3]",\
"BaseAddress = DWORD : 458752",\
"ModuleType = STRING : [7] 7",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[V1190A_4]",\
"BaseAddress = DWORD : 524288",\
"ModuleType = STRING : [7] 7",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[V792N_0]",\
"Thresholds = INT[16] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"",\
"[chambers/GLOBAL]",\
"ADC Threshold = FLOAT : 0",\
"misswires = FLOAT : 0",\
"max_tdc_channels = FLOAT : 0",\
"z_x1x2 = FLOAT : 0",\
"x_x1x2 = FLOAT : 0",\
"min_x_wires = FLOAT : 0",\
"min_u_wires = FLOAT : 0",\
"max_x_wires = FLOAT : 0",\
"max_u_wires = FLOAT : 0",\
"lut_x1_offset = FLOAT : 0",\
"lut_u1_offset = FLOAT : 0",\
"lut_x2_offset = FLOAT : 0",\
"lut_u2_offset = FLOAT : 0",\
"x1_1st_wire_chan = FLOAT : 0",\
"x1_last_wire_chan = FLOAT : 0",\
"x2_1st_wire_chan = FLOAT : 0",\
"x2_last_wire_chan = FLOAT : 0",\
"u1_1st_wire_chan = FLOAT : 0",\
"u1_last_wire_chan = FLOAT : 0",\
"u2_1st_wire_chan = FLOAT : 0",\
"u2_last_wire_chan = FLOAT : 0",\
"",\
"[chambers/focalplane]",\
"x1_driftt_low = FLOAT : 0",\
"x1_driftt_hi = FLOAT : 0",\
"x2_driftt_low = FLOAT : 0",\
"u1_driftt_low = FLOAT : 0",\
"u2_driftt_low = FLOAT : 0",\
"x2_driftt_hi = FLOAT : 0",\
"u2_driftt_hi = FLOAT : 0",\
"u1_driftt_hi = FLOAT : 0",\
"lowtof = FLOAT : 0",\
"hitof = FLOAT : 0",\
"lowpad1 = FLOAT : 0",\
"lowpad2 = FLOAT : 0",\
"hipad1 = FLOAT : 0",\
"hipad2 = FLOAT : 0",\
"",\
"[V1190A_6]",\
"BaseAddress = DWORD : 655360",\
"ModuleType = STRING : [7] ",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[V1190A_5]",\
"BaseAddress = DWORD : 589824",\
"ModuleType = STRING : [7] 7",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[v785]",\
"Thresholds = INT[32] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"Adccalib = INT[136] :",\
"[0] 98",\
"[1] 85",\
"[2] 108",\
"[3] 80",\
"[4] 136",\
"[5] 110",\
"[6] 106",\
"[7] 102",\
"[8] 134",\
"[9] 98",\
"[10] 110",\
"[11] 124",\
"[12] 120",\
"[13] 93",\
"[14] 120",\
"[15] 91",\
"[16] 118",\
"[17] 124",\
"[18] 115",\
"[19] 119",\
"[20] 112",\
"[21] 114",\
"[22] 146",\
"[23] 119",\
"[24] 129",\
"[25] 105",\
"[26] 122",\
"[27] 119",\
"[28] 104",\
"[29] 103",\
"[30] 113",\
"[31] 106",\
"[32] 119",\
"[33] 136",\
"[34] 121",\
"[35] 123",\
"[36] 111",\
"[37] 117",\
"[38] 132",\
"[39] 112",\
"[40] 134",\
"[41] 128",\
"[42] 134",\
"[43] 138",\
"[44] 126",\
"[45] 87",\
"[46] 141",\
"[47] 118",\
"[48] 119",\
"[49] 111",\
"[50] 131",\
"[51] 109",\
"[52] 115",\
"[53] 96",\
"[54] 120",\
"[55] 113",\
"[56] 135",\
"[57] 132",\
"[58] 133",\
"[59] 143",\
"[60] 125",\
"[61] 127",\
"[62] 120",\
"[63] 116",\
"[64] 126",\
"[65] 125",\
"[66] 111",\
"[67] 122",\
"[68] 119",\
"[69] 108",\
"[70] 148",\
"[71] 118",\
"[72] 120",\
"[73] 125",\
"[74] 127",\
"[75] 122",\
"[76] 137",\
"[77] 128",\
"[78] 111",\
"[79] 153",\
"[80] 168",\
"[81] 152",\
"[82] 128",\
"[83] 117",\
"[84] 191",\
"[85] 164",\
"[86] 143",\
"[87] 143",\
"[88] 122",\
"[89] 138",\
"[90] 125",\
"[91] 129",\
"[92] 137",\
"[93] 142",\
"[94] 178",\
"[95] 138",\
"[96] 109",\
"[97] 122",\
"[98] 112",\
"[99] 114",\
"[100] 137",\
"[101] 119",\
"[102] 119",\
"[103] 104",\
"[104] 133",\
"[105] 108",\
"[106] 127",\
"[107] 110",\
"[108] 160",\
"[109] 109",\
"[110] 138",\
"[111] 111",\
"[112] 144",\
"[113] 87",\
"[114] 144",\
"[115] 112",\
"[116] 129",\
"[117] 117",\
"[118] 137",\
"[119] 102",\
"[120] 132",\
"[121] 109",\
"[122] 139",\
"[123] 125",\
"[124] 126",\
"[125] 112",\
"[126] 145",\
"[127] 0",\
"[128] 0",\
"[129] 0",\
"[130] 0",\
"[131] 0",\
"[132] 0",\
"[133] 0",\
"[134] 0",\
"[135] 0",\
"",\
"[V785Thres]",\
"thresholds = INT[32] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"thresholdsN = INT[16] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"",\
"[.]",\
"NumTDC = INT : 0",\
"",\
NULL }

#endif

#ifndef EXCL_BEAMLINE

#define BEAMLINE_EVENT_DEFINED

typedef struct {
  float     demand[50];
  float     measured[50];
} BEAMLINE_EVENT;

#define BEAMLINE_EVENT_STR(_name) const char *_name[] = {\
"[.]",\
"Demand = FLOAT[50] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"Measured = FLOAT[50] :",\
"[0] 255",\
"[1] -0",\
"[2] 0",\
"[3] 0",\
"[4] -0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 5",\
"[23] 0.39",\
"[24] 20.2",\
"[25] 0.1",\
"[26] 14.5",\
"[27] -2.9",\
"[28] 8.60425",\
"[29] 0.3",\
"[30] 9.25475",\
"[31] -1.5",\
"[32] 11.01",\
"[33] 0.4",\
"[34] 10.3575",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 21.93",\
"[39] 0",\
"[40] 23.94",\
"[41] 22.1",\
"[42] 19.55",\
"[43] 18.19",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 3",\
"",\
NULL }

#define BEAMLINE_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} BEAMLINE_COMMON;

#define BEAMLINE_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] FIXED",\
"Enabled = BOOL : y",\
"Read on = INT : 121",\
"Period = INT : 5000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] xiafe",\
"Frontend name = STRING : [32] k600Epics",\
"Frontend file name = STRING : [256] frontend.c",\
"",\
NULL }

#define BEAMLINE_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      char      channel_name[50][32];
    } beamline;
  } devices;
  char      names[50][32];
  float     update_threshold_measured[50];
} BEAMLINE_SETTINGS;

#define BEAMLINE_SETTINGS_STR(_name) const char *_name[] = {\
"[Devices/Beamline]",\
"Channel name = STRING[50] :",\
"[32] VarTable:FC10J:RefValue",\
"[32] VarTable:quad01sp:RefValue",\
"[32] VarTable:bmag01sp:RefValue",\
"[32] VarTable:bmag02sp:RefValue",\
"[32] VarTable:trim01sp:RefValue",\
"[32] VarTable:trim02sp:RefValue",\
"[32] VarTable:quad01si:RefValue",\
"[32] VarTable:quad02si:RefValue",\
"[32] VarTable:quad03si:RefValue",\
"[32] VarTable:quad04si:RefValue",\
"[32] VarTable:quad05si:RefValue",\
"[32] VarTable:quad06si:RefValue",\
"[32] VarTable:b_mag01pi:RefValue",\
"[32] VarTable:b_mag03pi:RefValue",\
"[32] VarTable:s_lx09xg:RefValue",\
"[32] VarTable:s_lx09xp:RefValue",\
"[32] VarTable:s_ly09xg:RefValue",\
"[32] VarTable:s_ly09xp:RefValue",\
"[32] VarTable:s_lx12xg:RefValue",\
"[32] VarTable:s_lx12xp:RefValue",\
"[32] VarTable:s_ly12xg:RefValue",\
"[32] VarTable:s_ly12xp:RefValue",\
"[32] VarTable:slx01pg:RefValue",\
"[32] VarTable:slx01pp:RefValue",\
"[32] VarTable:slx02pg:RefValue",\
"[32] VarTable:slx02pp:RefValue",\
"[32] VarTable:sly02pg:RefValue",\
"[32] VarTable:sly02pp:RefValue",\
"[32] VarTable:slx10pg:RefValue",\
"[32] VarTable:slx10pp:RefValue",\
"[32] VarTable:sly10pg:RefValue",\
"[32] VarTable:sly10pp:RefValue",\
"[32] VarTable:slx02sg:RefValue",\
"[32] VarTable:slx02sp:RefValue",\
"[32] VarTable:sly02sg:RefValue",\
"[32] VarTable:sly02sp:RefValue",\
"[32] VarTable:hallp01",\
"[32] VarTable:hallp02:",\
"[32] VarTable:hallp03:ActValue",\
"[32] VarTable:hallp04:ActValue",\
"[32] VarTable:hallp05:ActValue",\
"[32] VarTable:hallp06:ActValue",\
"[32] VarTable:hallp07:ActValue",\
"[32] VarTable:hallp08:ActValue",\
"[32] VarTable:hallp09:ActValue",\
"[32] VarTable:hallp01:RefValue",\
"[32] VarTable:hallp01:RefValue",\
"[32] VarTable:hallp01:RefValue",\
"[32] VarTable:hallp01:RefValue",\
"[32] Watchdog",\
"",\
"[.]",\
"Names = STRING[50] :",\
"[32] FC10J",\
"[32] K600 quad",\
"[32] K600 D1",\
"[32] K600 D2",\
"[32] K600 H coil",\
"[32] K600 K coil",\
"[32] Q1S",\
"[32] Q2S",\
"[32] Q3S",\
"[32] Q4S",\
"[32] Q5S",\
"[32] Q6S",\
"[32] BM1P",\
"[32] BM3P",\
"[32] slit 9X: X gap",\
"[32] slit 9X: X position",\
"[32] slit 9X: Y gap",\
"[32] slit 9X: Y position",\
"[32] slit 12X: X gap",\
"[32] slit 12X: X position",\
"[32] slit 12X: Y gap",\
"[32] slit 12X: Y position",\
"[32] slit 1P: X gap",\
"[32] slit 1P: X position",\
"[32] slit 2/3P: X gap",\
"[32] slit 2/3P: X position",\
"[32] slit 2/3P: Y gap",\
"[32] slit 2/3P: Y position",\
"[32] slit 10P: X gap",\
"[32] slit 10P: X position",\
"[32] slit 10P: Y gap",\
"[32] slit 10P: Y position",\
"[32] slit 2S: X gap",\
"[32] slit 2S: X position",\
"[32] slit 2S: Y gap",\
"[32] slit 2S: Y position",\
"[32] Hall probe: B1P",\
"[32] Hall probe: B3P",\
"[32] Hall probe: Q1S",\
"[32] Hall probe: Q2S",\
"[32] Hall probe: Q3S",\
"[32] Hall probe: Q4S",\
"[32] Hall probe: Q5S",\
"[32] Hall probe: Q6S",\
"[32] spare channel",\
"[32] spare channel",\
"[32] spare channel",\
"[32] spare channel",\
"[32] spare channel",\
"[32] Watchdog",\
"Update Threshold Measured = FLOAT[50] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 1",\
"[15] 1",\
"[16] 1",\
"[17] 1",\
"[18] 1",\
"[19] 1",\
"[20] 1",\
"[21] 1",\
"[22] 1",\
"[23] 1",\
"[24] 1",\
"[25] 1",\
"[26] 1",\
"[27] 1",\
"[28] 1",\
"[29] 1",\
"[30] 1",\
"[31] 1",\
"[32] 1",\
"[33] 1",\
"[34] 1",\
"[35] 1",\
"[36] 1",\
"[37] 1",\
"[38] 1",\
"[39] 1",\
"[40] 1",\
"[41] 1",\
"[42] 1",\
"[43] 1",\
"[44] 1",\
"[45] 1",\
"[46] 1",\
"[47] 1",\
"[48] 1",\
"[49] 1",\
"",\
NULL }

#endif

#ifndef EXCL_TRIGGER

#define TRIGGER_EVENT_DEFINED

typedef struct {
  DWORD     adc0;
  DWORD     tdc0;
  DWORD     pat0;
} TRIGGER_EVENT;

#define TRIGGER_EVENT_STR(_name) const char *_name[] = {\
"[.]",\
"ADC0 = DWORD : 0",\
"TDC0 = DWORD : 0",\
"PAT0 = DWORD : 0",\
"",\
NULL }

#define TRIGGER_COMMON_DEFINED

typedef struct {
  char      frontend_name[256];
  WORD      event_id;
  WORD      trigger_mask;
  char      format[80];
  INT       log_history;
  INT       type;
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Frontend name = STRING : [256] k600Epics",\
"event ID = WORD : 0",\
"Trigger mask = WORD : 0",\
"Format = STRING : [80] FIXED",\
"Log history = INT : 1",\
"Type = INT : 16",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] K600SYSTEMS",\
"Type = INT : 33",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 377",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] k600vme1",\
"Frontend name = STRING : [32] k600fevme",\
"Frontend file name = STRING : [256] k600fevme.c",\
"Status = STRING : [256] k600fevme@k600vme1",\
"Status color = STRING : [32] #00FF00",\
"",\
NULL }

#endif

