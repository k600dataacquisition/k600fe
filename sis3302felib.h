/*
 * =====================================================================================
 *
 *       Filename:  sis3302felib.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  27/10/2015 11:22:08
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */

#ifndef SIS3302FELIB_H
#define SIS3302FELIB_H

#include "midas.h"


DWORD nof_raw_data_words;
DWORD nof_energy_words;
DWORD max_nof_events;
DWORD event_length_lwords;
DWORD event_sample_start_addr;
DWORD event_sample_length;
DWORD energy_max_index;
DWORD gshift_factor;

DWORD gDAC[8];
//Acquisition Control
struct ACQUISITIONCONTROL {
	WORD MCAMode : 1;
	WORD LemoIn1Enable : 1; //Enable LEMO 1
	WORD LemoIn2Enable : 1; //Enable LEMO 2
	WORD LemoIn3Enable : 1; //Enable LEMO 3
	WORD ClockTypeSet : 3;
	WORD LemoOutMode : 2;
	WORD LemoInMode : 3;
	WORD EnableTriggerFeedback : 1;
}gAcquisitionControl;

//Event Configuration Register
//Event Config for Groups: 12, 34, 56, 78
DWORD gADCHeaderID[8];
struct EVENTCONFIGURATION {
	WORD ADC1InputInvert : 1;
	WORD ADC1InternalTriggerEnable : 1;
	WORD ADC1ExternalTriggerEnable : 1;
	WORD ADC1InternalGateEnable : 1;
	WORD ADC1ExternalGateEnable : 1;
	WORD ADC1NPlus1NNGateEnable : 1;
	WORD ADC1NMinus1NNGateEnable : 1;
	WORD ADC2InputInvert : 1;
	WORD ADC2InternalTriggerEnable : 1;
	WORD ADC2ExternalTriggerEnable : 1;
	WORD ADC2InternalGateEnable : 1;
	WORD ADC2ExternalGateEnable : 1;
	WORD ADC2NPlus1NNGateEnable : 1;
	WORD ADC2NMinus1NNGateEnable : 1;
}gEventConfiguration[5];

//Extended Event Configuration
//Groups: 12,34,56,78
struct EXTEVENTCONFIG {
	WORD ADC1NPlus1NNTriggerEnable : 1;
	WORD ADC1NMinus1NNTriggerEnable : 1;
	WORD ADC2NPlus1NNTriggerEnable : 1;
	WORD ADC2NMinus1NNTriggerEnable : 1;
	WORD ADC1Trigger50Enable : 1;
	WORD ADC2Trigger50Enable : 1;
}gExtEventConfig[5];

//EndAddressThreshold
DWORD gEndAddressThreshold[4];  // 2 * samples

//PreTrigger Delay and Trigger Gate Length
struct TRIGGERGATEPRETRIGGER {
	WORD pretriggerDelay;
	WORD triggerGateLength;
}gTriggerGatePretrigger[4];

//RawDataBuffer Configuration
struct RAWDATABUFFERCONFIG {
	DWORD SampleStartIndex;
	DWORD SampleLength;
}gRawDataBufferConfig[5];

//TriggerSetup and  
//ExtendedTriggerSetup
struct TRIGGERSETUP {
	WORD PeakingTime : 9; //9-bit word max 511 clocks
	WORD SumGapTime : 9;  //9-bit word max 511 clocks
	WORD InternalGateLength : 6; //max 63 clcoks
	WORD InternalTriggerDelay : 5; //max 31 clocks
	WORD InternalTriggerPulseLength : 8; //max 255 clocks
	WORD TriggerDecimationMode : 3; //5 decimation Modes + 3 reserved
}gTriggerSetup[8];

//TriggerThreshold and
//Extended Trigger Threshold
struct TRIGGERTHRESHOLD {
	DWORD FIRTrapThreshold; //17-bit word
	DWORD FIRExtTrapThreshold; //26-bit word
	WORD FIRDisableTrigOut : 1;
	WORD FIRTriggerModeGT : 1;
	WORD FIRExtThresholdMode : 1;
}gTriggerThreshold[8];

/* Energy Registers */

//EnergySetup
// ADC groups, 12,34,56,78
struct ENERGYSETUP {
WORD FIREnergyGap : 8; //8-bits
WORD FIREnergyPeak : 10; //10-bits
WORD FIREnergyDecimation : 2; //2-bits = 4 decimation modes
}gEnergySetup[5];

//EnergyReadout
struct ENERGYREADOUT {
	DWORD GateLength; //17-bit word
	WORD TestMode : 2; //2-bits = 4 testing modes
}gEnergyReadOut[5];

//Energy Tau Factor
DWORD gFIRTauFactor[8];

//SampleRegisters
//Groups: 12, 34, 56, 78
struct ENERGYSAMPLE {
	WORD EnergySampleLength : 11; //first 11-bits, even values, max 510
	WORD EnergySampleStartIndex1;
	WORD EnergySampleStartIndex2;
	WORD EnergySampleStartIndex3;
	WORD EnergySampleMode;
}gEnergySample[5];

INT gEnergySampleMode;

/* MCA Registers */
//MCA Energy_to_Histogram Calculation Parameters
struct MCAENERGYTOHISTO {
	DWORD MCAEnergyOffset;  		//20-bits
	WORD MCAEnergyMultiplier; 		//8-bits
	WORD MCAEnergyNDivider : 4; 	//4-bits
}gMCAEnergytoHisto[8];

//MCA Histrogram Parameter
struct MCAHISTOPARAMETER{
	WORD PileUpEnable : 1;
	WORD ADC2468HistoEnable : 1;
	WORD ADC1357HistoEnable : 1;
	WORD MemoryWriteTestmode : 1;
	WORD HistogramSize : 2;
}gMCAHistoParameter[4];

//MCA Scan nof Hisograms preset Register
DWORD gMCAScanNofHistos;

//MCA Scan LNE Setup and Prescale Factor
struct MCALNEPRESCALESETUP {
DWORD PrescaleFactor; // 28-bits
WORD LNESource : 1; //Source bit (0|1)
}gMCALNEPrescaleSetup;

//MCA Scan Control Register
struct MCASCANCONTROL {
	WORD StartScanBank2 : 1;
	WORD ScanHistoAutoClearDisable : 1;
}gMCAScanControl;

#endif /* SIS3302FELIB_H */

