#!/bin/bash
#===============================================================================
#
#          FILE:  debugstart.sh
# 
#         USAGE:  ./debugstart.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  06/03/2010 10:05:48 AM SAST
#      REVISION:  ---
#===============================================================================

FRONTEND="./myfrontend -h concurrentvme2.tlabs.ac.za -e k600test"
VALGRINDCOMMAND="valgrind -v --leak-check=yes --show-reachable=yes"
COMAND= "$VALGRIND $FRONTEND"
exec COMAND

