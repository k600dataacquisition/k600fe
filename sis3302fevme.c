/*
 * =====================================================================================
 *
 *       Filename:  sis3302fevme.c
 *
 *    Description: This library holds all the vme read/dma for reading the sis3302 module. 
 *
 *        Version:  1.0
 *        Created:  27/10/2015 12:52:11
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>
#include <byteswap.h>
#include <time.h>

/* Midas includes */
#include "midas.h"
#include "mcstd.h"
#include "experim.h"
#include "msystem.h"

#include "sis3320drv.h"
//#include "sis3302gamma.h"
#include "sis3302_v1411.h"
#include "sis3302fevme.h"
#include "fesis3302.h"
/* 
 *
 * Functions
 *
 *
 */

#undef DEBUG_SIS_VME
#undef DEBUG_SIS_HEADER
#undef DEBUG_SIS_TRAILER

#undef  DEBUG_SIS_BANKSWAP

INT isSIS3302ErrorWord( DWORD *ptr )
{
	DWORD a=(*ptr);

	if( (a&0xff) == 0x14 ){
		return 1;
	}
	return 0;
}

/*  is the word a SIS header word  */
INT isSIS3302Header( DWORD *ptr , int sis_channel)
{
	DWORD a=(*ptr);
	DWORD header = (a&0xffff);
	DWORD check_header = (0xad00 + sis_channel);

	//DWORD header = (a&0xff00);
	//DWORD check_header = (0xad00);


//	cm_msg(MINFO,"isSIS3302Header","CH[%d] , check header: 0x%08x, Header: 0x%x, a-word 0x%x",sis_channel,check_header,header,a);
	//if( header == 0xad00 ){
	if( header == check_header ){
#ifdef DEBUG_SIS_HEADER
		cm_msg(MINFO,"isSIS3302Header"," =============== FOUND header word: 0x%08x \n",a);
#endif	

//	cm_msg(MINFO,"isSIS3302Header","CH[%d] , check header: 0x%08x, Header: 0x%x, a-word 0x%x",sis_channel,check_header,header,a);
		return 1;
	}
	return 0;
}


/*  is the word a QDC Trailer word */
INT isSIS3302Trailer( DWORD *ptr )
{
	DWORD a=(*ptr);

	if( a == 0xdeadbeef){
#ifdef DEBUG_SIS_TRAILER
		cm_msg(MINFO,"isSIS3302Trailer"," =============== FOUND trailer word: 0x%08x \n",a);
#endif		
		return 1;
	}

	return 0;
}



INT WaitDataReady( INT cnt , INT test )
{
	DWORD armed=0x0;
	DWORD status_threshold_flags;
	DWORD stat_thres_adc1_flag ;
	DWORD stat_thres_adc2_flag ;
	DWORD stat_thres_adc3_flag ;
	DWORD stat_thres_adc4_flag ;
	DWORD stat_thres_adc5_flag ;
	DWORD stat_thres_adc6_flag ;
	DWORD stat_thres_adc7_flag ;
	DWORD stat_thres_adc8_flag ;


	INT poll_counter = 0;
	INT status = 0;
	INT poll_success=0;
//	cm_msg(MINFO,"isDataReady", "Waiting for Threshold ... ");
	do {
		armed = sis3320_RegisterRead(k600vme,SIS3302_BASE,0x10);
		if( armed < 0 )
		{
	//		cm_msg(MINFO,"WaitDataReady", " Things are fucked up: armed<0 : 0x%x ",armed)	;
	//		break;
		}else if( (armed& 0x80000) == 0x80000 ){
	//	cm_msg(MINFO,"WaitDataReady","End Address Threshold Reached : armed: 0x%x , poll_counter: %d ",armed,poll_counter);
			poll_success = 1;
			break;
	        } 


		//printf("armed = 0x%08x " ,armed);
		//if( (armed &0x40000) == 0x40000)
	//	printf("I'm still busy ... \n");
		status_threshold_flags = (armed>>24)&0xff;
		stat_thres_adc1_flag = (status_threshold_flags&0x1);
		stat_thres_adc2_flag = (status_threshold_flags&0x2);
		stat_thres_adc3_flag = (status_threshold_flags&0x4);
		stat_thres_adc4_flag = (status_threshold_flags&0x8);
		stat_thres_adc5_flag = (status_threshold_flags&0x10);
		stat_thres_adc6_flag = (status_threshold_flags&0x20);
		stat_thres_adc7_flag = (status_threshold_flags&0x40);
		stat_thres_adc8_flag = (status_threshold_flags&0x80);


		if( poll_counter == 10000 ){
			//printf("Waiting for Threshold too long.... \n");	
			poll_counter = 0;
		}
		poll_counter++;	
//		status = cm_yield(0);
//		ss_sleep(10);
//	}while( ((armed & 0x80000) != 0x80000) && (gRunStopStatusFlag==FALSE) && (status != RPC_SHUTDOWN) && (status != SS_ABORT));
//	}while( ((armed & 0x80000) != 0x80000) && my_readout_enabled() );
	}while( ((armed & 0x80000) != 0x80000) && (gRunStopStatusFlag==FALSE) );


	if(poll_success == 1){
		poll_success = 0;
		return 1;
	}




	return 0;

}//WaitDataReady


INT DisarmSample(INT bank1_flag, DWORD module_addr)
{
	//sis3320_RegisterWrite(k600vme,SIS3302_BASE,SIS3302_KEY_VME_DISARM_SAMPLE_LOGIC,0x0);
	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_KEY_VME_DISARM_SAMPLE_LOGIC,0x0);
	bank1_flag = 0;
	return bank1_flag;
}//DisarmSample

INT ArmSample(INT bank1_flag, DWORD module_addr)
{
	DWORD armed=0x0;

	if(bank1_flag == 0){
		sis3320_RegisterWrite(k600vme,module_addr, SIS3302_KEY_DISARM_SAMPLE_ARM_BANK1,0x0);
		armed = sis3320_RegisterRead(k600vme,module_addr, SIS3302_ACQUISTION_CONTROL);
		//bank1_flag = 1; //start condition
		bank1_flag = ((armed>>16)&0x1); //start condition
		return bank1_flag;
	}//if-unarm-arm
	return bank1_flag;
}//ArmSample

INT DualBankArmDisarm(INT bank1_flag, DWORD module_addr)
{

	if(bank1_flag == 1){
		//Disarm Bank1 and arm Bank2
		sis3320_RegisterWrite(k600vme,module_addr, SIS3302_KEY_DISARM_SAMPLE_ARM_BANK2,0x0);
//		printf("disarming bank1, arming bank2\n");
		return 0; //start condition
	}else{
		//Disarm Bank2 and arm Bank1
		sis3320_RegisterWrite(k600vme,module_addr, SIS3302_KEY_DISARM_SAMPLE_ARM_BANK1,0x0);
//		printf("disarming bank2, arming bank1\n");
		return 1; //start condition
	}
	return bank1_flag;

}//DualBankArmDisarm

INT DualBankCheckIfSwapped(INT bank1_flag , DWORD module_addr)
{
	DWORD armed = 0x0;
	int poll_loop_valid = 1;
	int poll_counter = 0;
	do{

		armed = sis3320_RegisterRead(k600vme, module_addr , SIS3302_ACQUISTION_CONTROL);

		//If Bank1 is armed
		if(bank1_flag == 1){
			//If valid arm for Bank1
			if( (armed &0x10000) == 0x10000){
				poll_loop_valid = 0;
#ifdef DEBUG_SIS_BANKSWAP
				 cm_msg(MINFO,"DualBankCheckIfSwapped","Bank1  Valid ===== ***: pollcounter [%d] ,BF [%d] ",poll_counter,bank1_flag);
#endif			
			}
		}else{
			if( (armed &0x20000) == 0x20000){
				poll_loop_valid = 0;
#ifdef DEBUG_SIS_BANKSWAP
				cm_msg(MINFO,"DualBankCheckIfSwapped","Bank2  Valid ===== ***: pollcounter [%d] ,BF [%d] ",poll_counter,bank1_flag);
#endif			
			}
		}//if-else


		poll_counter++;
		if(poll_counter > 500){
			//poll_counter = 0;
			poll_loop_valid = 0;
			//printf(" Counter = [%d] \n", mycounter*1000);
		}		
	}while(poll_loop_valid == 1);

	if(poll_loop_valid == 0)
		return 1;

	return 0;
}//DualBankCheckIfSwapped


#undef SIS_TIMING

INT sis3302_read_A32BLT_ADC_MEMORY(DWORD module_addr,
		DWORD adc_channel,
		DWORD memory_start_addr,
		DWORD *data_buffer,
		DWORD req_lwords,
		DWORD *got_lwords)
{

	INT return_code;
	INT cmode;
	INT dmode;

	DWORD data;
	DWORD addr, got_nof_lwords;
	DWORD index_num_data;

	DWORD max_page_lword_length, page_lword_length_mask;
	DWORD page_byte_addr_mask;
	DWORD next_memory_byte_addr;
	DWORD rest_req_lwords;
	DWORD sub_memory_byte_addr;
	DWORD sub_req_lwords;
	DWORD sub_max_page_lword_length;
	DWORD sub_page_addr_offset;
#ifdef SIS_TIMING
	struct timeval t1,t2,t3,t4;
	int dt1,dt2,dt3,dt4;
#endif

	max_page_lword_length = SIS3302_MAX_ADC_MEMORY_PAGE_BYTE_LENGTH/4;
	page_lword_length_mask = max_page_lword_length - 1 ;

	page_byte_addr_mask = SIS3302_MAX_ADC_MEMORY_PAGE_BYTE_LENGTH -1;
	next_memory_byte_addr = memory_start_addr & (SIS3302_MAX_ADC_MEMORY_PAGE_BYTE_LENGTH-4);
	rest_req_lwords = req_lwords;
#ifdef DEBUG_SIS_VME
	cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," ----=== [ sis3302_read_A32BLT_ADC_MEMORY ] ===---- ");
	cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY","max_page_lword_length = 0x%08x ",max_page_lword_length);
	cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY","page_lword_length_mask = 0x%08x ", page_lword_length_mask );
	cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," page_byte_addr_mask = 0x%08x ", page_byte_addr_mask);
	cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," next_memory_byte_addr = 0x%08x ",next_memory_byte_addr);
	cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," req_lwords = 0x%08x ", req_lwords);
	cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY","(buffer_length) rest_req_lwords = 0x%08x ", rest_req_lwords);
#endif

	//data_buffer_transfer = M_MALLOC(req_lwords*sizeof(uint32_t));
	//mset(data_buffer_transfer,0,req_lwords*sizeof(uint32_t));


	got_nof_lwords = 0x0; 
	if( ((4*rest_req_lwords) + next_memory_byte_addr) > SIS3302_MAX_ADC_MEMORY_PAGE_BYTE_LENGTH ){
		cm_msg(MERROR,"sis3302_read_A32BLT_ADC_MEMORY","Error!");
		return -1;
	}

	return_code = 0;
	index_num_data = 0x0;
	//cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," ----=============== VME READOUT LOOP STARTED ================-------------- ");
	int loop_counter = 0;
	do{

		sub_memory_byte_addr = (next_memory_byte_addr & page_byte_addr_mask);
		sub_max_page_lword_length = max_page_lword_length - (sub_memory_byte_addr >> 2);
#ifdef DEBUG_SIS_VME
			cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," sub_memory_byte_addr = 0x%08x ",sub_memory_byte_addr);
			cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," sub_max_page_lword_length = 0x%08x ",sub_max_page_lword_length);
#endif
		if( rest_req_lwords >= sub_max_page_lword_length)
			sub_req_lwords = sub_max_page_lword_length;
		else
			sub_req_lwords = rest_req_lwords;

		sub_page_addr_offset = (next_memory_byte_addr >> SIS3302_ADC_MEMORY_PAGE_REG_ADDR_SHIFT) 
			& SIS3302_ADC_MEMORY_PAGE_REG_ADDR_MASK;

		data = sub_page_addr_offset;
		//data = 0x0;
#ifdef SIS_TIMING
		gettimeofday(&t1,NULL);	
#endif



		sis3320_RegisterWrite(k600vme,SIS3302_BASE,SIS3302_ADC_MEMORY_PAGE_REGISTER,data);



#ifdef SIS_TIMING
		gettimeofday(&t2,NULL);
		dt1 = t2.tv_usec - t1.tv_usec;
#endif
		addr = module_addr+SIS3302_ADC1_OFFSET
			+(SIS3302_NEXT_ADC_OFFSET*adc_channel)+(sub_memory_byte_addr);

#ifdef DEBUG_SIS_VME
		cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," addr = 0x%08x ",addr);
		cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," sub_req_lwords = 0x%08x ",sub_req_lwords);
		cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," sub_page_addr_offset = 0x%08x ",sub_page_addr_offset);
		cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," sub_memory_byte_addr = 0x%08x ",sub_memory_byte_addr);
#endif


		mvme_get_dmode(k600vme,&dmode);
		mvme_get_am(k600vme,&cmode);
		//mvme_set_am(k600vme,MVME_AM_A32_NMBLT);
		mvme_set_am(k600vme,MVME_AM_A32_SD);
		mvme_set_dmode(k600vme,MVME_DMODE_D32);
		//mvme_set_blt(k600vme,MVME_BLT_BLT32);
		mvme_size_t req_bytes = (sub_req_lwords*sizeof(DWORD));
		//mvme_size_t req_bytes = (sub_req_lwords);
		//	mvme_size_t req_bytes = sub_req_lwords;
		//	cm_msg(MINFO,"fuckingvmeread","sub_req_lwords : [%d], req_bytes : [%d] ",sub_req_lwords,req_bytes);
		//	if( req_bytes >  127 )	

		DWORD bytes_read_by_vme=0;
#ifdef SIS_TIMING
		gettimeofday(&t3,NULL);
#endif
		bytes_read_by_vme = mvme_read( k600vme, &data_buffer[index_num_data],addr, sub_req_lwords );
#ifdef SIS_TIMING
		gettimeofday(&t4,NULL);
		dt2 = t4.tv_usec - t3.tv_usec;

		cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY","Page vme write time: [%ld], total mvme read time [%ld], bytes read: [%d], loop counter [%d], channel [%d] \n",dt1,dt2,sub_req_lwords,loop_counter,adc_channel);
#endif
		
		mvme_set_dmode(k600vme,dmode);
		mvme_set_am(k600vme,cmode);
		//	else 

		//got_nof_lwords = bytes_read_by_vme/sizeof(DWORD);
		got_nof_lwords = sub_req_lwords;
		//

		if(got_nof_lwords < 0)
			break;

		//	got_nof_lwords = mvme_read( k600vme, data_buffer,addr, sub_req_lwords );



		//	int i = 0;
		//	for(i = 0; i < got_nof_lwords; i++ )
		//		printf(" vword = 0x%08x \n",data_buffer[i]);	

		index_num_data += got_nof_lwords;
		next_memory_byte_addr += (4*sub_req_lwords);
		rest_req_lwords -= sub_req_lwords;
		loop_counter++;
	}while( (return_code == 0) && (rest_req_lwords>0));

	//memcpy(data_buffer,data_buffer_transfer,index_num_data*sizeof(DWORD));

	*got_lwords = got_nof_lwords;
#ifdef DEBUG_SIS_VME
	cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," ------ VME READ DONE ----- ");
	cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," index_num_data = 0x%08x[%d] ",index_num_data,index_num_data);
	cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," next_memory_byte_addr = 0x%08x ",next_memory_byte_addr);
	cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," rest_req_lwords = [%d] ",rest_req_lwords);
	cm_msg(MINFO,"sis3302_read_A32BLT_ADC_MEMORY"," got_lwords = [%d], index_num_data = [%d], loop_counter = [%d] ", got_nof_lwords,index_num_data,loop_counter);
#endif
	//free(data_buffer_transfer);


	return 0;

}//sis3302_read_A32BLT_ADC_MEMORY




