# K600 VME frontend

This is the K600 Multi-Threaded VME frontend. It does the readout of the detector hardware, and event construction.
It uses the [MIDAS](https://bitbucket.org/tmidas/midas/src/develop/) data acquisition framework, user code, and hardware specific drivers.

## Getting Started

This project can be obtained via permission from bitbucket:

git clone git@bitbucket.org:k600dataacquisition/k600fe.git

For more information, visit: 

[iTL DAQ Page](http://daq.tlabs.ac.za)


or via email to lcpool@tlabs.ac.za, to obtain from iTL local git repo.



### Prerequisites

To compile and use the basic functionality of this frontend, 
you will need,

[MIDAS-K600](https://bitbucket.org/rloots/midas-k600/src/newconc/) fork, which will include all the required hardware drivers that
is used by the K600 Data Aqcuisition system. 

git clone git@bitbucket.org:rloots/midas-k600.git

For external users to iTL, this requires permission, email to lcpool@tlabs.ac.za

The single board controller vme kernel driver is required, and is supplied with the 
board.


### Installing


To select which hardware driver you wish to use/is available to you, you can set this via
the Makefile supplied,

This reflects the single board controller, and this example it is the concurrent vme controller board,
but if you have the gefanuc xrv15 board, then here you would use: gefvme.

DRIVER = concvme

These are the hardware normally within the vme crate that you wish to read out via the single board controller 
above. You can select(remove) or add modules that is compitable with vme readout, and that is within the 
$MIDASSYS/driver/vme directory.

VMEMODULES = v1190A v792 v792n v830 sis3320

The libraries and helper source code below is required if you would like to use the STRUCK sis3302 flash ADC's.
This requires additional kernel setup, for the required reservered memory to be available. For this
you would have to see the driver install/setup requirements, as this is specific to the sbc being used
and driver. The source below, also provides user interface with the MIDAS odb and thus via mhttpd web
interface for online changes.

MODULESFE = sis3302felib sis3302fevme


## Building
Clone the repo

	$ git clone git@bitbucket.org:k600dataacquisition/k600fe.git
	$ cd k600fe

Run make

	$ ./runmake.sh
	
If running an experiment,

	$ cd /home/online
	$ ./k600fevme -h host -e experiment, eg. k600pr291




additional notes about how to deploy this on a live system

## Built With

* [MIDAS-K600](https://bitbucket.org/rloots/midas-k600/src/develop/) - Fork of the original PSI/TRIUMF [MIDAS](https://bitbucket.org/tmidas/midas/src/develop/) also on bitbucket.

## Contributing


## Versioning

We use [SemVer](http://semver.org/) for versioning. 


