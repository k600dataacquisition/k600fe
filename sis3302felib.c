/*
 * =====================================================================================
 *
 *       Filename:  sis3302felib.c
 *
 *    Description:  This library does the configuration for the sis3302 with midas odb.
 *    				It does contain some default setups for the sis3302 module, but 
 *    				in general it requires the midas odb to be configured with the
 *    				correct parameters to work. 
 *
 *        Version:  1.0
 *        Created:  27/10/2015 11:20:16
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>
#include <byteswap.h>
#include <time.h>

/* Midas includes */
#include "midas.h"
#include "mcstd.h"
#include "experim.h"
#include "msystem.h"

#include "sis3320drv.h"
#include "sis3302gamma.h"
//#include "sis3302_v1411.h"

/* header */
#include "sis3302felib.h"
#include "fesis3302.h"
/* make frontend functions callable from the C framework */
//#ifdef __cplusplus
//extern "C" {
//#endif

//nof_raw_data_words = 0;
//nof_energy_words = 0;
//max_nof_events = 0;
//event_length_lwords = 0;
//event_sample_start_addr = 0x0;
//event_sample_length = 0x00;
//energy_max_index = 0;

extern HNDLE hDB;
HNDLE hSet;
HNDLE hkey;


INT gChannel;
INT gWriteDAC;

INT sis3302_Setup(DWORD mode , DWORD module_addr, int mod_num);
void sis3302_LED( int led_switch ,DWORD,int);

int sis3302_write_dac_offset(DWORD,DWORD*);
void sis3302_ReadVersion(DWORD,int);
void sis3302_ClockStatus(DWORD , int );
void sis3302_FrontPanelLemoIn_1(DWORD,int);
void sis3302_FrontPanelLemoIn_2(DWORD,int);
void sis3302_FrontPanelLemoIn_3(DWORD,int);
void sis3302_LemoMode(DWORD, int);
/*
 * Functions
 *
 */
void DAC_INFOpage(int mod_num)
{
	float V_REFIN = 5.0;
	float V_OUT_DAC[8];
	int status = 0;
	int size =0;
	int i = 0;
	
	char str[50];

	cm_msg(MINFO,"DAC_INFOpage"," ---- Updating Info Page : SIS Module [%d] ---- ",mod_num);
	
	for(i = 0; i < 8;i++){
			float term1 = -2.0*V_REFIN;
			float term2 = 4.0*V_REFIN;
		
			
			float term3 = (float)gDAC[i]/(float)65536.0;


			V_OUT_DAC[i] = term1 + term2*term3;

	}
	size = sizeof(V_OUT_DAC);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/InfoPage/DAC_VOUT_VALUES",mod_num);
	status = db_set_value(hDB, 0, str, &V_OUT_DAC, size, 8, TID_FLOAT);
	if( status != DB_SUCCESS){
		cm_msg(MERROR,"DAC_INFOpage","Could not update odb infopage error [%d] ",status);
	}
}//DAC_INFOpage



void sis3302_init_dac(DWORD module_addr, int mod_num)
{
	int status;
	char str[80];
	uint32_t dac_odb[8];
	int size = sizeof(dac_odb);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/RAW/dac",mod_num);
	status =
		db_get_value(hDB, 0, str,&dac_odb, &size, TID_DWORD, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read dac values ");
	}

	int i = 0;

	 cm_msg(MINFO,"sis3302_init_dac"," -----SIS Module [%d] -------- DAC Values  ----------- ",mod_num);
	 
	for(i=0;i<8;i++){
		gDAC[i] = dac_odb[i];

	 cm_msg(MINFO,"sis3302_init_dac"," DAC[%d]					: [0x%08x], ODB [0x%08x] ",i,gDAC[i],dac_odb[i]);
		//	if( endaddr[0] >= 0 )
	//		gEndAddressThreshold = (endaddr[0]<<2);
	//	//gEndAddressThreshold = 0x1c0;

	}//for

	 cm_msg(MINFO,"sis3302_init_dac"," -------------------- Writing DAC Values  ----------- ");
	 status = sis3302_write_dac_offset(module_addr,gDAC);
	
	 if( status == 1 ){
	 		cm_msg(MINFO,"sis3302_init_dac","Wrote DAC Offsets Succesfully  ");
	 }else if(status < 0 ){
		cm_msg(MERROR,"sis3302_init_dac","ERROR in Writing DAC Offset.");
	 }


	 DAC_INFOpage(mod_num);


}//sis3302_init_dac



int sis3302_write_dac_offset(uint32_t mod_addr, uint32_t *offset_value_array)
{
	int i = 0;
	uint32_t data,max_timeout,timeout_cnt;

	for(i=0;i<8;i++) {
			data = offset_value_array[i];
		    sis3320_RegisterWrite(k600vme,mod_addr,SIS3302_DAC_DATA,data);

			data = 1 + (i<<4);
		    sis3320_RegisterWrite(k600vme,mod_addr,SIS3302_DAC_CONTROL_STATUS,data);
			
			
			//ss_sleep(100);
			
			//wait for dac status
			max_timeout = 5000;
			timeout_cnt = 0;
			do {
					data  = sis3320_RegisterRead(k600vme, mod_addr, SIS3302_DAC_CONTROL_STATUS);
					if( data < 0 ){
						cm_msg(MERROR,"sis3302_write_dac_offset"," Could not WRITE DAC OFFSETS: Ch[%d]",i);
						return -1;
					}
					timeout_cnt++;
			}while( ((data&0x8000) == 0x8000) && (timeout_cnt < max_timeout) );
	
			if( timeout_cnt >= max_timeout )
			{
				cm_msg(MERROR,"sis3302_write_dac_offset","timeoutcnt >= max_timeout: timeout issue while WRITING");
				return -2;
			}

			//load DACs
			data = 2 + (i<<4);
		    sis3320_RegisterWrite(k600vme,mod_addr,SIS3302_DAC_CONTROL_STATUS,data);

			//wait for dac status
			max_timeout = 5000;
			timeout_cnt = 0;
			do {
					data  = sis3320_RegisterRead(k600vme, mod_addr, SIS3302_DAC_CONTROL_STATUS);
					if( data < 0 ){
						cm_msg(MERROR,"sis3302_write_dac_offset"," Could not LOAD DAC OFFSETS: Ch[%d]",i);
						return -1;
					}
					timeout_cnt++;
			}while( ((data&0x8000) == 0x8000) && (timeout_cnt < max_timeout) );
	
			if( timeout_cnt >= max_timeout )
			{
				cm_msg(MERROR,"sis3302_write_dac_offset","timeoutcnt >= max_timeout: timeout issue while LOADING");
				return -3;
			}




	}//for=channels

	return 1;
}



void Channel_update(HNDLE hDB, HNDLE hKey, void *info )
{
	cm_msg(MINFO,"Channel_update","new channel %d ",gChannel);
}

void WriteDAC_update(HNDLE hDB, HNDLE hKey, void *info )
{
	cm_msg(MINFO,"WriteDAC","new value %d",gWriteDAC);
	

}

void VREFIN_update(HNDLE hDB, HNDLE hKey, void *info )
{
//	cm_msg(MINFO,"VREFIN_update","new value %d",gVREFIN);
	
//	DAC_INFOpage();

}



void createHotLinks()
{

    HNDLE hKey;
    //int status;
//    cm_get_experiment_database(&hDB,NULL);

 
    /* now open run_number with automatic updates */
   db_find_key(hDB, 0, "/Analyzer/Parameters/sis3302/Channel", &hKey);
   db_open_record(hDB, hKey, &gChannel, sizeof(gChannel),
                  MODE_READ, Channel_update, NULL);
  
    db_find_key(hDB, 0, "/Analyzer/Parameters/sis3302/RAW/writeDAC", &hKey);
   db_open_record(hDB, hKey, &gWriteDAC, sizeof(gWriteDAC),
                  MODE_READ, WriteDAC_update, NULL);

 //  db_find_key(hDB, 0, "/Analyzer/Parameters/sis3302/InfoPage/VREFIN", &hKey);
 //  db_open_record(hDB, hKey, &gVREFIN, sizeof(gVREFIN),
  //                MODE_READ, VREFIN_update, NULL);

 
   //int size = sizeof(gChannel);
   //status =
    //   db_get_value(hDB, 0, "Analyzer/Parameters/sis3302/Channel", &gChannel, &size, TID_INT, TRUE);
  // if (status != DB_SUCCESS) {
   //   printf("Cannot read Channel Number");
 //  }

//   cm_msg(MINFO,"createHotLinks"," Channel Number %d ", gChannel);


}

/*
 *
 *
 * SIS3302 ADDED Functions.
 *
 *
 *
 *
 */

void sis3302_DacSettings()
{
	int i=0;

	for(i=0;i<8;i++){


	}



}


void sis3302_LED( int led_switch ,DWORD module_addr, int mod_num)
{
	int csr;

	cm_msg(MINFO,"sis3302_LED"," ----------------- USER LED ------ SIS Module[%d]",mod_num);
	if( led_switch == 1 ){
		  cm_msg(MINFO,"sis3302_LED","----LED On");
		  csr = sis3320_RegisterRead(k600vme, module_addr, 0x0);
  		  //cm_msg(MINFO,"sis3302_LED"," csr *before* setting LED on:  0x%x , led switch %d ", csr,csr&0x1 );
  		  sis3320_RegisterWrite(k600vme,module_addr,0x0,csr|0x1);
 		  //csr = sis3320_RegisterRead(k600vme, SIS3302_BASE, 0x0);
  		  cm_msg(MINFO,"sis3302_LED"," csr *after* setting LED on:  0x%x , led switch %d ", csr,csr&0x1 );
	}else if( led_switch == 0 ){
		  cm_msg(MINFO,"sis3302_LED","---LED Off");
	          csr = sis3320_RegisterRead(k600vme, module_addr, 0x0);
  		  //cm_msg(MINFO,"sis3302_LED"," csr *before* setting LED on:  0x%x , led switch %d ", csr,csr&0x1 );
  		  sis3320_RegisterWrite(k600vme, module_addr,0x0,csr|0x10000);
 		  csr = sis3320_RegisterRead(k600vme, module_addr, 0x0);
  		  //cm_msg(MINFO,"sis3302_LED"," csr *after* setting LED on:  0x%x , led switch %d ", csr,csr&0x1 );
	}

}//sis3302_LED


void sis3302_ReadVersion(DWORD module_addr , int mod_num)
{

     uint32_t mod_id = sis3320_RegisterRead(k600vme, module_addr,SIS3302_MODID);

     uint32_t major = mod_id&0xffff;
     uint32_t idd = (mod_id>>16)&0xffff;
	 char str[80];
     cm_msg(MINFO,"ReadVersion","  SIS3302: Version [0x%x], Firmware: major: 0x%x minor: 0x%0x  \n",idd,major&0xff00,major&0x00ff );
     sprintf(str,"/Analyzer/Parameters/sis3302_%d/general/ModVersion",mod_num);
	 db_set_value(hDB, 0, str,&idd, sizeof(idd), 1, TID_DWORD);
     uint32_t mj = major&0xff00;
	 memset(str,0,80);
	 sprintf(str,"/Analyzer/Parameters/sis3302_%d/general/ModVersionMajor",mod_num);
     db_set_value(hDB, 0, str, &mj, sizeof(mj), 1, TID_DWORD);
     uint32_t mn = major&0x00ff;
	 memset(str,0,80);
	 sprintf(str, "/Analyzer/Parameters/sis3302_%d/general/ModVersionMinor",mod_num);
     db_set_value(hDB, 0,str, &mn, sizeof(mn), 1, TID_DWORD);
}//sis3302_ReadVersion


void sis3302_AcquisitionControl(DWORD module_addr, int mod_num)
{
	int status,size;
	uint32_t acontrol_setup = 0x0;
    char str[80];

    int getclock_odb;	
	size = sizeof(getclock_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/general/clock",mod_num);
	status =
		db_get_value(hDB, 0, str, &getclock_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_AcquisitionControl","Cannot read clock mode odb");
	}
	
	gAcquisitionControl.ClockTypeSet = getclock_odb;

	int getmcamode_odb=0;	
	size = sizeof(getmcamode_odb);
	memset(str,0,80);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/general/MCAMode",mod_num);
	status =
		db_get_value(hDB, 0, str,&getmcamode_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_AcquisitionControl","Cannot read mcamode odb");
	}
	
	gAcquisitionControl.MCAMode = getmcamode_odb;

	int getlemoinmode_odb=0;	
	size = sizeof(getlemoinmode_odb);
	memset(str,0,80);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/general/LemoIn/LemoInMode",mod_num);
	status =
		db_get_value(hDB, 0,str, &getlemoinmode_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_AcquisitionControl","Cannot read Lemo In Mode odb");
	}
	
	gAcquisitionControl.LemoInMode = getlemoinmode_odb;

	int getlemoOutmode_odb=0;	
	size = sizeof(getlemoOutmode_odb);
	memset(str,0,80);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/general/LemoOut/LemoOutMode",mod_num);
	status =
		db_get_value(hDB, 0, str, &getlemoOutmode_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_AcquisitionControl","Cannot read Lemo Out Mode odb");
	}
	
	gAcquisitionControl.LemoOutMode = getlemoOutmode_odb;

	int getlemo1Enable_odb=0;	
	size = sizeof(getlemo1Enable_odb);
	memset(str,0,80);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/general/LemoIn1Enable",mod_num);
	status =
		db_get_value(hDB, 0, str, &getlemo1Enable_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_AcquisitionControl","Cannot read Lemo in 1 Enable odb");
	}
	gAcquisitionControl.LemoIn1Enable = getlemo1Enable_odb;
	
	int getlemo2Enable_odb=0;	
	size = sizeof(getlemo2Enable_odb);
	memset(str,0,80);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/general/LemoIn2Enable",mod_num);
	status =
		db_get_value(hDB, 0, str, &getlemo2Enable_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_AcquisitionControl","Cannot read Lemo in 2 Enable odb");
	}
	gAcquisitionControl.LemoIn2Enable = getlemo2Enable_odb;
	
	
	int getlemo3Enable_odb=0;	
	size = sizeof(getlemo3Enable_odb);
	memset(str,0,80);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/general/LemoIn3Enable",mod_num);
	status =
		db_get_value(hDB, 0, str, &getlemo3Enable_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_AcquisitionControl","Cannot read Lemo in 3 Enable odb");
	}
	gAcquisitionControl.LemoIn3Enable = getlemo3Enable_odb;


	switch(gAcquisitionControl.ClockTypeSet){
		case 0: acontrol_setup += SIS3302_ACQ_SET_CLOCK_TO_100MHZ;
			break;
		case 1: acontrol_setup += SIS3302_ACQ_SET_CLOCK_TO_50MHZ;
			break;
		case 2: acontrol_setup += SIS3302_ACQ_SET_CLOCK_TO_25MHZ;
			break;
		case 3: acontrol_setup += SIS3302_ACQ_SET_CLOCK_TO_10MHZ;
			break;
		case 4: acontrol_setup += SIS3302_ACQ_SET_CLOCK_TO_1MHZ;
			break;
		case 5: acontrol_setup += SIS3302_ACQ_SET_CLOCK_TO_100MHZ;
			break;
		case 6: acontrol_setup += SIS3302_ACQ_SET_CLOCK_TO_LEMO_RANDOM_CLOCK_IN;
			break;
		case 7: acontrol_setup += SIS3302_ACQ_SET_CLOCK_TO_SECOND_100MHZ;
			break;
	};


	switch(gAcquisitionControl.LemoOutMode){
		case 0: acontrol_setup += SIS3302_ACQ_SET_LEMO_OUT_MODE0;
			break;
		case 1: acontrol_setup += SIS3302_ACQ_SET_LEMO_OUT_MODE1;
			break;
		case 2: acontrol_setup += SIS3302_ACQ_SET_LEMO_OUT_MODE2;
			break;
		case 3: acontrol_setup += SIS3302_ACQ_SET_LEMO_OUT_MODE3;
			break;
	};


	switch(gAcquisitionControl.LemoInMode){
		case 0: acontrol_setup += SIS3302_ACQ_SET_LEMO_IN_MODE0;
			break;
		case 1: acontrol_setup += SIS3302_ACQ_SET_LEMO_IN_MODE1;
			break;
		case 2: acontrol_setup += SIS3302_ACQ_SET_LEMO_IN_MODE2;
			break;
		case 3: acontrol_setup += SIS3302_ACQ_SET_LEMO_IN_MODE3;
			break;
		case 4: acontrol_setup += SIS3302_ACQ_SET_LEMO_IN_MODE4;
			break;
		case 5: acontrol_setup += SIS3302_ACQ_SET_LEMO_IN_MODE5;
			break;
		case 6: acontrol_setup += SIS3302_ACQ_SET_LEMO_IN_MODE6;
			break;
		case 7: acontrol_setup += SIS3302_ACQ_SET_LEMO_IN_MODE7;
			break;
	};


	if(gAcquisitionControl.LemoIn1Enable == 1)
		acontrol_setup += SIS3302_ACQ_ENABLE_EXTERNAL_LEMO_IN1;
	else if(gAcquisitionControl.LemoIn1Enable == 0)
		acontrol_setup += SIS3302_ACQ_DISABLE_EXTERNAL_LEMO_IN1;

	if(gAcquisitionControl.LemoIn2Enable == 1)
		acontrol_setup += SIS3302_ACQ_ENABLE_EXTERNAL_LEMO_IN2;
	else if(gAcquisitionControl.LemoIn2Enable == 0)
		acontrol_setup += SIS3302_ACQ_DISABLE_EXTERNAL_LEMO_IN2;

	if(gAcquisitionControl.LemoIn3Enable == 1)
		acontrol_setup += SIS3302_ACQ_ENABLE_EXTERNAL_LEMO_IN3;
	else if(gAcquisitionControl.LemoIn3Enable == 0)
		acontrol_setup += SIS3302_ACQ_DISABLE_EXTERNAL_LEMO_IN3;

	if(gAcquisitionControl.EnableTriggerFeedback == 1)
		acontrol_setup += SIS3302_ACQ_SET_FEEDBACK_INTERNAL_TRIGGER;
	else if(gAcquisitionControl.EnableTriggerFeedback == 0)
		acontrol_setup += SIS3302_ACQ_CLR_FEEDBACK_INTERNAL_TRIGGER;

	if(gAcquisitionControl.MCAMode == 1)
		acontrol_setup += SIS3302_ACQ_SET_MCA_MODE;
	else if(gAcquisitionControl.MCAMode == 0)
		acontrol_setup += SIS3302_ACQ_CLR_MCA_MODE;	


	//write Acquisition Control Register
	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_ACQUISTION_CONTROL,acontrol_setup);

	//ReadBackup
	DWORD acontrol = sis3320_RegisterRead(k600vme, module_addr, SIS3302_ACQUISTION_CONTROL);
	
	DWORD status_mca_mscan_busy = (acontrol>>21)&0x1;
	DWORD status_mca_scan_busy = (acontrol>>20)&0x1;
	DWORD status_end_adr_threshold = (acontrol>>19)&0x1;
	DWORD status_adc_busy = (acontrol>>18)&0x1;
	DWORD status_adc_arm_bank2 = (acontrol>>17)&0x1;
	DWORD status_adc_arm_bank1 = (acontrol>>16)&0x1;
	DWORD status_ored_internal_trigger = (acontrol>>6)&0x1;
	DWORD status_mca_mode = (acontrol>>6)&0x1;
	DWORD status_myclock = (acontrol>>12)&0x7;
	

	cm_msg(MINFO,"sis3302_AcquisitionControl"," ------------------- ACQUISITION CONTROL  -------SIS Module [%d] ---------- ",mod_num);
	cm_msg(MINFO,"sis3302_AcquisitionControl"," Status of MCA Multiscan Busy 				  : [%d]",status_mca_mscan_busy);
	cm_msg(MINFO,"sis3302_AcquisitionControl"," Status of MCA Scan Busy 					  : [%d]",status_mca_scan_busy);
	cm_msg(MINFO,"sis3302_AcquisitionControl"," Status of End Address Threshold Flag			  : [%d]",status_end_adr_threshold);
	cm_msg(MINFO,"sis3302_AcquisitionControl"," Status of ADC-Sample-Logic Busy(Armed)			  : [%d]",status_adc_busy);
	cm_msg(MINFO,"sis3302_AcquisitionControl"," Status of ADC-Sample-Logic Armed Bank2			  : [%d]",status_adc_arm_bank2);
	cm_msg(MINFO,"sis3302_AcquisitionControl"," Status of ADC-Sample-Logic Armed Bank1			  : [%d]",status_adc_arm_bank1);
	cm_msg(MINFO,"sis3302_AcquisitionControl"," Status ored Internal Trigger to External Trigger In (Feedback): [%d]",status_ored_internal_trigger);
	cm_msg(MINFO,"sis3302_AcquisitionControl"," Status MCA Mode Bit						  : [%d]",status_mca_mode);
	cm_msg(MINFO,"sis3302_AcquisitionControl"," Clock : [%d], ODB[%d]					  : [%d]",status_myclock,getclock_odb);

	sis3302_ClockStatus(module_addr,mod_num);
	sis3302_FrontPanelLemoIn_1(module_addr,mod_num);
	sis3302_FrontPanelLemoIn_2(module_addr,mod_num);
	sis3302_FrontPanelLemoIn_3(module_addr,mod_num);
	sis3302_LemoMode(module_addr,mod_num);
}//sis3302_AcquisitionControl



void sis3302_PreTriggerGateDelay(DWORD module_addr, int mod_num)
{

	uint32_t preTrigDelay[4];
	uint32_t GateLength[4];
	int status,i;
	int size = sizeof(preTrigDelay);
	char str[80];
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/PretriggerDelay",mod_num);
	status =
		db_get_value(hDB, 0,str , &preTrigDelay, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read TriggerSetup ");
	}
	size = sizeof(GateLength);
	memset(str,0,80);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/TriggerGateLength",mod_num);
	status =
		db_get_value(hDB, 0,str, &GateLength, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read TriggerSetup ");
	}

	for(i=0;i<4;i++){
		gTriggerGatePretrigger[i].pretriggerDelay = preTrigDelay[i];
		gTriggerGatePretrigger[i].triggerGateLength = GateLength[i];

		DWORD preTrigGate_Register = ((preTrigDelay[i]&0x1ff)<<16) + (GateLength[i]&0xffff);

		switch(i) {
			case 0: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC12,preTrigGate_Register); //Pretrigger = 20; trig window = 448
					break;
			case 1: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC34,preTrigGate_Register); //Pretrigger = 20; trig window = 448
					break;
			case 2: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC56,preTrigGate_Register); //Pretrigger = 20; trig window = 448
					break;
			case 3: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC78,preTrigGate_Register); //Pretrigger = 20; trig window = 448
					break;
		};//switch
	}//for
	
	cm_msg(MINFO,"sis3302_PreTriggerADC12"," ---SIS Module [%d]------------ PRETRIGGER DELAY & TRIGGER GATE LENGTH ",mod_num);
	DWORD reg_read;
	DWORD pre_trigger;
	DWORD gate_length; 
	for(i=0;i<4;i++){ 
		reg_read = 0x0;
		pre_trigger = 0x0;
		gate_length = 0x0;
		switch(i) {
			case 0: reg_read = sis3320_RegisterRead(k600vme, module_addr,SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC12);
					pre_trigger = (reg_read>>16)&0x1ff;
					gate_length = (reg_read)&0xffff;
					cm_msg(MINFO,"sis3302_PreTriggerGateDelay","Group: [12] - Pretrigger Delay 		: [%d]",pre_trigger); 
					cm_msg(MINFO,"sis3302_PreTriggerGateDelay","Group: [12] - Trigger Gate Length	: [%d]",gate_length);
					break;
			case 1: reg_read = sis3320_RegisterRead(k600vme, module_addr,SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC34);
					pre_trigger = (reg_read>>16)&0x1ff;
					gate_length = (reg_read)&0xffff;
					cm_msg(MINFO,"sis3302_PreTriggerGateDelay","Group: [34] - Pretrigger Delay 		: [%d]",pre_trigger); 
					cm_msg(MINFO,"sis3302_PreTriggerGateDelay","Group: [34] - Trigger Gate Length	: [%d]",gate_length);
					break;
			case 2: reg_read = sis3320_RegisterRead(k600vme, module_addr,SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC56);
					pre_trigger = (reg_read>>16)&0x1ff;
					gate_length = (reg_read)&0xffff;
					cm_msg(MINFO,"sis3302_PreTriggerGateDelay","Group: [56] - Pretrigger Delay 		: [%d]",pre_trigger); 
					cm_msg(MINFO,"sis3302_PreTriggerGateDelay","Group: [56] - Trigger Gate Length	: [%d]",gate_length);
					break;
			case 3: reg_read = sis3320_RegisterRead(k600vme, module_addr,SIS3302_PRETRIGGER_DELAY_TRIGGERGATE_LENGTH_ADC78);
					pre_trigger = (reg_read>>16)&0x1ff;
					gate_length = (reg_read)&0xffff;
					cm_msg(MINFO,"sis3302_PreTriggerGateDelay","Group: [78] - Pretrigger Delay 		: [%d]",pre_trigger); 
					cm_msg(MINFO,"sis3302_PreTriggerGateDelay","Group: [78] - Trigger Gate Length	: [%d]",gate_length);
					break;
		};//switch
	}//for
}//sis3302_PreTriggerGateDelay

void sis3302_EventConfigurationPerChannel(DWORD module_addr, int mod_num)
{

	int inputinvert_odb,intTrig_odb,extTrig_odb,intGate_odb,extGate_odb,status;
	int size = sizeof(intTrig_odb);
	char str[80];
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/TriggerEnable/internalTrigger",mod_num);
	status =
		db_get_value(hDB, 0,str, &intTrig_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read ADCEvent_Configuration");
	}

	gEventConfiguration[0].ADC1InternalTriggerEnable = intTrig_odb;

	size = sizeof(extTrig_odb);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/TriggerEnable/externalTrigger",mod_num);
	status =
		db_get_value(hDB, 0,str, &extTrig_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read ADCEvent_Configuration");
	}

	gEventConfiguration[0].ADC1ExternalTriggerEnable = extTrig_odb;

	size = sizeof(extGate_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/GateEnable/externalGate",mod_num);
	status =
		db_get_value(hDB, 0,str , &extGate_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read ADCEvent_Configuration");
	}

	gEventConfiguration[0].ADC1ExternalGateEnable = extGate_odb;


	size = sizeof(intGate_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/GateEnable/internalGate",mod_num);
	status =
		db_get_value(hDB, 0,str , &intGate_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read ADCEvent_Configuration");
	}

	gEventConfiguration[0].ADC1InternalGateEnable = intGate_odb;


	size = sizeof(inputinvert_odb);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/TriggerEnable/ADCInputInvert",mod_num);
	status =
		db_get_value(hDB, 0,str, &inputinvert_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read ADCEvent_Configuration");
	}

	gEventConfiguration[0].ADC1InputInvert = inputinvert_odb;


	DWORD adc_header_odb[8];

	size = sizeof(adc_header_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/ADCHeader/ADC_ID",mod_num);
	status =
		db_get_value(hDB, 0,str , &adc_header_odb, &size, TID_DWORD, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read ADCEvent_Configuration");
	}

	int i = 0;
	for(i=0;i<8;i++){
		gADCHeaderID[i] = adc_header_odb[i];
	}


	uint32_t event_conf=0x0;
	//uint32_t adc_id = (SIS3302_BASE+0xf8000000);
	//uint32_t adc_id = (0xad000000);
	event_conf += adc_header_odb[0];

	if(gEventConfiguration[0].ADC1InputInvert == 1)
		event_conf += EVENT_CONF_ADC1_INPUT_INVERT_BIT;

	if(gEventConfiguration[0].ADC1InternalTriggerEnable == 1 && gEventConfiguration[0].ADC1ExternalTriggerEnable == 0 )
		event_conf += EVENT_CONF_ADC1_INTERN_TRIGGER_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalTriggerEnable == 0 && gEventConfiguration[0].ADC1ExternalTriggerEnable == 1 )
		event_conf += EVENT_CONF_ADC1_EXTERN_TRIGGER_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalGateEnable == 1 && gEventConfiguration[0].ADC1ExternalGateEnable == 0)
		event_conf += EVENT_CONF_ADC1_INTERN_GATE_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalGateEnable == 0 && gEventConfiguration[0].ADC1ExternalGateEnable == 1)
		event_conf += EVENT_CONF_ADC1_EXTERN_GATE_ENABLE_BIT;

	//WriteRegister
	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_EVENT_CONFIG_ALL_ADC,event_conf);
	
	//ReadBack
	event_conf = 0x0;
	event_conf = sis3320_RegisterRead(k600vme, module_addr, SIS3302_EVENT_CONFIG_ADC12);

	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," -- SIS Module [%d] ----   Event Config Register ADC 1/2 ------------ : 0x%x ",mod_num,event_conf);
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC HEADER ID  				: 0x%x ",(event_conf>>17)&0x7ffc);
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 Input invert bit				: %d ",(event_conf&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 Internal Trigger Enable(asynchronous) 	: %d ",((event_conf>>2)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 External Trigger Enable(synchronous) 	: %d ",((event_conf>>3)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 Internal Gate Enable  			: %d ",((event_conf>>4)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 External Gate Enable  			: %d ",((event_conf>>5)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 ADC N - 1 Next Neighbour gate enable  	: %d ",((event_conf>>6)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 ADC N + 1 Next Neighbour gate enable  	: %d ",((event_conf>>7)&0x1));
}//sis3302_EventConfiguration



void sis3302_EventConfiguration(DWORD module_addr, int mod_num)
{

	int inputinvert_odb,intTrig_odb,extTrig_odb,intGate_odb,extGate_odb,status;
	int size = sizeof(intTrig_odb);
	char str[80];

	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/TriggerEnable/internalTrigger",mod_num);
	status =
		db_get_value(hDB, 0,str, &intTrig_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read ADCEvent_Configuration");
	}

	gEventConfiguration[0].ADC1InternalTriggerEnable = intTrig_odb;

	size = sizeof(extTrig_odb);
	sprintf(str,  "/Analyzer/Parameters/sis3302_%d/ADC/TriggerEnable/externalTrigger",mod_num);
	status =
		db_get_value(hDB, 0, str,&extTrig_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read ADCEvent_Configuration");
	}

	gEventConfiguration[0].ADC1ExternalTriggerEnable = extTrig_odb;

	size = sizeof(extGate_odb);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/GateEnable/externalGate",mod_num);
	status =
		db_get_value(hDB, 0,str, &extGate_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read ADCEvent_Configuration");
	}

	gEventConfiguration[0].ADC1ExternalGateEnable = extGate_odb;


	size = sizeof(intGate_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/GateEnable/internalGate",mod_num);
	status =
		db_get_value(hDB, 0,str , &intGate_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read ADCEvent_Configuration");
	}

	gEventConfiguration[0].ADC1InternalGateEnable = intGate_odb;


	size = sizeof(inputinvert_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/TriggerEnable/ADCInputInvert",mod_num);
	status =
		db_get_value(hDB, 0,str , &inputinvert_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read ADCEvent_Configuration");
	}

	gEventConfiguration[0].ADC1InputInvert = inputinvert_odb;


	DWORD adc_header_odb[8];

	size = sizeof(adc_header_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/ADCHeader/ADC_ID",mod_num);
	status =
		db_get_value(hDB, 0,str, &adc_header_odb, &size, TID_DWORD, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read ADCEvent_Configuration");
	}

	int i = 0;
	for(i=0;i<8;i++){
		gADCHeaderID[i] = adc_header_odb[i];
	}


	uint32_t event_conf=0x0;
	//uint32_t adc_id = (SIS3302_BASE+0xf8000000);
	//uint32_t adc_id = (0xad000000);
	event_conf += adc_header_odb[0];

	if(gEventConfiguration[0].ADC1InputInvert == 1)
		event_conf += EVENT_CONF_ADC1_INPUT_INVERT_BIT;

	if(gEventConfiguration[0].ADC1InternalTriggerEnable == 1 && gEventConfiguration[0].ADC1ExternalTriggerEnable == 0 )
		event_conf += EVENT_CONF_ADC1_INTERN_TRIGGER_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalTriggerEnable == 0 && gEventConfiguration[0].ADC1ExternalTriggerEnable == 1 )
		event_conf += EVENT_CONF_ADC1_EXTERN_TRIGGER_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalGateEnable == 1 && gEventConfiguration[0].ADC1ExternalGateEnable == 0)
		event_conf += EVENT_CONF_ADC1_INTERN_GATE_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalGateEnable == 0 && gEventConfiguration[0].ADC1ExternalGateEnable == 1)
		event_conf += EVENT_CONF_ADC1_EXTERN_GATE_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InputInvert == 1)
		event_conf += EVENT_CONF_ADC2_INPUT_INVERT_BIT;

	if(gEventConfiguration[0].ADC1InternalTriggerEnable == 1 && gEventConfiguration[0].ADC1ExternalTriggerEnable == 0 )
		event_conf += EVENT_CONF_ADC2_INTERN_TRIGGER_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalTriggerEnable == 0 && gEventConfiguration[0].ADC1ExternalTriggerEnable == 1 )
		event_conf += EVENT_CONF_ADC2_EXTERN_TRIGGER_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalGateEnable == 1 && gEventConfiguration[0].ADC1ExternalGateEnable == 0)
		event_conf += EVENT_CONF_ADC2_INTERN_GATE_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalGateEnable == 0 && gEventConfiguration[0].ADC1ExternalGateEnable == 1)
		event_conf += EVENT_CONF_ADC2_EXTERN_GATE_ENABLE_BIT;


	//WriteRegister
	//sis3320_RegisterWrite(k600vme,SIS3302_BASE,SIS3302_EVENT_CONFIG_ALL_ADC,event_conf);
	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_EVENT_CONFIG_ADC12,event_conf);
	
	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_EVENT_CONFIG_ADC34,event_conf);
	
	event_conf=0x0;
	//uint32_t adc_id = (SIS3302_BASE+0xf8000000);
	//uint32_t adc_id = (0xad000000);
	event_conf += adc_header_odb[0];


	gEventConfiguration[0].ADC1InputInvert = 0;	
	if(gEventConfiguration[0].ADC1InputInvert == 1)
		event_conf += EVENT_CONF_ADC1_INPUT_INVERT_BIT;

	if(gEventConfiguration[0].ADC1InternalTriggerEnable == 1 && gEventConfiguration[0].ADC1ExternalTriggerEnable == 0 )
		event_conf += EVENT_CONF_ADC1_INTERN_TRIGGER_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalTriggerEnable == 0 && gEventConfiguration[0].ADC1ExternalTriggerEnable == 1 )
		event_conf += EVENT_CONF_ADC1_EXTERN_TRIGGER_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalGateEnable == 1 && gEventConfiguration[0].ADC1ExternalGateEnable == 0)
		event_conf += EVENT_CONF_ADC1_INTERN_GATE_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalGateEnable == 0 && gEventConfiguration[0].ADC1ExternalGateEnable == 1)
		event_conf += EVENT_CONF_ADC1_EXTERN_GATE_ENABLE_BIT;


	gEventConfiguration[0].ADC1InputInvert = 0;	
	if(gEventConfiguration[0].ADC1InputInvert == 1)
		event_conf += EVENT_CONF_ADC2_INPUT_INVERT_BIT;

	if(gEventConfiguration[0].ADC1InternalTriggerEnable == 1 && gEventConfiguration[0].ADC1ExternalTriggerEnable == 0 )
		event_conf += EVENT_CONF_ADC2_INTERN_TRIGGER_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalTriggerEnable == 0 && gEventConfiguration[0].ADC1ExternalTriggerEnable == 1 )
		event_conf += EVENT_CONF_ADC2_EXTERN_TRIGGER_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalGateEnable == 1 && gEventConfiguration[0].ADC1ExternalGateEnable == 0)
		event_conf += EVENT_CONF_ADC2_INTERN_GATE_ENABLE_BIT;

	if(gEventConfiguration[0].ADC1InternalGateEnable == 0 && gEventConfiguration[0].ADC1ExternalGateEnable == 1)
		event_conf += EVENT_CONF_ADC2_EXTERN_GATE_ENABLE_BIT;


	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_EVENT_CONFIG_ADC56,event_conf);
	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_EVENT_CONFIG_ADC78,event_conf);
	
	//ReadBack
	event_conf = 0x0;
	event_conf = sis3320_RegisterRead(k600vme, module_addr, SIS3302_EVENT_CONFIG_ADC12);

	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," --SIS[%d]-----   Event Config Register ADC 1/2 ------------ : 0x%x ",mod_num,event_conf);
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC HEADER ID  				: 0x%x ",(event_conf&0xfffe0000));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 Input invert bit				: %d ",(event_conf&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 Internal Trigger Enable(asynchronous) 	: %d ",((event_conf>>2)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 External Trigger Enable(synchronous) 	: %d ",((event_conf>>3)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 Internal Gate Enable  			: %d ",((event_conf>>4)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 External Gate Enable  			: %d ",((event_conf>>5)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 ADC N - 1 Next Neighbour gate enable  	: %d ",((event_conf>>6)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 1 ADC N + 1 Next Neighbour gate enable  	: %d ",((event_conf>>7)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 2 Input invert bit				: %d ",((event_conf>>8)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 2 Internal Trigger Enable(asynchronous) 	: %d ",((event_conf>>10)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 2 External Trigger Enable(synchronous) 	: %d ",((event_conf>>11)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 2 Internal Gate Enable  			: %d ",((event_conf>>12)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 2 External Gate Enable  			: %d ",((event_conf>>13)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 2 ADC N - 1 Next Neighbour gate enable  	: %d ",((event_conf>>14)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 2 ADC N + 1 Next Neighbour gate enable  	: %d ",((event_conf>>15)&0x1));

	event_conf = sis3320_RegisterRead(k600vme, module_addr, SIS3302_EVENT_CONFIG_ADC34);

	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," --SIS[%d]-----   Event Config Register ADC 3/4 ------------ : 0x%x ",mod_num,event_conf);
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC HEADER ID  				: 0x%x ",(event_conf&0xfffe0000));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 3 Input invert bit				: %d ",(event_conf&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 3 Internal Trigger Enable(asynchronous) 	: %d ",((event_conf>>2)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 3 External Trigger Enable(synchronous) 	: %d ",((event_conf>>3)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 3 Internal Gate Enable  			: %d ",((event_conf>>4)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 3 External Gate Enable  			: %d ",((event_conf>>5)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 3 ADC N - 1 Next Neighbour gate enable  	: %d ",((event_conf>>6)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 3 ADC N + 1 Next Neighbour gate enable  	: %d ",((event_conf>>7)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 4 Input invert bit				: %d ",((event_conf>>8)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 4 Internal Trigger Enable(asynchronous) 	: %d ",((event_conf>>10)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 4 External Trigger Enable(synchronous) 	: %d ",((event_conf>>11)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 4 Internal Gate Enable  			: %d ",((event_conf>>12)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 4 External Gate Enable  			: %d ",((event_conf>>13)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 4 ADC N - 1 Next Neighbour gate enable  	: %d ",((event_conf>>14)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 4 ADC N + 1 Next Neighbour gate enable  	: %d ",((event_conf>>15)&0x1));

	event_conf = sis3320_RegisterRead(k600vme,module_addr, SIS3302_EVENT_CONFIG_ADC56);

	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," --SIS[%d]---   Event Config Register ADC 5/6 ------------ : 0x%x ",mod_num,event_conf);
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC HEADER ID  				: 0x%x ",(event_conf&0xfffe0000));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 5 Input invert bit				: %d ",(event_conf&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 5 Internal Trigger Enable(asynchronous) 	: %d ",((event_conf>>2)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 5 External Trigger Enable(synchronous) 	: %d ",((event_conf>>3)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 5 Internal Gate Enable  			: %d ",((event_conf>>4)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 5 External Gate Enable  			: %d ",((event_conf>>5)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 5 ADC N - 1 Next Neighbour gate enable  	: %d ",((event_conf>>6)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 5 ADC N + 1 Next Neighbour gate enable  	: %d ",((event_conf>>7)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 6 Input invert bit				: %d ",((event_conf>>8)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 6 Internal Trigger Enable(asynchronous) 	: %d ",((event_conf>>10)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 6 External Trigger Enable(synchronous) 	: %d ",((event_conf>>11)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 6 Internal Gate Enable  			: %d ",((event_conf>>12)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 6 External Gate Enable  			: %d ",((event_conf>>13)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 6 ADC N - 1 Next Neighbour gate enable  	: %d ",((event_conf>>14)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 6 ADC N + 1 Next Neighbour gate enable  	: %d ",((event_conf>>15)&0x1));

	event_conf = sis3320_RegisterRead(k600vme, module_addr, SIS3302_EVENT_CONFIG_ADC78);

	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," --SIS[%d]---   Event Config Register ADC 7/8 ------------ : 0x%x ",mod_num,event_conf);
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC HEADER ID  				: 0x%x ",(event_conf&0xfffe0000));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 7 Input invert bit				: %d ",(event_conf&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 7 Internal Trigger Enable(asynchronous) 	: %d ",((event_conf>>2)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 7 External Trigger Enable(synchronous) 	: %d ",((event_conf>>3)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 7 Internal Gate Enable  			: %d ",((event_conf>>4)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 7 External Gate Enable  			: %d ",((event_conf>>5)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 7 ADC N - 1 Next Neighbour gate enable  	: %d ",((event_conf>>6)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 7 ADC N + 1 Next Neighbour gate enable  	: %d ",((event_conf>>7)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 8 Input invert bit				: %d ",((event_conf>>8)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 8 Internal Trigger Enable(asynchronous) 	: %d ",((event_conf>>10)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 8 External Trigger Enable(synchronous) 	: %d ",((event_conf>>11)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 8 Internal Gate Enable  			: %d ",((event_conf>>12)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 8 External Gate Enable  			: %d ",((event_conf>>13)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 8 ADC N - 1 Next Neighbour gate enable  	: %d ",((event_conf>>14)&0x1));
	cm_msg(MINFO,"sis3302_ADCEvent_Configuration"," ADC 8 ADC N + 1 Next Neighbour gate enable  	: %d ",((event_conf>>15)&0x1));



}//sis3302_EventConfiguration


void sis3302_EndThreshold_Register(DWORD module_addr, int mod_num)
{
	
	int status;
	int endaddr[4];
	int size = sizeof(endaddr);
	char str[80];

	sprintf(str,"/Analyzer/Parameters/sis3302_%d/RAW/EndAddressThreshold",mod_num);
	status =
		db_get_value(hDB, 0,str, &endaddr, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read EndThresholdRegister ");
	}

	int i = 0;
	for(i<0;i<4;i++) {

		gEndAddressThreshold[i] = (endaddr[i]<<2);
		//gEndAddressThreshold = 0x1c0;


//	 sis3320_RegisterWrite(k600vme,SIS3302_BASE, SIS3302_END_ADDRESS_THRESHOLD_ADC12,gEndAddressThreshold);
//	 sis3320_RegisterWrite(k600vme,SIS3302_BASE, SIS3302_END_ADDRESS_THRESHOLD_ALL_ADC,gEndAddressThreshold[0]);
//
	 switch(i) {
		 case 0: sis3320_RegisterWrite(k600vme,module_addr, SIS3302_END_ADDRESS_THRESHOLD_ADC12,gEndAddressThreshold[i]);
			 break;
		 case 1: sis3320_RegisterWrite(k600vme,module_addr, SIS3302_END_ADDRESS_THRESHOLD_ADC34,gEndAddressThreshold[i]);
			 break;
		 case 2: sis3320_RegisterWrite(k600vme,module_addr, SIS3302_END_ADDRESS_THRESHOLD_ADC56,gEndAddressThreshold[i]);
			 break;
		 case 3: sis3320_RegisterWrite(k600vme,module_addr, SIS3302_END_ADDRESS_THRESHOLD_ADC78,gEndAddressThreshold[i]);
			 break;
	 };//switch

	}//For

	//ReadBack
         cm_msg(MINFO,"sis3302_EndThreshold_Register"," -----SIS Module [%d]--------- END ADDRESS THRESHOLD ALL ADC ----------- ",mod_num);
	
	 uint32_t gend;
	 for(i=0;i<4;i++) {
		switch(i) {
			case 0:	 gend = sis3320_RegisterRead(k600vme, module_addr, SIS3302_END_ADDRESS_THRESHOLD_ADC12);
				 break;
			case 1:	 gend = sis3320_RegisterRead(k600vme, module_addr, SIS3302_END_ADDRESS_THRESHOLD_ADC34);
				 break;
			case 2:	 gend = sis3320_RegisterRead(k600vme, module_addr, SIS3302_END_ADDRESS_THRESHOLD_ADC56);
				 break;
			case 3:	 gend = sis3320_RegisterRead(k600vme, module_addr, SIS3302_END_ADDRESS_THRESHOLD_ADC78);
				 break;
		};//switch

		 cm_msg(MINFO,"sis3302_EndThreshold_Register","[%d] End address Threshold value 			: [0x%08x] ",i,(gend>>2)&0x3fffff);
		 cm_msg(MINFO,"sis3302_EndThreshold_Register","[%d] End address Threshold value 	ODB		: [0x%08x] ",i,(gEndAddressThreshold[i]>>2));

	 }//for
}//sis3302_EndThreshold_Register



void sis3302_TriggerThreshold(DWORD module_addr, int mod_num)
{

	int size,status;

	int trap_thres_odb[8],trig_gt_mode_odb[8],disable_trig_odb[8],ext_trig_mode_odb[8];
	int ext_trap_thres_odb[8];
	char str[80];

	size = sizeof(trap_thres_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/TriggerThreshold/Threshold",mod_num);
	status =
		db_get_value(hDB, 0, str , &trap_thres_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_TriggerThreshold","Cannot read Threshold odb");
	}
	size = sizeof(trig_gt_mode_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/TriggerThreshold/GTMode",mod_num);
	status =
		db_get_value(hDB, 0, str,&trig_gt_mode_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_TriggerThreshold","Cannot read GTMode odb");
	}
	size = sizeof(disable_trig_odb);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/TriggerThreshold/DisableTriggerOut",mod_num); 
	status =
		db_get_value(hDB, 0,str,&disable_trig_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_TriggerThreshold","Cannot read disable_trig_odb");
	}
	size = sizeof(ext_trig_mode_odb);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/TriggerThreshold/ExtThresholdMode",mod_num); 
	status =
		db_get_value(hDB, 0,str,&ext_trig_mode_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_TriggerThreshold","Cannot read ext_trig_mode_odb");
	}
	size = sizeof(ext_trap_thres_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/TriggerThreshold/ExtThreshold",mod_num); 
	status =
		db_get_value(hDB, 0,str,&ext_trap_thres_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_TriggerThreshold","Cannot read ext_trap_thres_odb");
	}
	
	int i = 0;
	for(i=0;i<SISCount;i++){
		gTriggerThreshold[i].FIRTrapThreshold = trap_thres_odb[i];
		gTriggerThreshold[i].FIRExtTrapThreshold = ext_trap_thres_odb[i];
		gTriggerThreshold[i].FIRDisableTrigOut = disable_trig_odb[i];
		gTriggerThreshold[i].FIRTriggerModeGT = trig_gt_mode_odb[i];
		gTriggerThreshold[i].FIRExtThresholdMode = ext_trig_mode_odb[i];

		DWORD trap_s =  (gTriggerThreshold[i].FIRTrapThreshold&0x1ffff) + 0x10000;	

		DWORD trigthres_set = ((gTriggerThreshold[i].FIRDisableTrigOut&0x1)<<26)
			+((gTriggerThreshold[i].FIRTriggerModeGT&0x1)<<25)
			+((gTriggerThreshold[i].FIRExtThresholdMode&0x1)<<23)
			+ trap_s;

		switch(i){
			case 0:	sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_THRESHOLD_ADC1,trigthres_set);
					break;
			case 1:	sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_THRESHOLD_ADC2,trigthres_set);
					break;
			case 2:	sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_THRESHOLD_ADC3,trigthres_set);
					break;
			case 3:	sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_THRESHOLD_ADC4,trigthres_set);
					break;
			case 4:	sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_THRESHOLD_ADC5,trigthres_set);
					break;
			case 5:	sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_THRESHOLD_ADC6,trigthres_set);
					break;
			case 6:	sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_THRESHOLD_ADC7,trigthres_set);
					break;
			case 7:	sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_THRESHOLD_ADC8,trigthres_set);
					break;
		};//switch


		if( gTriggerThreshold[i].FIRExtThresholdMode == 1 ) {
			DWORD extTrigThres = (gTriggerThreshold[i].FIRExtTrapThreshold&0x3ffffff) + 0x2000000;
			switch(i) {
				case 0 : sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_EXTENDED_THRESHOLD_ADC1,trigthres_set);
						 break;
				case 1 : sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_EXTENDED_THRESHOLD_ADC2,trigthres_set);
						 break;
				case 2 : sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_EXTENDED_THRESHOLD_ADC3,trigthres_set);
						 break;
				case 3 : sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_EXTENDED_THRESHOLD_ADC4,trigthres_set);
						 break;
				case 4 : sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_EXTENDED_THRESHOLD_ADC5,trigthres_set);
						 break;
				case 5 : sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_EXTENDED_THRESHOLD_ADC6,trigthres_set);
						 break;
				case 6 : sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_EXTENDED_THRESHOLD_ADC7,trigthres_set);
						 break;
				case 7 : sis3320_RegisterWrite(k600vme,module_addr, SIS3302_TRIGGER_EXTENDED_THRESHOLD_ADC8,trigthres_set);
						 break;
			};//switch	 
		}//if
	}//FOR

	// Read Back
	for(i=0;i<SISCount;i++) {	
		DWORD trig_thres = 0x0;
		switch(i) {
			case 0:	trig_thres = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_THRESHOLD_ADC1);
					break;
			case 1:	trig_thres = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_THRESHOLD_ADC2);
					break;
			case 2:	trig_thres = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_THRESHOLD_ADC3);
					break;
			case 3:	trig_thres = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_THRESHOLD_ADC4);
					break;
			case 4:	trig_thres = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_THRESHOLD_ADC5);
					break;
			case 5:	trig_thres = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_THRESHOLD_ADC6);
					break;
			case 6:	trig_thres = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_THRESHOLD_ADC7);
					break;
			case 7:	trig_thres = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_THRESHOLD_ADC8);
					break;

		};

		uint32_t trap_value = (trig_thres)&0xffff;
		uint32_t cfd_control = (trig_thres>>20)&0x7;
		uint32_t ext_thres_mode = (trig_thres>>23)&0x1;
		uint32_t trigger_mode_gt = (trig_thres>>25)&0x1;
		uint32_t disable_trigger = (trig_thres>>26)&0x1;


		cm_msg(MINFO,"sis3302_TriggerThreshold","--SIS[%d]---- Trigger Threshold ADC %d --------------",mod_num,i);
		cm_msg(MINFO,"sis3302_TriggerThreshold","Trapezoidal threshold value	: [%d]",trap_value);
		cm_msg(MINFO,"sis3302_TriggerThreshold","CFD Control bits	    		: [%d]",cfd_control);
		cm_msg(MINFO,"sis3302_TriggerThreshold","Ext_Threshold_Mode			: [%d]",ext_thres_mode);
		cm_msg(MINFO,"sis3302_TriggerThreshold","Trigger Mode GT				: [%d]",trigger_mode_gt);
		cm_msg(MINFO,"sis3302_TriggerThreshold","Disable Trigger Output		: [%d]",disable_trigger);

		switch(cfd_control)
		{
			case 0:cm_msg(MINFO,"sis3302_TriggerThreshold","CFD Function Disabled");break;
			case 1:cm_msg(MINFO,"sis3302_TriggerThreshold","CFD Function Disabled");break;
			case 2:cm_msg(MINFO,"sis3302_TriggerThreshold","CFD Function enabled with 75%% ");break;
			case 3:cm_msg(MINFO,"sis3302_TriggerThreshold","CFD Function enabled with 50%% ");break;
			default:
				   cm_msg(MINFO,"sis3302_TriggerThreshold"," Unknown CFD Control Value ");
		};


		int32_t temp_adc_counts=0;
		int32_t adcflattime =0;
		if(ext_thres_mode == 0 ){

			uint32_t val = (gTriggerSetup[i].PeakingTime&0xffff);	
			if( val < 16){
				gshift_factor = 16;	
			}else if( val > 15 && val < 32 )	
			{
				gshift_factor = 32;
			} else if( val > 31 && val < 64 )	
			{
				gshift_factor = 64;
			} else if( val > 63 && val < 128 )	
			{
				gshift_factor = 128;
			} else if( val > 127 && val < 256 )	
			{
				gshift_factor = 512;
			} else if( val > 255 && val < 512 )	
			{
				gshift_factor = 1024;
			} 

			if( gTriggerSetup[i].PeakingTime != 0 )
				temp_adc_counts = (gshift_factor*gTriggerThreshold[i].FIRTrapThreshold)/ (gTriggerSetup[i].PeakingTime&0xffff);

			adcflattime = gTriggerSetup[i].SumGapTime - gTriggerSetup[i].PeakingTime;

			cm_msg(MINFO,"sis3302_TriggerThreshold"," ---=== Extended Threshold Mode = [%d] == SIS [%d]  ----- ",ext_thres_mode,mod_num);
			cm_msg(MINFO,"sis3302_TriggerThreshold"," 25 bit running sum is shifted to the right by : [%d] , Peaking Time [%d] ",
					gshift_factor,gTriggerSetup[i].PeakingTime);
			cm_msg(MINFO,"sis3302_TriggerThreshold"," FIR Trigger ADC Counts : [%d] ",temp_adc_counts);
			cm_msg(MINFO,"sis3302_TriggerThreshold"," Flat Time: gSumGapTime[%d] - gPeakingTime[%d] = [%d] ", 
					gTriggerSetup[i].SumGapTime,gTriggerSetup[i].PeakingTime,adcflattime);	

			//update ODB
			sprintf(str, "/Analyzer/Parameters/sis3302_%d/Fir Trigger ADC Counts",mod_num);
			db_set_value(hDB, 0,str , &temp_adc_counts, sizeof(temp_adc_counts), 1, TID_INT);
			sprintf(str,"/Analyzer/Parameters/sis3302_%d/Flat Time",mod_num);
			db_set_value(hDB, 0,str,&adcflattime, sizeof(adcflattime), 1, TID_INT);
			sprintf(str,"/Analyzer/Parameters/sis3302_%d/Shift Factor",mod_num);
			db_set_value(hDB, 0,str, &gshift_factor, sizeof(gshift_factor), 1, TID_INT);
		}//if
	}//FOR
}//sis3302_TriggerThreshold


void sis3302_TriggerSetup(DWORD module_addr, int mod_num)
{
	uint32_t trig_setup;
	uint32_t exttrig_setup;
	int status,size;
	
	int peak_val_odb[8];
	int sumg_odb[8];
	int internal_gate_length_odb[8];
	int internal_trigger_delay_odb[8];
	int internal_trigger_pulse_length_odb[8];
	int trigger_decimation_odb[8];

	char str[80];
	size = sizeof(peak_val_odb);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/PeakingTime",mod_num); 
	status =
		db_get_value(hDB, 0, str,&peak_val_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_TriggerSetup","Cannot read peaking time odb");
	}
	size = sizeof(sumg_odb);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/SumG",mod_num); 
	status =
		db_get_value(hDB, 0,str,&sumg_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_TriggerSetup","Cannot read sumg odb ");
	}
	size = sizeof(trigger_decimation_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/Decimation",mod_num);
	status =
		db_get_value(hDB, 0, str,&trigger_decimation_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_TriggerSetup","Cannot read trigger decimation odb");
	}
	size = sizeof(internal_trigger_pulse_length_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/InternalSetup/internalTrigPulseLength",mod_num); 
	status =
		db_get_value(hDB, 0,str,&internal_trigger_pulse_length_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_TriggerSetup","Cannot read internal trigger pulse length odb");
	}
	size = sizeof(internal_gate_length_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/InternalSetup/internalGateLength",mod_num); 
	status =
		db_get_value(hDB, 0,str,&internal_gate_length_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_TriggerSetup","Cannot read internal gate length odb");
	}
	size = sizeof(internal_trigger_delay_odb);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/TriggerSetup/InternalSetup/internalTriggerDelay",mod_num); 
	status =
		db_get_value(hDB, 0,str,&internal_trigger_delay_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_TriggerSetup","Cannot read internal trigger delay odb");
	}


	int i = 0;
	for( i=0; i < SISCount;i++) {
		gTriggerSetup[i].PeakingTime = peak_val_odb[i];
		gTriggerSetup[i].SumGapTime = sumg_odb[i];
		gTriggerSetup[i].TriggerDecimationMode = trigger_decimation_odb[i];
		gTriggerSetup[i].InternalTriggerPulseLength = internal_trigger_pulse_length_odb[i];
		gTriggerSetup[i].InternalGateLength = internal_gate_length_odb[i];
		gTriggerSetup[i].InternalTriggerDelay = internal_trigger_delay_odb[i];


		uint32_t sumg_val_lower = gTriggerSetup[i].SumGapTime&0xff;
		uint32_t sumg_val_upper = (gTriggerSetup[i].SumGapTime>>8)&0x1;

		uint32_t peak_val_lower = gTriggerSetup[i].PeakingTime&0xff;
		uint32_t peak_val_upper = (gTriggerSetup[i].PeakingTime>>8)&0x1;


		trig_setup = ((gTriggerSetup[i].InternalGateLength &0x3f)<<24)+
			((gTriggerSetup[i].InternalTriggerPulseLength &0xff)<<16)  + 
			( (sumg_val_lower)<<8) + (peak_val_lower) ;

		exttrig_setup = ((gTriggerSetup[i].InternalTriggerDelay&0x1f)<<24) + 
			((gTriggerSetup[i].TriggerDecimationMode&0x7)<<16) 
			+ ((sumg_val_upper)<<8) + peak_val_upper;

		//cm_msg(MINFO,"sis3302_ADC1_Trigger_Setup_Register","Trigger Setup After Setting: 0x%x ",trig_setup);
		//write
		switch(i) {
			case 0: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_SETUP_ADC1,trig_setup); 
					sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_EXTENDED_SETUP_ADC1,exttrig_setup); 
					break;
			case 1: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_SETUP_ADC2,trig_setup); 
					sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_EXTENDED_SETUP_ADC2,exttrig_setup); 
					break;
			case 2: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_SETUP_ADC3,trig_setup); 
					sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_EXTENDED_SETUP_ADC3,exttrig_setup); 
					break;
			case 3: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_SETUP_ADC4,trig_setup); 
					sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_EXTENDED_SETUP_ADC4,exttrig_setup); 
					break;
			case 4: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_SETUP_ADC5,trig_setup); 
					sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_EXTENDED_SETUP_ADC5,exttrig_setup); 
					break;
			case 5: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_SETUP_ADC6,trig_setup); 
					sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_EXTENDED_SETUP_ADC6,exttrig_setup); 
					break;
			case 6: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_SETUP_ADC7,trig_setup); 
					sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_EXTENDED_SETUP_ADC7,exttrig_setup); 
					break;
			case 7: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_SETUP_ADC8,trig_setup); 
					sis3320_RegisterWrite(k600vme,module_addr,SIS3302_TRIGGER_EXTENDED_SETUP_ADC8,exttrig_setup); 
					break;
		};//switch
	}//for

	//read
	for(i=0;i<SISCount;i++) {
		trig_setup = 0x0;
		exttrig_setup = 0x0;

		switch(i) {
			case 0 :trig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_SETUP_ADC1);
					exttrig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_EXTENDED_SETUP_ADC1);
					break;
			case 1 :trig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_SETUP_ADC2);
					exttrig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_EXTENDED_SETUP_ADC2);
					break;
			case 2 :trig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_SETUP_ADC3);
					exttrig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_EXTENDED_SETUP_ADC3);
					break;
			case 3 :trig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_SETUP_ADC4);
					exttrig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_EXTENDED_SETUP_ADC4);
					break;
			case 4 :trig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_SETUP_ADC5);
					exttrig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_EXTENDED_SETUP_ADC5);
					break;
			case 5 :trig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_SETUP_ADC6);
					exttrig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_EXTENDED_SETUP_ADC6);
					break;
			case 6 :trig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_SETUP_ADC7);
					exttrig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_EXTENDED_SETUP_ADC7);
					break;
			case 7 :trig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_SETUP_ADC8);
					exttrig_setup = sis3320_RegisterRead(k600vme, module_addr, SIS3302_TRIGGER_EXTENDED_SETUP_ADC8);
					break;
		};
		uint32_t peaking_time = (trig_setup&0xff) + (((exttrig_setup&0x1)<<8));
		uint32_t sumg = ((trig_setup>>8)&0xff) + (((exttrig_setup>>8)&0x1)<<8);
		uint32_t trig_pulse_length = (trig_setup>>16)&0xff;
		uint32_t gate_length = (trig_setup>>24)&0x3f;


		cm_msg(MINFO,"sis3302_TriggerSetup","-----SIS [%d] ------- TRIGGER SETUP REGISTER ADC[%d] -----------------",mod_num,i);
		cm_msg(MINFO,"sis3302_TriggerSetup","peaking time				: [%d]",peaking_time);
		cm_msg(MINFO,"sis3302_TriggerSetup","sumg time   				: [%d]",sumg);
		cm_msg(MINFO,"sis3302_TriggerSetup","Internal Pulse Length 			: [%d]",trig_pulse_length);
		cm_msg(MINFO,"sis3302_TriggerSetup","Internal Gate  Length 			: [%d]",gate_length);

		uint32_t xpeaking_time = (exttrig_setup&0x1);
		uint32_t xsumg = (exttrig_setup>>8)&0x1;
		uint32_t trig_dec = (exttrig_setup>>16)&0x7;
		uint32_t intTrigDelay = (exttrig_setup>>24)&0x1f;


		cm_msg(MINFO,"sis3302_TriggerExtendedSetup_Register","------SIS [%d] ----- TRIGGER EXTENDED SETUP REGISTER ADC[%d] -----------------",mod_num,i);
		cm_msg(MINFO,"sis3302_TriggerExtendedSetup_Register","Internal Trigger Delay 			: [%d]",intTrigDelay);
		cm_msg(MINFO,"sis3302_TriggerExtendedSetup_Register","Trigger Decimation Mode			: [%d]",trig_dec);
		cm_msg(MINFO,"sis3302_TriggerExtendedSetup_Register","SumG (upper value)				: [%d]",xsumg);
		cm_msg(MINFO,"sis3302_TriggerExtendedSetup_Register","Peaking Time ( upper value)		: [%d]",xpeaking_time);

	}//for
}//sis3302_TriggerSetup

void sis3302_MCAScanNofHistogramsPreset_Register(DWORD module_addr, int mod_num)
{
	
	sis3320_RegisterWrite(k600vme,module_addr,0x80,gMCAScanNofHistos);

	uint32_t nof_histos  = sis3320_RegisterRead(k600vme, module_addr,0x80);


	cm_msg(MINFO,"sis3302_MCAScanNofHistogramsPreset_Register"," -------SIS [%d]--------- MCA Scan Nof Histograms Preset Register -------------- ",mod_num);
	cm_msg(MINFO,"sis3302_MCAScanNofHistogramsPreset_Register"," MCA Scan Nof Histograms preset  		: 0x%x [%d] ",nof_histos,nof_histos);

}

void sis3302_MCAScanLNEPrescaleFactor_Register(DWORD module_addr, int mod_num)
{
	
	DWORD datum = ((gMCALNEPrescaleSetup.LNESource&0x1)<<28) + (gMCALNEPrescaleSetup.PrescaleFactor&0xfffffff);

	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_MCA_SCAN_SETUP_PRESCALE_FACTOR,datum);

	DWORD factor_reg = sis3320_RegisterRead(k600vme, module_addr,SIS3302_MCA_SCAN_SETUP_PRESCALE_FACTOR);

	DWORD lne_source_bit = (factor_reg>>28)&0x1;
	DWORD lne_prescale   = (factor_reg&0xfffffff);
	
	cm_msg(MINFO,"sis3302_MCAScanLNEPrescaleFactor_Register"," --------SIS [%d] -------- MCA Scan LNE Setup And Prescale Factor  Register ----- ",mod_num);
	cm_msg(MINFO,"sis3302_MCAScanLNEPrescaleFactor_Register"," LNE source bit		   		: 0x%x [%d] ",lne_source_bit,lne_source_bit);
	cm_msg(MINFO,"sis3302_MCAScanLNEPrescaleFactor_Register"," LNE Prescale Factor 			: 0x%x [%d] ",lne_prescale,lne_prescale);



}


void sis3302_MCAScanControl_Register()
{




}



void sis3302_RawDataBuffer_Configuration(DWORD module_addr, int mod_num)
{

	int status;
	int rawlength;
	int rawstart;
	char str[80];
	int size = sizeof(rawlength);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/RAW/RawDataSampleLength",mod_num);
	status =
		db_get_value(hDB, 0,str,&rawlength, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_RawDataBuffer","Cannot read RawDataBuffer_Configuration ");
	}
	size = sizeof(rawstart);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/RAW/RawDataSampleStart",mod_num);
	status =
		db_get_value(hDB, 0, str,&rawstart, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_RawDataBuffer","Cannot read RawDataBuffer_Configuration ");
	}

	if( rawlength >= 0 )
		gRawDataBufferConfig[0].SampleLength = rawlength;

	
	if( rawstart >= 0)
		gRawDataBufferConfig[0].SampleStartIndex = rawstart;

	
	uint32_t sample_length 		    = (gRawDataBufferConfig[0].SampleLength & 0xfffc) << 16;
	uint32_t sample_start_index	    = gRawDataBufferConfig[0].SampleStartIndex & 0xfffe;


	uint32_t rawdata_reg = sample_length + sample_start_index;

	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_RAW_DATA_BUFFER_ALL_ADC,rawdata_reg);

	rawdata_reg = 0x0;
	rawdata_reg = sis3320_RegisterRead(k600vme,module_addr,SIS3302_RAW_DATA_BUFFER_CONFIG_ADC12);
	cm_msg(MINFO,"sis3302_RawDataBuffer_Configuration"," -----SIS Module[%d]--------- RAW DATA BUFFER CONFIGURATION --------------------- ",mod_num);
	cm_msg(MINFO,"sis3302_RawDataBuffer_Configuration"," Raw Data Sample Length (quad sample aligned)	: [%d]",( (rawdata_reg>>16)&0xfffc));
	cm_msg(MINFO,"sis3302_RawDataBuffer_Configuration"," Raw Data Sample Start Index (even values only)	: [%d]",( rawdata_reg & 0xfffe ));
	cm_msg(MINFO,"sis3302_RawDataBuffer_Configuration"," Raw Data Sample Start Index ODB			: [%d]",rawstart);
	cm_msg(MINFO,"sis3302_RawDataBuffer_Configuration"," Raw Data Sample Length ODB				: [%d]",rawlength);

}//sis3302_RawDataBuffer_Configuration

void sis3302_EnergyTauFactor_Register(DWORD module_addr,int mod_num)
{

	int status;
	int etau_odb;
	char str[80];
	int size = sizeof(etau_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/EnergySetup/Tau",mod_num); 
	status =
		db_get_value(hDB, 0,str,&etau_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read EnergyTauFactor_Register ");
	}

	if( etau_odb >= 0 )
		gFIRTauFactor[0] = etau_odb;


	uint32_t datum = gFIRTauFactor[0]&0x3f;

	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_ENERGY_TAU_FACTOR_ADC1,datum);


	uint32_t etau = sis3320_RegisterRead(k600vme, module_addr,SIS3302_ENERGY_TAU_FACTOR_ADC1);


	cm_msg(MINFO,"sis3302_EnergyTauFactor_Register"," -----SIS [%d] ---------- Energy Tau Factor Register -------------- ",mod_num);
	cm_msg(MINFO,"sis3302_EnergyTauFactor_Register"," Energy Tau Factor			: 0x%x [%d] ",etau&0x3f,etau&0x3f);


}


void sis3302_EnergyGateLength_Register(DWORD module_addr, int mod_num)
{

	int status;
	int egatelength;
	char str[80];
	int size = sizeof(egatelength);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/EnergySetup/GateLength",mod_num);
	status =
		db_get_value(hDB, 0, str,&egatelength, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_EnergyGateLength","Cannot read EnergyGateLength_Register ");
	}

	if( egatelength >= 0 )
		gEnergyReadOut[0].GateLength = egatelength;

	uint32_t datum = gEnergyReadOut[0].GateLength&0xffff;

	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_ENERGY_GATE_LENGTH_ALL_ADC,datum);


	uint32_t egatelength_reg = sis3320_RegisterRead(k600vme, module_addr,SIS3302_ENERGY_GATE_LENGTH_ADC12);

	uint32_t gatelength_reg = egatelength_reg&0xffff;

	cm_msg(MINFO,"sis3302_EnergyGateLength_Register"," ----SIS Module[%d] ------ Energy Gate Length Register -------------- ",mod_num);
	cm_msg(MINFO,"sis3302_EnergyGateLength_Register"," Energy Gate Length 			: 0x%x [%d] ",gatelength_reg,gatelength_reg);
}//sis3302_EnergyGateLength_Register



void sis3302_EnergySetupGP_Register(DWORD module_addr,int mod_num)
{

	int status;
	int epeak_odb[4];
	int egap_odb[4],edec_odb[4];
	char str[80];

	int size = sizeof(epeak_odb);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/EnergySetup/PeakingTime",mod_num); 
	status =
		db_get_value(hDB, 0, str,&epeak_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_EnergySetupGP","Cannot read Energy Peaking Times ");
	}

	size = sizeof(egap_odb);
	sprintf(str,  "/Analyzer/Parameters/sis3302_%d/ADC/EnergySetup/GapTime",mod_num); 
	status =
		db_get_value(hDB, 0,str,&egap_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_EnergySetupGP","Cannot read Energy Gap Times ");
	}

	size = sizeof(edec_odb);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/EnergySetup/Decimation",mod_num); 

	status =
		db_get_value(hDB, 0,str,&edec_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_EnergySetupGP","Cannot read Energy Setup Decimation ");
	}

	int i=0;
	for(i=0;i<4;i++) {
		gEnergySetup[i].FIREnergyPeak = epeak_odb[i];
		gEnergySetup[i].FIREnergyGap  = egap_odb[i];
		gEnergySetup[i].FIREnergyDecimation  = edec_odb[i];

		uint32_t datum = (gEnergySetup[i].FIREnergyDecimation<<28) + 
			((gEnergySetup[i].FIREnergyPeak&0x300)<<8) + 
			(gEnergySetup[i].FIREnergyPeak&0xff) + 
			(gEnergySetup[i].FIREnergyGap<<8);

		switch(i) {
			case 0: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_ENERGY_SETUP_GP_ADC12,datum);
					break;
			case 1: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_ENERGY_SETUP_GP_ADC34,datum);
					break;
			case 2: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_ENERGY_SETUP_GP_ADC56,datum);
					break;
			case 3: sis3320_RegisterWrite(k600vme,module_addr,SIS3302_ENERGY_SETUP_GP_ADC78,datum);
					break;
		};//switch
	}//Fori

	//ReadBack
	cm_msg(MINFO,"sis3302_EnergySetupGP","---SIS[%d]---- Energy Setup GP Register --------------",mod_num);
	DWORD energy_gp_reg = 0x0; 
	for(i=0;i<4;i++) {

		switch(i) {
			case 0 : energy_gp_reg = sis3320_RegisterRead(k600vme, module_addr,SIS3302_ENERGY_SETUP_GP_ADC12);
					 break;
			case 1 : energy_gp_reg = sis3320_RegisterRead(k600vme, module_addr,SIS3302_ENERGY_SETUP_GP_ADC34);
					 break;
			case 2 : energy_gp_reg = sis3320_RegisterRead(k600vme, module_addr,SIS3302_ENERGY_SETUP_GP_ADC56);
					 break;
			case 3 : energy_gp_reg = sis3320_RegisterRead(k600vme, module_addr,SIS3302_ENERGY_SETUP_GP_ADC78);
					 break;
		};//switch
		uint32_t epeaking_time_top = (energy_gp_reg>>16)&0x3;
		uint32_t epeaking_time_lower = (energy_gp_reg&0xff);
		uint32_t epeaking_time = (epeaking_time_top<<8) + epeaking_time_lower;

		uint32_t egap_time = (energy_gp_reg>>8)&0xff;

		uint32_t edec_reg  = (energy_gp_reg>>28)&0x3;
		
		cm_msg(MINFO,"sis3302_EnergySetupGP","Energy Setup GP : Group [%d]",i);
		cm_msg(MINFO,"sis3302_EnergySetupGP","Peaking Time [max. 1023]   	: 0x%x [%d]",epeaking_time,epeaking_time);
		cm_msg(MINFO,"sis3302_EnergySetupGP","Gap Time [max. 255] 			: 0x%x [%d]",egap_time,egap_time);
		cm_msg(MINFO,"sis3302_EnergySetupGP","Decimation: 1/2/4/8 			: 0x%x [%d]",edec_reg,edec_reg);
	}//For

}//sis3302_EnergySetupGP_Register



void sis3302_EnergySample_Registers(DWORD module_addr, int mod_num)
{
	int status;
	int elength;
	int esx1,esx2,esx3;
	char str[80];

	int size = sizeof(elength);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/RAW/EnergySampleLength",mod_num);
	status =
		db_get_value(hDB, 0, str,&elength, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_EnergySample","Cannot read EnergySample_Registers ");
	}

	size = sizeof(esx1);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/RAW/EnergySampleStartIndex1",mod_num);
	status =
		db_get_value(hDB, 0,str,&esx1, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_EnergySample","Cannot read EnergySample_Registers ");
	}

	size = sizeof(esx2);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/RAW/EnergySampleStartIndex2",mod_num);
	status =
		db_get_value(hDB, 0, str,&esx2, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_EnergySample","Cannot read EnergySample_Registers ");
	}
	
	size = sizeof(esx3);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/RAW/EnergySampleStartIndex3", mod_num);
	status =
		db_get_value(hDB, 0,str, &esx3, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		cm_msg(MERROR,"sis3302_EnergySample","Cannot read EnergySample_Registers ");
	}
	

	if( elength >= 0 && elength < 511 )
	{
		gEnergySample[0].EnergySampleLength = elength;
	}	

	if( esx1 >= 0 ){
		gEnergySample[0].EnergySampleStartIndex1 = esx1;
	}
	if( esx2 >= 0 ){
		gEnergySample[0].EnergySampleStartIndex2 = esx2;
	}
	if( esx3 >= 0 ){
		gEnergySample[0].EnergySampleStartIndex3 = esx3;
	}

	//Write
	uint32_t datum = gEnergySample[0].EnergySampleLength&0x7ff;
	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_ENERGY_SAMPLE_LENGTH_ALL_ADC,datum);
	datum = gEnergySample[0].EnergySampleStartIndex1&0xffff;
	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_ENERGY_SAMPLE_START_INDEX1_ALL_ADC,datum);
	datum = gEnergySample[0].EnergySampleStartIndex2&0xffff;
	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_ENERGY_SAMPLE_START_INDEX2_ALL_ADC,datum);
	datum = gEnergySample[0].EnergySampleStartIndex3&0xffff;
	sis3320_RegisterWrite(k600vme,module_addr,SIS3302_ENERGY_SAMPLE_START_INDEX3_ALL_ADC,datum);


	//Read Back
	uint32_t energysamplelength_reg  = sis3320_RegisterRead(k600vme, module_addr,SIS3302_ENERGY_SAMPLE_LENGTH_ADC12);
	uint32_t energysamplestartindex1_reg  = sis3320_RegisterRead(k600vme, module_addr,SIS3302_ENERGY_SAMPLE_START_INDEX1_ADC12);
	uint32_t energysamplestartindex2_reg  = sis3320_RegisterRead(k600vme, module_addr,SIS3302_ENERGY_SAMPLE_START_INDEX2_ADC12);
	uint32_t energysamplestartindex3_reg  = sis3320_RegisterRead(k600vme, module_addr,SIS3302_ENERGY_SAMPLE_START_INDEX3_ADC12);

	uint32_t esl = energysamplelength_reg&0x7ff;
	uint32_t essi1 = energysamplestartindex1_reg&0xffff;
	uint32_t essi2 = energysamplestartindex2_reg&0xffff;
	uint32_t essi3 = energysamplestartindex3_reg&0xffff;

	cm_msg(MINFO,"sis3302_EnergySample_Register"," -----SIS Module [%d] ---------- Energy Sample Registers -------------- ",mod_num);
	cm_msg(MINFO,"sis3302_EnergySample_Register"," Energy Sample Length				: 0x%x [%d] ",esl,esl);
	cm_msg(MINFO,"sis3302_EnergySample_Register"," Energy Sample Start Index1 			: 0x%x [%d] ",essi1,essi1);
	cm_msg(MINFO,"sis3302_EnergySample_Register"," Energy Sample Start Index2 			: 0x%x [%d] ",essi2,essi2);
	cm_msg(MINFO,"sis3302_EnergySample_Register"," Energy Sample Start Index3 			: 0x%x [%d] ",essi3,essi3);
	cm_msg(MINFO,"sis3302_EnergySample_Register"," Energy Sample Length			ODB	: 0x%x [%d] ",elength,elength);
	cm_msg(MINFO,"sis3302_EnergySample_Register"," Energy Sample Start Index1 		ODB	: 0x%x [%d] ",esx1,esx1);
	cm_msg(MINFO,"sis3302_EnergySample_Register"," Energy Sample Start Index2 		ODB	: 0x%x [%d] ",esx2,esx2);
	cm_msg(MINFO,"sis3302_EnergySample_Register"," Energy Sample Start Index3 		ODB	: 0x%x [%d] ",esx3,esx3);


}//sis3302_EnergySample_Registers


void sis3302_GetEnergyModeOdb(int mod_num)
{

	int status;
	int emode_odb;
	char str[80];

	int size = sizeof(emode_odb);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/ADC/EnergySetup/Mode",mod_num);
	status =
		db_get_value(hDB, 0,  str,&emode_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read EnergySetup Mode");
	}

	if( emode_odb >= 0 ){
		gEnergySampleMode = emode_odb;
	}


	cm_msg(MINFO,"sis3302_GetEnergyModeOdb"," -------------------- Energy Mode --------------------- ");
	cm_msg(MINFO,"sis3302_GetEnergyModeOdb"," Energy Mode         : [%d], ODB [%d] ",gEnergySampleMode,emode_odb);
	


}//sis3302_GetEnergyModeOdb


void sis3302_CalcBufferReq(int mod_num)
{

	//Number of Raw Words to Readout
	//Number of Energy Words to Readout
	//Total Event Size
	//
	int status;
	int emode_odb;
	char str[80];
	int size = sizeof(emode_odb);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/ADC/EnergySetup/Mode",mod_num);
	status =
		db_get_value(hDB, 0, str,&emode_odb, &size, TID_INT, TRUE);
	if (status != DB_SUCCESS) {
		printf("Cannot read EnergySetup Mode");
	}



	nof_energy_words = 0;
	//switch(gEnergySampleMode)
	switch(emode_odb)
	{
		case 0: nof_energy_words = 510;
			break;
		case 1: nof_energy_words = 256;
			break;
		case 2: nof_energy_words = 128;
			break;
		case 3: nof_energy_words = 32;
			break;
	};

	uint32_t raw_sample_length = gRawDataBufferConfig[0].SampleLength;

	nof_raw_data_words = (raw_sample_length>>1);
	
	energy_max_index = 2 + nof_energy_words + nof_raw_data_words;

	event_length_lwords = 6;
	event_length_lwords = event_length_lwords + nof_raw_data_words;
	event_length_lwords = event_length_lwords + nof_energy_words;

	// 8MByte = 0x800000 , Bytes = 0x400000 samples
//ax_nof_events = 0x20000 / (event_length_lwords);
//	max_nof_events = 0x100000 / (event_length_lwords);
	max_nof_events = 100 * (event_length_lwords);


	uint32_t proposed_endaddress_threshold = (max_nof_events*event_length_lwords);

	cm_msg(MINFO,"sis3302_CalcBufferReq"," ---------SIS [%d] ------ Memory Buffers --------------------- ",mod_num);
	cm_msg(MINFO,"sis3302_CalcBufferReq"," nof_raw_data_words 		: 0x%x [%d] ",nof_raw_data_words,nof_raw_data_words);
	cm_msg(MINFO,"sis3302_CalcBufferReq"," Raw Data Sample Length           : 0x%x [%d] ",raw_sample_length,raw_sample_length);
	cm_msg(MINFO,"sis3302_CalcBufferReq"," energy max index 		: 0x%x [%d] ",energy_max_index,energy_max_index);
	cm_msg(MINFO,"sis3302_CalcBufferReq"," nof_energy_words 		: 0x%x [%d] ",nof_energy_words,nof_energy_words);
	cm_msg(MINFO,"sis3302_CalcBufferReq"," max nof events	                : 0x%x [%d] ",max_nof_events,max_nof_events);
	cm_msg(MINFO,"sis3302_CalcBufferReq"," event_length_lwords 		: 0x%x [%d] ",event_length_lwords,event_length_lwords);
	cm_msg(MINFO,"sis3302_CalcBufferReq"," Energy Mode	 		: [%d] ",emode_odb);
	cm_msg(MINFO,"sis3302_CalcBufferReq"," Proposed EndAddress Value	: 0x%x [%d] ",proposed_endaddress_threshold,proposed_endaddress_threshold);

	sprintf(str,"/Analyzer/Parameters/sis3302_%d/InfoPage/nofRawDataWords",mod_num);
	db_set_value(hDB, 0, str,&nof_raw_data_words, sizeof(nof_raw_data_words), 1, TID_INT);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/InfoPage/RawDataSampleLength",mod_num);
	db_set_value(hDB, 0, str, &raw_sample_length, sizeof(raw_sample_length), 1, TID_INT);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/InfoPage/EnergyMaxIndex",mod_num);
	db_set_value(hDB, 0, str,&energy_max_index, sizeof(energy_max_index), 1, TID_INT);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/InfoPage/nofEnergyWords",mod_num);
	db_set_value(hDB, 0, str, &nof_energy_words, sizeof(nof_energy_words), 1, TID_INT);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/InfoPage/maxnofEvents",mod_num);
	db_set_value(hDB, 0,str, &max_nof_events, sizeof(max_nof_events), 1, TID_INT);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/InfoPage/event_length_lwords", mod_num);
	db_set_value(hDB, 0, str,&event_length_lwords, sizeof(event_length_lwords), 1, TID_INT);
	sprintf(str, "/Analyzer/Parameters/sis3302_%d/InfoPage/EnergyMode",mod_num);
	db_set_value(hDB, 0,str, &emode_odb, sizeof(emode_odb), 1, TID_INT);
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/InfoPage/PropEndAddress",mod_num);
	db_set_value(hDB, 0,str, &proposed_endaddress_threshold, sizeof(proposed_endaddress_threshold), 1, TID_INT);
}//sis3302_CalcBufferReq




int sis3302_Setup(uint32_t mode , DWORD module_addr , int mod_num )
{
	char str[80];
	sprintf(str,"/Analyzer/Parameters/sis3302_%d/general/BaseAddress",mod_num);
	db_set_value(hDB, 0, str,&module_addr, sizeof(module_addr), 1, TID_DWORD);
	cm_msg(MINFO,"sis3302_Setup"," ---------SIS [%d] Base address 0x%x-----mode[%d]-------------- ",mod_num,module_addr,mode);
	cm_msg(MINFO,"sis3302_Setup"," ------------------------------ ");

	switch(mode){
		case 0x1:{


				 //Control Register
				 //
				  sis3320_RegisterWrite(k600vme, module_addr, 0x0,0x00000000 );
				 
				 //
				 sis3302_LED(0,module_addr,mod_num);
			 	 sis3302_AcquisitionControl(module_addr, mod_num);
				 sis3302_EndThreshold_Register(module_addr,mod_num);
				 sis3302_RawDataBuffer_Configuration(module_addr,mod_num);
				 sis3302_EnergySample_Registers(module_addr,mod_num);
				 sis3302_PreTriggerGateDelay(module_addr,mod_num);
				 sis3302_TriggerSetup(module_addr,mod_num);
				 sis3302_EventConfiguration(module_addr,mod_num);
				 sis3302_TriggerThreshold(module_addr,mod_num);
				 sis3302_EnergySetupGP_Register(module_addr,mod_num);
				 sis3302_EnergyGateLength_Register(module_addr,mod_num);
				 sis3302_EnergyTauFactor_Register(module_addr,mod_num);
				 sis3302_GetEnergyModeOdb(mod_num);


				 sis3302_CalcBufferReq(mod_num);
			
			 	 if(gWriteDAC == 1	){
				 	sis3302_init_dac(module_addr,mod_num);
				 } else if( gWriteDAC == 2){
				 	sis3302_init_dac(module_addr,mod_num);
				 }

				 break;
			 }
		case 0x2:break;
		case 0x3:break;
		default :
			cm_msg(MERROR,"sis3302_Setup","Unknown Setup mode");
			return -1;

	}//switch


	return 0;
}/* sis3302_Setup*/


/*
 *  Register Status display Functions
 *
 *
 *
 *
 *
 *
 *
 *
 */


void sis3302_FrontPanelLemoIn_1(DWORD module_addr, int mod_num)
{

	DWORD aqui_control;
	aqui_control = sis3320_RegisterRead(k600vme, module_addr, SIS3302_ACQUISTION_CONTROL);
	cm_msg(MINFO,"sis3302_FrontPanelLemoIn_1"," SIS Module: [%d] Front Panel Lemo In 1 Status <<%d>> ",mod_num, (aqui_control>>10)&0x1);
}

void sis3302_FrontPanelLemoIn_2(DWORD module_addr, int mod_num)
{
	DWORD aqui_control;
	aqui_control = sis3320_RegisterRead(k600vme, module_addr, SIS3302_ACQUISTION_CONTROL);
	cm_msg(MINFO,"sis3302_FrontPanelLemoIn_2"," SIS Module: [%d] Front Panel Lemo In 2 Status <<%d>> ",mod_num,(aqui_control>>9)&0x1);

}

void sis3302_FrontPanelLemoIn_3(DWORD module_addr, int mod_num)
{
	DWORD aqui_control;
	aqui_control = sis3320_RegisterRead(k600vme,module_addr, SIS3302_ACQUISTION_CONTROL);
	cm_msg(MINFO,"sis3302_FrontPanelLemoIn_3"," SIS Module: [%d] Front Panel Lemo In 3 Status <<%d>> ",mod_num,(aqui_control>>8)&0x1);

}

void sis3302_ClockStatus(DWORD module_addr, int mod_num)
{
	int myclock;
	int aqui_control;
	aqui_control = sis3320_RegisterRead(k600vme, module_addr, SIS3302_ACQUISTION_CONTROL);

	myclock = (aqui_control>>12)&0x7;
	
	char str[256];
	char str_set[80];
	cm_msg(MINFO,"sis3302_ClockStatus"," --------------- CLOCK SETTING ------SIS Module [%d] --- ",mod_num);
	switch(myclock){
		case 0:
			cm_msg(MINFO,"sis3302_ClockStatus","Clock Internal 100 MHz");
		   	sprintf(str,"Clock Internal 100 MHz");
			sprintf(str_set, "/Analyzer/Parameters/sis3302_%d/general/ClockType",mod_num);
			db_set_value(hDB, 0,str_set, &str, sizeof(str), 1, TID_STRING);
			break;
		case 1:
			cm_msg(MINFO,"sis3302_ClockStatus","Clock Internal 50 MHz");
			sprintf(str,"Clock Internal 50 MHz");
			sprintf(str_set, "/Analyzer/Parameters/sis3302_%d/general/ClockType",mod_num);
			db_set_value(hDB, 0,  str_set, &str, sizeof(str), 1, TID_STRING);
			break;
		case 2:
			cm_msg(MINFO,"sis3302_ClockStatus","Clock Internal 25 MHz");
			sprintf(str,"Clock Internal 25 MHz");
			sprintf(str_set, "/Analyzer/Parameters/sis3302_%d/general/ClockType",mod_num);
			db_set_value(hDB, 0,  str_set, &str, sizeof(str), 1, TID_STRING);
			break;
		case 3:
			cm_msg(MINFO,"sis3302_ClockStatus","Clock Internal 10 MHz");
			sprintf(str,"Clock Internal 10 MHz");
			sprintf(str_set, "/Analyzer/Parameters/sis3302_%d/general/ClockType",mod_num);
			db_set_value(hDB, 0,  str_set, &str, sizeof(str), 1, TID_STRING);
			break;
		case 4:
			cm_msg(MINFO,"sis3302_ClockStatus","Clock Internal 1 MHz");
			sprintf(str,"Clock Internal 1 MHz");
			sprintf(str_set, "/Analyzer/Parameters/sis3302_%d/general/ClockType",mod_num);
			db_set_value(hDB, 0,  str_set, &str, sizeof(str), 1, TID_STRING);
			break;
		case 5:
			cm_msg(MINFO,"sis3302_ClockStatus","Clock Internal 100 MHz");
			sprintf(str,"Clock Internal 100 MHz");
			sprintf(str_set, "/Analyzer/Parameters/sis3302_%d/general/ClockType",mod_num);
			db_set_value(hDB, 0,  str_set, &str, sizeof(str), 1, TID_STRING);
			break;
		case 6:
			cm_msg(MINFO,"sis3302_ClockStatus","Clock External clock, LEMO front panel , min. 1 MHz");
			sprintf(str,"Clock External, LEMO front panel, min. 1 MHz");
			sprintf(str_set, "/Analyzer/Parameters/sis3302_%d/general/ClockType",mod_num);
			db_set_value(hDB, 0,  str_set, &str, sizeof(str), 1, TID_STRING);
			break;
		case 7:
			cm_msg(MINFO,"sis3302_ClockStatus","Clock Second Internal 100 MHz");
			sprintf(str,"Clock Second Internal 100 MHz");
			sprintf(str_set, "/Analyzer/Parameters/sis3302_%d/general/ClockType",mod_num);
			db_set_value(hDB, 0,  str_set, &str, sizeof(str), 1, TID_STRING);
			break;
		default:
			sprintf(str,"Unknown clock config");
			sprintf(str_set, "/Analyzer/Parameters/sis3302_%d/general/ClockType",mod_num);
			db_set_value(hDB, 0, str_set, &str, sizeof(str), 1, TID_STRING);
			cm_msg(MINFO,"sis3302_ClockStatus","Unknown clock config");
	}


}//sis3302_ClockStatus


void sis3302_LemoMode(DWORD module_addr, int mod_num )
{
	int mcamode;
	DWORD aqui_control = 0x0;
	char str[256];
	char str_set[80];
	aqui_control = sis3320_RegisterRead(k600vme, module_addr, SIS3302_ACQUISTION_CONTROL);
	mcamode = (aqui_control>>3)&0x1;
	if(mcamode==0) {
		int lemo_out = (aqui_control>>4)&0x3;
		int lemo_in = (aqui_control)&0x7;
		switch(lemo_out){
			case 0:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 3 -> ADC sample logic armed ");
				sprintf(str,"ADC sample logic armed ");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoOut/LemoOut3",mod_num);
				db_set_value(hDB, 0,str_set,&str, sizeof(str), 1, TID_STRING);
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 2 -> ADCx event sampling busy");
				sprintf(str,"ADCx event sampling busy");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoOut/LemoOut2",mod_num);
				db_set_value(hDB, 0,str_set, &str, sizeof(str), 1, TID_STRING);
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 1 -> Trigger Lemo output");
				sprintf(str,"Trigger Lemo output");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoOut/LemoOut1",mod_num);
				db_set_value(hDB, 0,str_set,&str, sizeof(str), 1, TID_STRING);
				break;
			case 1:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 3 -> ADC sample logic armed ");
				sprintf(str,"ADC sample logic armed ");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoOut/LemoOut3",mod_num);
				db_set_value(hDB, 0, str_set,&str, sizeof(str), 1, TID_STRING);
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 2 -> ADCx event sampling busy OR ADC sample logic not armed (veto) ");
				sprintf(str,"ADCx event sampling busy OR ADC sample logic not armed (veto) ");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoOut/LemoOut2",mod_num);
				db_set_value(hDB, 0,str_set, &str, sizeof(str), 1, TID_STRING);
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 1 -> Trigger Lemo output");
				sprintf(str,"Trigger Lemo output");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoOut/LemoOut1",mod_num);
				db_set_value(hDB, 0,str_set,&str, sizeof(str), 1, TID_STRING);
				break;
			case 2:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 3 -> ADC N+1 Neighbour Trigger/Gate Out");
				sprintf(str,"ADC N+1 Neighbour Trigger/Gate Out");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoOut/LemoOut3",mod_num);
				db_set_value(hDB, 0, str_set,&str, sizeof(str), 1, TID_STRING);
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 2 -> Trigger Lemo output");
				sprintf(str,"Trigger Output");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoOut/LemoOut2",mod_num);
				db_set_value(hDB, 0,str_set, &str, sizeof(str), 1, TID_STRING);
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 1 -> ADC N-1 Neighbour Trigger/Gate Out");
				sprintf(str,"ADC N-1 Neighbour Trigger/Gate Out");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoOut/LemoOut1",mod_num);
				db_set_value(hDB, 0,str_set,&str, sizeof(str), 1, TID_STRING);
				break;
			case 3:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 3 -> ADC N+1 Neighbour Trigger/Gate Out");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 2 -> ADC Sampling busy OR ADC sample logic not armed (veto) ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 1 -> ADC N-1 Neighbour Trigger/Gate Out ");
				break;
			default:
				cm_msg(MINFO,"sis3302_LemoOutMode","Unknown mcamode 0 lemo Lemo output config");
		}//switch
		switch(lemo_in){
			case 0:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 3 -> Trigger ");
				sprintf(str,"Trigger");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoIn/LemoIn2",mod_num);
				db_set_value(hDB, 0,str_set, &str, sizeof(str), 1, TID_STRING);
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 2 -> Timestamp Clear ");
				sprintf(str,"Timestamp Clear");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoIn/LemoIn2",mod_num);
				db_set_value(hDB, 0,str_set, &str, sizeof(str), 1, TID_STRING);
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 1 -> Veto ");
				sprintf(str,"Veto");
				sprintf(str_set, "/Analyzer/Parameters/sis3302_%d/general/LemoIn/LemoIn1",mod_num);
				db_set_value(hDB, 0,str_set,&str, sizeof(str), 1, TID_STRING);
				break;
			case 1:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 3 -> Trigger ");
				sprintf(str,"Trigger");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoIn/LemoIn3",mod_num);
				db_set_value(hDB, 0,str_set,&str, sizeof(str), 1, TID_STRING);
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 2 -> Timestamp Clear ");
				sprintf(str,"Timestamp Clear");
				sprintf(str_set,"/Analyzer/Parameters/sis3302_%d/general/LemoIn/LemoIn2",mod_num);
				db_set_value(hDB, 0,str_set , &str, sizeof(str), 1, TID_STRING);
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 1 -> Gate ");
				sprintf(str,"Gate");
				sprintf(str_set, "/Analyzer/Parameters/sis3302_%d/general/LemoIn/LemoIn1",mod_num);
				db_set_value(hDB, 0,str_set,&str, sizeof(str), 1, TID_STRING);
				break;
			case 2:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 3 -> reserved ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 2 -> reserved ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 1 -> reserved ");
				break;
			case 3:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 3 -> reserved ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 2 -> reserved ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 1 -> reserved ");
				break;
		        case 4:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 3 -> ADC N+1 Neighbour Trigger/Gate In ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 2 -> Trigger ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 1 -> ADC N-1 Neighbour Trigger/Gate In  ");
				break;
			case 5:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 3 -> ADC N+1 Neighbour Trigger/Gate In ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 2 -> Timestamp Clear  ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 1 -> ADC N-1 Neighbour Trigger/Gate In ");
				break;
			case 6:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 3 -> ADC N+1 Neighbour Trigger/Gate In");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 2 -> Veto ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 1 -> ADC N-1 Neighbour Trigger/Gate In ");
				break;
			case 7:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 3 -> ADC N+1 Neighbour Trigger/Gate In ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 2 -> Gate ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 1 -> ADC N-1 Neighbour Trigger/Gate In ");
				break;
			default:
				cm_msg(MINFO,"sis3302_LemoOutMode","Unknown mcamode 0 lemo Lemo input config");
		}//switch

	}else if(mcamode == 1){
		int lemo_out = (aqui_control>>4)&0x3;
		int lemo_in = (aqui_control)&0x7;

		switch(lemo_out){
			case 0:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 3 -> ADC sample logic armed ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 2 -> ADCx event sampling busy");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 1 -> Trigger Lemo output");
				break;
			case 1:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 3 -> Multiscan first scan signal ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 2 -> LNE ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 1 -> Scan Enable ");
				break;
			case 2:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 3 -> Scan Enable ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 2 -> LNE ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 1 -> Trigger Lemo output ");
				break;
			case 3:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 3 -> reserved ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 2 -> reserved ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo output 1 -> reserved ");
				break;
			default:
				cm_msg(MINFO,"sis3302_LemoOutMode","Unknown mcamode 1 lemo Lemo output config");
		}//switch
		switch(lemo_in){
			case 0:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 3 -> reserved  ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 2 -> External MCA_Start (histogram ptr reset/start pulse) ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 1 -> External next pulse (LNE) ");
				break;
			case 1:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 3 -> Trigger ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 2 -> external MCA_Start (histogram ptr reset/start pulse) ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 1 -> external next pulse (LNE) ");
				break;
			case 2:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 3 -> Veto ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 2 -> external MCA_Start (histogram ptr reset/start pulse) ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 1 -> external next pulse (LNE) ");
				break;
			case 3:
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 3 -> Gate ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 2 -> external MCA_Start (histogram ptr reset/start pulse) ");
				cm_msg(MINFO,"sis3302_LemoOutMode","Lemo input 1 -> external next pulse (LNE) ");
				break;
			case 4: 
				cm_msg(MINFO,"sis3302_LemoOutMode","Reserved all ");
				break;
			case 5: 
				cm_msg(MINFO,"sis3302_LemoOutMode","Reserved all ");
				break;
			case 6: 
				cm_msg(MINFO,"sis3302_LemoOutMode","Reserved all ");
				break;
			case 7: 
				cm_msg(MINFO,"sis3302_LemoOutMode","Reserved all ");
				break;
			default:
				cm_msg(MINFO,"sis3302_LemoOutMode","Unknown mcamode 1 lemo Lemo input config");
		}//switch



	}//if-else

	

}//LemoOutMode

void sis3302_SetMCAMode(int mcamode )
{
	int mca;

	if( mcamode == 0){
		 // cm_msg(MINFO,"sis3302_SetMCAMode","MCA Mode Disabled ");
		 // mca = sis3320_RegisterRead(k600vme, SIS3302_BASE, 0x10);
  		 // cm_msg(MINFO,"sis3302_SetMCAMode"," Acquisition Control Register: 0x%x, MCA Mode: %d ",mca,(mca>>3)&0x1);
		//  sis3320_RegisterWrite(k600vme,SIS3302_BASE,0x10,mca|0x80000);
		  mca = sis3320_RegisterRead(k600vme, SIS3302_BASE, 0x10);
  		  cm_msg(MINFO,"sis3302_SetMCAMode","Disabled:  Acquisition Control Register: 0x%x, MCA Mode: %d ",mca,(mca>>3)&0x1);
	}else if(mcamode ==1){
		  cm_msg(MINFO,"sis3302_SetMCAMode","MCA Mode Enabled ");
		  mca = sis3320_RegisterRead(k600vme, SIS3302_BASE, 0x10);
  		  cm_msg(MINFO,"sis3302_SetMCAMode"," Acquisition Control Register: 0x%x, MCA Mode: %d ",mca,(mca>>3)&0x1);
		  sis3320_RegisterWrite(k600vme,SIS3302_BASE,0x10,mca|0x8);
		  mca = sis3320_RegisterRead(k600vme, SIS3302_BASE, 0x10);
  		  cm_msg(MINFO,"sis3302_SetMCAMode","Enabled:  Acquisition Control Register: 0x%x, MCA Mode: %d ",mca,(mca>>3)&0x1);
	}
}



