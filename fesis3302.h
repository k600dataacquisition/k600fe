/*
 * =====================================================================================
 *
 *       Filename:  fesis3302.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  10/29/2015 10:11:11 PM
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */

#ifndef FESIS3302_H
#define FESIS3302_H

#include "midas.h"

//k600fevme.c
extern MVME_INTERFACE *k600vme;
extern DWORD SIS3302_BASE[2];
extern INT gRunStopStatusFlag;
extern INT SISCount;
extern uint32_t bank1_armed_flag[2];

//sis3302felib.c
extern DWORD event_length_lwords;


#endif


