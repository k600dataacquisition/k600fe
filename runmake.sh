#!/bin/bash
#===============================================================================
#
#          FILE:  runmake.sh
# 
#         USAGE:  ./runmake.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  28/01/2010 13:13:24 SAST
#      REVISION:  ---
#===============================================================================
rm -f *.log *.bin .*.swp *.dump *.debug
./make_buildnum.sh
make clean
make
#rm -f /home/online/k600fevme
#cp k600fevme /home/online
#chown online:online /home/online/k600fevme
