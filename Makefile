#####################################################################
#
#  Name:         Makefile
#  Created by:   Stefan Ritt
#
#  Contents:     Makefile for MIDAS example frontend and analyzer
#
#  $Id$
#
#####################################################################
#
#--------------------------------------------------------------------
# The MIDASSYS should be defined prior the use of this Makefile
ifndef MIDASSYS
missmidas::
	@echo "...";
	@echo "Missing definition of environment variable 'MIDASSYS' !";
	@echo "...";
endif


OS_DIR = linux
OSFLAGS = -DOS_LINUX -Dextname 

#Timing
# CFLAGS = -g -O2 -Wall -fstrict-aliasing -DTIMING

#Normal with EQ_MULTITHREAD ENABLED
# CFLAGS = -g -O2 -Wall -fstrict-aliasing -DDEBUG_CON -DNO_MMAP  -DCHECK_THREAD_ID -DTEST_THREADS

#Normal without EQ_MULTITHREAD ENABLED
#CFLAGS = -pg -Wall  -DDEBUG_CON -DNO_MMAP  #-DDEBUGC 
#CFLAGS =  -g -Wall -fstrict-aliasing  -DNO_MMAP -DTIMING_TEMP # -DDEBUGC 
#CFLAGS =  -m32 -pg -g -Wall -fstrict-aliasing  -DNO_MMAP #-DTIMING_TEMP # -DDEBUGC 

#CFLAGS =  -m32 -g -Wall -fstrict-aliasing  -DNO_MMAP -D_LARGEFILE64_SOURCE #-DTIMING_TEMP # -DDEBUGC 

#CFLAGS =  -g -m32 -DOS_LINUX -Dextname -Wall -fstrict-aliasing  -DDEBUG_CON -DNO_MMAP -D_LARGEFILE64_SOURCE #-DTIMING_TEMP # -DDEBUGC 


#CFLAGS = -m32 -g -Wall -Wextra -fstrict-aliasing -D_LARGEFILE64_SOURCE #-DTIMING_TEMP # -DDEBUGC 

# CFLAGS = -g -O2 -Wall -fstrict-aliasing -DDEBUG_CON -DNO_MMAP  

#Normal 2
# CFLAGS = -g -Wall -DDEBUG_CON -DNO_MMAP 

# add to compile in 32-bit mode
# OSFLAGS += -m32
#LIBS = -lmidas -lm -lz -lutil -lnsl -lpthread -lcctvmeen -lrt
#LIBS = -lmidas -lm -lz -lutil -lnsl -lpthread -lrt

#-----------------------------------------
# ROOT flags and libs
#
ifdef ROOTSYS
ROOTCFLAGS := $(shell  $(ROOTSYS)/bin/root-config --cflags)
ROOTCFLAGS += -DHAVE_ROOT -DUSE_ROOT
ROOTLIBS   := $(shell  $(ROOTSYS)/bin/root-config --libs) -Wl,-rpath,$(ROOTSYS)/lib
ROOTLIBS   += -lThread
endif
#-------------------------------------------------------------------
# The following lines define directories. Adjust if necessary
#                 
#DRV_DIR   = $(MIDASSYS)/drivers/vme/vmic
DRV_DIR   = $(MIDASSYS)/drivers/vme/vcon
VMEDRV_DIR   = $(MIDASSYS)/drivers/vme
# this is for vme modules driver directory.
MODULES_DIR= $(MIDASSYS)/drivers/vme
INC_DIR   = $(MIDASSYS)/include
LIB_DIR   = $(MIDASSYS)/$(OS_DIR)/lib
SRC_DIR   = $(MIDASSYS)/src
GEF_INC_DIR=/usr/include
GEF_LIB_DIR=/usr/lib/gef
CONC_INC_DIR = /usr/local/linuxvmeen
CONC_LIB_DIR = /usr/local/linuxvmeen/cct_modules

LIB = $(MIDASSYS)/$(OS_DIR)/lib/libmidas.a -L.

LIBPATHSHARED = /usr/local/linuxvmeen/cct_modules/shared
LIBPATH = /usr/local/linuxvmeen/cct_modules/
LIBPATH += $(LIB) 

#ifeq ($(COMPATLIB),FALSE)
LIBEN	= -lcctvmeen
LIBEN 	+= -lm -lz -lutil -lnsl -lpthread -lrt
#else
#LIBEN	= -lcctvmeencompat
#LIBEN 	+= -lm -lz -lutil -lnsl -lpthread -lrt
#endif

INCLUDE = /usr/local/linuxvmeen/
RPATH:=$(shell cd $(LIBPATHSHARED); pwd)
#ifeq ($(OS64BIT),FALSE)
#CFLAGS  = -Wall -m32 -I $(INCLUDE)
CFLAGS =   -g -m32 -DOS_LINUX -Dextname -Wall -fstrict-aliasing  -DNO_MMAP -D_LARGEFILE64_SOURCE  #-DTIMING_TEMP # -DDEBUGC 
CFLAGS +=  -I $(INCLUDE) -I$(INC_DIR) -I$(DRV_DIR) -I$(MODULES_DIR) -I$(CONC_INC_DIR) -I. 
#else
#CFLAGS  =  -g -Wall -m64 -DOS64BIT 
#CFLAGS += -I $(INCLUDE) -I$(INC_DIR) -I$(DRV_DIR) -I$(MODULES_DIR) -I$(CONC_INC_DIR) -I. 
#endif






#-------------------------------------------------------------------
# List of analyzer modules
#
MODULES   = adccalib.o adcsum.o scaler.o  


#MODULES   = adccalib.o adcsum.o scaler.o
#-------------------------------------------------------------------
# Hardware driver can be (camacnul, kcs2926, kcs2927, hyt1331)
#
DRIVER = concvme
VMEMODULES = v1190A v792 v792n v830 sis3320
VMEMODULES_OBJ = $(addsuffix .o, $(VMEMODULES))
MODULESFE = sis3302felib sis3302fevme
MODULESFE_OBJ = $(addsuffix .o, $(MODULESFE))
#-------------------------------------------------------------------
# Frontend code name defaulted to frontend in this example.
# comment out the line and run your own frontend as follow:
# gmake UFE=my_frontend
#
#UFE = k600frontend
#UFE = myfrontend
UFE = k600fevme

####################################################################
# Lines below here should not be edited
####################################################################

# MIDAS library
#LIB = -L$(LIB_DIR)/libmidas.a
#LIB = -L$(MIDASSYS)/$(OS_DIR)/lib  -L$(GEF_LIB_DIR) -L.
#LIB = -L$(MIDASSYS)/$(OS_DIR)/lib  -L$(CONC_LIB_DIR) -L.
# compiler
CC = cc
CXX = g++
# CFLAGS += -g -I$(INC_DIR) -I$(DRV_DIR) -I$(MODULES_DIR) -I$(VMEDRV_DIR)
#CFLAGS += -I$(INC_DIR) -I$(DRV_DIR) -I$(MODULES_DIR) -I$(CONC_INC_DIR) -I. 
#LDFLAGS += -lgefcmn-temp -lgefcmn-timers

all: $(UFE)

$(UFE): $(LIB_DIR)/mfe.o $(DRIVER).o $(VMEMODULES_OBJ) $(MODULESFE_OBJ) $(UFE).o 
	$(CC) -Wl,-rpath=$(RPATH) $(CFLAGS) $(LDFLAGS) $^ $(LIB) $(LIBS) -o $@ -L $(RPATH) $(LIBEN) $(LIB)

#$(DRIVER).o  $(LIB_DIR)/mfe.o $(VMEMODULES_OBJ) $(LIB) \

$(UFE).o: $(UFE).c 
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $(UFE).o $(UFE).c

#$(MODULESFE).o: $(MODULESFE).c 
#	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $(MODULESFE).o $(MODULESFE).c


#	$(CC) $(CFLAGS) $(OSFLAGS) -o $(UFE) $(UFE).c \
#	$(DRIVER).o  $(LIB_DIR)/mfe.o $(VMEMODULES_OBJ) $(LIB) \
#	$(LDFEFLAGS) $(LIBS)
#	$(DRIVER).o  /opt/midas/drivers/vme/v1190A.o /opt/midas/drivers/vme/v792n.o /opt/midas/drivers/vme/v259.o /opt/midas/drivers/vme/v830.o $(LIB_DIR)/mfe.o $(LIB) \

$(DRIVER).o: $(DRV_DIR)/$(DRIVER).c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $(DRIVER).o $(DRV_DIR)/$(DRIVER).c

%.o: $(VMEDRV_DIR)/%.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ $<

$(MODULE_DIR)/%.o: $(MODULES_DIR)/%.c
	$(CXX) $(USERFLAGS) $(ROOTCFLAGS) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

analyzer: $(LIB) $(LIB_DIR)/rmana.o analyzer.o $(MODULES)
	$(CXX) $(CFLAGS) -o $@ $(LIB_DIR)/rmana.o analyzer.o $(MODULES) \
	$(LIB) $(LDFLAGS) $(ROOTLIBS) $(LIBS)

%.o: %.c /opt/vmecrates/concurrentvme/experim.h
	$(CXX) $(USERFLAGS) $(ROOTCFLAGS) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

clean::
	rm -f *.o *~ \#*

#end file
