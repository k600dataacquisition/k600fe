/*
 * =====================================================================================
 *
 *       Filename:  sis3302fevme.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  28/10/2015 09:45:39
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */

#ifndef SIS3302FEVME_H
#define SIS3302FEVME_H

#include "midas.h"
 

INT sis3302_read_A32BLT_ADC_MEMORY( DWORD module_addr,
				   DWORD adc_channel,
				   DWORD memory_start_addr,
				   DWORD *data_buffer,
				   DWORD req_lwords,
				   DWORD *got_lwords);

INT WaitDataReady( INT, INT);
//INT DisarmSample(INT );
INT DisarmSample(INT ,DWORD );
INT ArmSample(INT,DWORD);
INT DualBankArmDisarm(INT,DWORD);
INT DualBankCheckIfSwapped(INT,DWORD);
INT isSIS3302ErrorWord( DWORD*  );
INT isSIS3302Header( DWORD* ,int);
INT isSIS3302Trailer( DWORD* );



#endif//SIS3302FEVME_H
