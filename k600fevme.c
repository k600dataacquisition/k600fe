 /* =====================================================================================
 *
 *       Filename:  k600fevme.c
 *
 *    Description:  multithread frontend for k600 DAQ
 *
 *        Version:  1.0
 *        Created:  09/30/2010 01:49:03 PM
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 *
 * DEBUG SCALERS: 
 * Description: There are 500 Debug/Diagnostic scalers dotted through out the current
 * 		frontend, k600fevme.c. The global variable, int diag_sclr[200], is used
 * 		as the holder of the diagnostic/debug information.  
 *
 *
 * Below is a description of each scaler and what it is used for. 
 *
 *  Array Number  :  Description 
 *  0			TDC Event
 *  1			QDC Event
 *  2			Previous TDC Event
 *  3			TDCMod that is being skipped
 *  4			Number of Skips
 *  5			Number of Words QDC/TDC Searched
 *  6			Number of Words "    "   " 
 *
 *  TDC Buffer Leves The below values can alter from experiment. 
 *  7			TDC 1 Buffer Level
 *  8			TDC 2 Buffer Level
 *  9			TDC 3 Buffer Level
 *  10			TDC 4 Buffer Level
 *  11			TDC 5 Buffer Level
 *  12			TDC 6 Buffer Level
 *  13			TDC 7 Buffer Level
 *  14			TDC 8 Buffer Level 
 *
 *  QDC Buffer Level
 *  17			QDC Buffer Level
 *
 *  20			QDC Poll Read Average
 *  21			QDC Poll Read Average
 *  54			Positive Poll poll_event
 *  55			Negative Poll poll_event
 *
 *
 *  ADC Buffer Levels, The below can vary with experimental setup
 *  70			ADC 1
 *  71			ADC 2
 *  72			ADC 3
 *  73			ADC 4
 *  74			ADC 5
 *
 * TDC Word Counter, that sits in the read_trigger_event function
 *  80			TDC 1
 *  81			TDC 2
 *  82			TDC 3
 *  83			TDC 4
 *  84			TDC 5
 *  85			TDC 6
 *  86			TDC 7
 *  87			TDC 8
 *
 *  QDC DB_TIMEOUT checker
 *  90			QDC
 *
 *  ADC 1-5 adc word counter (adc_cntnum) checker
 *  91			ADC 1
 *  92			ADC 2
 *  93			ADC 3
 *  94			ADC 4
 *  95			ADC 5
 *
 *  read_trigger_event calls
 *  100			read_trigger_event function calls
 *
 *  101			QDC Positive Polls ( read out thread )
 *  102			QDC Negative Polls ( read out thread )
 *  
 *  103 		TDC 1 Event count ( headers) in function sortTDCData. ( readout thread )
 *  104 		TDC 2 Event count ( headers) in function sortTDCData. ( readout thread )
 *  105 		TDC 3 Event count ( headers) in function sortTDCData. ( readout thread )
 *  106 		TDC 4 Event count ( headers) in function sortTDCData. ( readout thread )
 *  107 		TDC 5 Event count ( headers) in function sortTDCData. ( readout thread )
 *  108 		TDC 6 Event count ( headers) in function sortTDCData. ( readout thread )
 *  109 		TDC 7 Event count ( headers) in function sortTDCData. ( readout thread )
 *  110 		TDC 8 Event count ( headers) in function sortTDCData. ( readout thread )
 *
 *  115			QDC Event count ( headers ) in function sortdataB. ( readout thread )
 *
 *  120			Positive Scaler Poll Count
 *  121			Negative Scaler Poll Count
 *
 *  125			gefanuc board temp sensor 1
 *  126			gefanuc board temp sensor 2
 *  127			gefanuc board temp sensor 2
 * 
 *  128			gefanuc board readout timer 1 
 *  129			gefanuc board readout timer 2 
 *  130			gefanuc board readout timer 3
 *
 *  142			TDC 1 time of module readout
 *  143			TDC 1 time of module readout
 *  144			TDC 1 time of module readout
 *  145			TDC 1 time of module readout
 *  146			TDC 1 time of module readout
 *  147			TDC 1 time of module readout
 *  148			TDC 1 time of module readout
 *  149			TDC 1 time of module readout
 *
 *  155			Scaler 1 module readout time
 *  156			Scaler 1 module readout time
 *  
 *  160 - 167 		SIS3302 ADC Channels Buffer Levels.
 *   
 *  170 = 178 		TDC Skip Stats
 *   
 *  194			adc data_size
 */



/* QDC Module */
#define MODULE_V792
#undef QDC_16
#define  QDC_32

 /* Define with ADC module */
#define WITH_ADC_MODULE

#define  WITH_TDC_MODULE

#define WITH_V830_MODULE


#undef WITH_ADC_N_MODULE
#undef WITH_SIS_MODULE

#define NUM_OF_SIS_MODS 2
#define NUM_ADC_SIS_CHANNELS 4

#undef DEBUG_SIS_1
#undef DEBUG_SIS_2
#undef DEBUG_SIS_3
#undef DEBUG_SIS_4


/*  Define with TDC Module */
/*  Causes a 1 second delay if internal
 *  tdc chip error is detected 
 *  */
#undef TDC_CHIP_DEBUG

#define FE_WITH_SCALAR
#define FE_WITH_DEBUG_SCALARS
#define  FE_WITH_TRIGGER
#undef USE_INT

#define DUALBANKREADOUT

/* Number of TDC(v1190) Modules in the crate */
#define NUM_OF_TDC_MODS 6
/* Number of ADC Modules in the crate */
#define NUM_OF_ADC_MODS 5
#define ADCN_START 5

/* Number of Scalar Modules in the crate */
#define NUM_OF_SCR_MODS 1
/* Number of debug scalars. */
#define NUMBER_SCALARS 600
/* Scalar module 1 */
#define MAXCHANNELS1 32
/* Scalar module 2 */
#define MAXCHANNELS2 32


#define QDC_THRESHOLD_CHANNELS 32
#define QDC16_THRESHOLD_CHANNELS 16

#define ADC_THRESHOLD_CHANNELS 32
#define ADC_THRESHOLD_CHANNELS_N 16

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>
#include <byteswap.h>
#include <time.h>
#include <math.h>

/*  midas includes */
#include "midas.h"
#include "mcstd.h"
#include "experim.h"
#include "mvmestd.h"
#include "msystem.h"
#include "build_number.h"
#include "v792n.h"	
#include "v792.h"	
#include "v1190A.h"
#include "v830.h"
#include "sis3320drv.h"
#include "sis3302gamma.h"
#include "sis3302felib.h"
#include "sis3302fevme.h"

//external globals
#include "fesis3302.h"

/* gefanuc temp sensors */
//#include "gef/gefcmn_temp.h"

#define TEMP_INVALID 0x10000000

/*  gefanuc timers */
#include <fcntl.h>
#include <semaphore.h>
//#include <gef/gefcmn_timers.h>


/*  Global Debugs */
int diag_sclr[NUMBER_SCALARS];
int FRONTEND_DEBUG_MODE = 0;
int FRONTEND_DEBUG_MODE_TIMER = 0;
int FRONTEND_DEBUG_MODE_A= 0;  		// No Assingment Yet
int FRONTEND_DEBUG_MODE_B = 0; 		// Ring Buffer QDC Checks
int FRONTEND_DEBUG_MODE_C = 0; 		// Ring Buffer ADC Checks
int FRONTEND_DEBUG_MODE_D = 0;	  	// Ring Buffer TDC Checks
int FRONTEND_DEBUG_MODE_E = 0;	  	// Software ADC Event Count Mode
int FRONTEND_DEBUG_MODE_F = 0;	  	// TDC Internal chip debug Mode 
int FRONTEND_MODE = 0; 				// Default is 0
volatile int FRONTEND_NO_SIS = 0;
//FILE *fp_thread;
INT my_global_test_counter=0;


//WORD QDC_Thresholds[QDC_THRESHOLD_CHANNELS];
WORD QDC_Thresholds[QDC16_THRESHOLD_CHANNELS];



WORD ADC_Thresholds[ADC_THRESHOLD_CHANNELS];
WORD ADC_Thresholds_N[ADC_THRESHOLD_CHANNELS_N];


//GEF_TIMER_HDL   timer_handle;
//GEF_CALLBACK_HDL callback_handle;
//static GEF_TIMER_MODE  opt_mode1    = GEF_TIMER_MODE_ONE_SHOT;
//static GEF_TIMER_MODE  opt_mode2    = GEF_TIMER_MODE_PERIODIC;

int QDC_HighWater = 29;		
int TDC_HighWater = 7;		

int *SCLR_CHANNELS[NUM_OF_SCR_MODS];

/*  VME Interface */
MVME_INTERFACE *k600vme;
uint32_t bank1_armed_flag[NUM_OF_SIS_MODS];
int gRunStopStatusFlag = TRUE;

int gTriggerEnabled = FALSE;
int gTriggerReadoutActive = FALSE;


/* thread global variables */
midas_thread_t READOUT_THREAD_ID;
midas_thread_t SYNC_THREAD_ID;

volatile int my_stop_all_threads = 0;
volatile int my_readout_thread_active = 0;
volatile int scaler_readout_thread_active = 0;
static int my_readout_enabled_flag = 0;
//static int scaler_readout_enabled_flag = 0;

/* VME Base-Addresses */
DWORD V792_BASE;
DWORD V1190_BASE[NUM_OF_TDC_MODS];
DWORD V785_BASE[NUM_OF_ADC_MODS];
DWORD V830_BASE[NUM_OF_SCR_MODS];
DWORD SIS3302_BASE[NUM_OF_SIS_MODS];

DWORD SIS3302_BASE_ADDR=0x800000;

/*  FPGA */
DWORD TRIGGER_REG = 0x0048;
DWORD V1495_FPGA2 = 0x30000;

int V1190Count=NUM_OF_TDC_MODS;
int V785Count=NUM_OF_ADC_MODS;
int SISCount=NUM_ADC_SIS_CHANNELS;


/*  RingBuffers */
int ringbh1=0, ringbh2 = 0, ringbh1_next = 0, ringbh2_next = 0;
int ringbh_1190[NUM_OF_TDC_MODS];
int ringbh_v830[NUM_OF_SCR_MODS];
int ringbh_v785[NUM_OF_ADC_MODS];
//int ringbh_sis3302[NUM_ADC_SIS_CHANNELS];
int ringbh_sis3302[NUM_OF_SIS_MODS][NUM_ADC_SIS_CHANNELS];

/*  GEF Timer  */
int gef_time_counter = 0;
int gef_time_counter1 = 0; 					//Total TDC readout+sorting
int gef_time_counter_tdc[NUM_OF_TDC_MODS];  //vme readout time per tdc
int gef_time_sum = 0;
int gef_time_sum1 = 0;
int gef_time_sum_tdc[NUM_OF_TDC_MODS];
int gef_time_counter_sclr[NUM_OF_SCR_MODS]; //sclr timers
DWORD gef_time_sum_sclr[NUM_OF_SCR_MODS];     //sclr timers
//static struct timeval opt_period = {1,0};
struct timeval prev_tv;
struct timeval prev_tv2;
struct timespec t_start,t_stop;
struct timeval prev_tv_scaler;


/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "k600fevme";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 1000;

/* maximum event size produced by this frontend */
//(working)

INT max_event_size = 10000; //Original value. This value is linked to
//INT max_event_size = 10000; //Original value. This value is linked to
							  //event buffer size.

//INT max_event_size = 11000;  /*  Retief: October 2013 Due to extreme noise with tdc's. */

//INT max_event_size = 5000; //(causes memory instability)

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
//INT event_buffer_size = 15000 * 5000; /*  LCP: (07/03/2012) */
//INT event_buffer_size = 20000 * 5000; //(causes memory instability)

//INT event_buffer_size = 2000 * 10000;

//(working)
INT event_buffer_size = 3000 * 10000;


//INT max_event_size = 1024*sizeof(DWORD);
//INT event_buffer_size = 5000 * (1024*sizeof(DWORD));



/*  QDC Specific Global Variables */
DWORD qevent_num_global = 0;   /* :WORKAROUND:03/08/2013 06:34:42 AM:LCP:  */
int prev_eventcount = 0;
int oldblevel = 0;
int qdc_poll_successfull = 0;
DWORD prev_qdc_event_num = -1; /* :WORKAROUND:03/08/2013 06:34:28 AM:LCP:  */

double global_qdc_poll_time;
double global_qdc_poll_counter;
double global_qdc_read_time;
double global_qdc_read_counter;
int global_qdc_buf_level=0;
int global_qdc_buf_counter=0;
int global_qdc_ave_event=0;
int global_qdc_ave_counter=0;
int global_qdc_evt_zero;

/* SIS */
int sis_oldblevel[NUM_OF_SIS_MODS][NUM_ADC_SIS_CHANNELS];
int global_sis_buf_level[NUM_OF_SIS_MODS][NUM_ADC_SIS_CHANNELS];
int global_sis_counter[NUM_OF_SIS_MODS][NUM_ADC_SIS_CHANNELS];


int global_sis_Nw_average[NUM_OF_SIS_MODS][NUM_ADC_SIS_CHANNELS];
int global_sis_Nw_counter[NUM_OF_SIS_MODS][NUM_ADC_SIS_CHANNELS];



/*  ADC Specific Global Variables */
int adc_oldblevel[NUM_OF_ADC_MODS];
int ADCBrEvntCnt[NUM_OF_ADC_MODS]; //Temp Fix for broken Event counter on ADC
int adcmatchqdc[NUM_OF_ADC_MODS];
int prevadcmatchqdc[NUM_OF_ADC_MODS];
int global_adc_buf_level[NUM_OF_ADC_MODS];
int global_adc_counter[NUM_OF_ADC_MODS];

/*  TDC Specific Global Variables */
int prevtdcblevel[NUM_OF_TDC_MODS];
int tdcmatchqdc[NUM_OF_TDC_MODS];
int prevtdcmatchqdc[NUM_OF_TDC_MODS];
/*  Debug TDC */
int tdc_check_incr[NUM_OF_TDC_MODS];
int tdc_check_counter[NUM_OF_TDC_MODS];

/*  TDC Event Skip/Alignment Stats */
int tdc_skipped_events[NUM_OF_TDC_MODS];			//average events skipped
int tdc_skipped_events_counter[NUM_OF_TDC_MODS]; 	//number of skips 
int tdc_ea_events[NUM_OF_TDC_MODS];					//average ea events
int tdc_ea_events_counter[NUM_OF_TDC_MODS];  		//number of ea's
int prevtdceventcount[NUM_OF_TDC_MODS];
int global_tdc_buf_level[NUM_OF_TDC_MODS];
int global_tdc_counter[NUM_OF_TDC_MODS];
int global_tdc_word_level[NUM_OF_TDC_MODS];
int global_tdc_word_counter[NUM_OF_TDC_MODS];
int global_tdc_ave_event[NUM_OF_TDC_MODS];	   //BigNumber?
int global_tdc_ave_counter[NUM_OF_TDC_MODS];
int global_tdc_evt_zero[NUM_OF_TDC_MODS];

/*  Scaler Specific Global Variables */
int prev_sclrcount[NUM_OF_SCR_MODS];
int prev_sclrblevel[NUM_OF_SCR_MODS];

DWORD prevscalercount[NUM_OF_SCR_MODS][32];

int scaler_channels[NUM_OF_SCR_MODS];

int scaler_prev_buf[NUM_OF_SCR_MODS];

int global_sclr_buf_level[NUM_OF_SCR_MODS];
int global_sclr_counter[NUM_OF_SCR_MODS];

int global_sclr_trigger_count[NUM_OF_SCR_MODS];
int global_sclr_trigger_count_cnt[NUM_OF_SCR_MODS];
int global_sclr_prev_trigcnt[NUM_OF_SCR_MODS];

int myglobalcounter = 0;
int prev_qq =0;
int gimp_counter = 0;
int adc_gimp_counter = 0;
int skip_current_qdc_event = 0;

/*  TDC Poll event counter, diag_sclr[90+k] */
int tdc_poll_cur_evt=0;
int tdc_poll_pre_evt=0;
int tdc_poll_gimp_counter=0;


extern HNDLE hDB;
HNDLE hSet;
HNDLE hkey;
WIRECHAMBER_SETTINGS K600TriggerSettings;
EXP_EDIT exp_edit;

/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event_dma(char *pevent, INT off);
INT read_scaler_event_diag(char *pevent, INT off);

/*-- Equipment list ------------------------------------------------*/
void VMEBASE_SETUP()
{

#ifdef WITH_TDC_MODULE	
//	printf("\t###>>>>> TDC MODULE [%d] Initialization <<<<<#### base 0x%08x\n", 0,V1190_BASE[0] );
//	sleep(10);

	
	V1190_BASE[0]	= 0x80000; /*slot 10*/
	V1190_BASE[1]	= 0xa0000;  /*slot 5*/
	V1190_BASE[2]	= 0xc0000;  /*slot 6*/
	V1190_BASE[3]	= 0xe0000;  /*slot 7*/
	V1190_BASE[4]	= 0x100000; /*slot 8*/
	V1190_BASE[5]	= 0x120000; /*slot 9*/
//	V1190_BASE[6]	= 0x140000; /*slot 10*/
//	V1190_BASE[7]	= 0x160000; /*slot 11*/
#endif
	//V830_BASE[0]	= 0x1c0000; /*slot 14*/
	//V830_BASE[1]	= 0x1e0000; /*slot 15*/
#ifdef WITH_V830_MODULE	
	//V830_BASE[0]	= 0x40000; /*slot 2*/
	V830_BASE[0]	= 0x60000; /*slot 3*/
#endif

	
	//V792_BASE  	    = 0x200000; /*slot 16*/
	//V792_BASE  	    = 0x1a0000; /*slot 13*/
	V792_BASE  	    = 0x1e0000; /*slot 13*/

#ifdef WITH_SIS_MODULE	
//	SIS3302_BASE[0]=0x10000000;
//	SIS3302_BASE[1]=0x20000000;

	SIS3302_BASE[0]=0x10000000;
	SIS3302_BASE[1]=0x20000000;
#endif

#ifdef WITH_ADC_MODULE
//	V785_BASE[0]  = 0x280000;  /*slot 20*/ /*  V785 */

	V785_BASE[0]  = 0x200000;  /*slot 15*/ /*  V785 */
	V785_BASE[1]  = 0x220000;  /*slot 16*/ /*  V785 */
	V785_BASE[2]  = 0x240000;  /*slot 17*/ /*  V785 */
	V785_BASE[3]  = 0x260000;  /*slot 18*/ /*  V785 */
	V785_BASE[4]  = 0x280000;  /*slot 19*/ /*  V785 */
//	V785_BASE[5]  = 0x2A0000;  /*slot 20*/ /*  V785 */

//	V785_BASE[0]  = 0x220000;  /*slot 15*/ /*  V785 */
//	V785_BASE[1]  = 0x240000;  /*slot 16*/ /*  V785 */
//	V785_BASE[2]  = 0x260000;  /*slot 17*/ /*  V785 */
//785_BASE[3]  = 0x260000;  /*slot 18*/ /*  V785 */
//785_BASE[4]  = 0x280000;  /*slot 19*/ /*  V785 */
//785_BASE[5]  = 0x2A0000;  /*slot 20*/ /*  V785 */



/*  12/10/2014: LCP: Removed ADC[2]:0x240000 from readout
 *  due to error. 
 **/

//	V785_BASE[0]  = 0x220000;  /*slot 17*/ /*  V785 */
	//V785_BASE[1]  = 0x240000;  /*slot 18*/ /*  V785 */
//	V785_BASE[1]  = 0x260000;  /*slot 19*/ /*  V785 */
//	V785_BASE[2]  = 0x280000;  /*slot 20*/ /*  V785 */
//	V785_BASE[3]  = 0x2A0000;  /*slot 21*/ /*  V785 */



	 /* :WORKAROUND:06/13/2014 07:31:58 AM:LCP: */
//	V785_BASE[0]  = 0x220000;  /*slot 17*/ /*  V785 */
//	V785_BASE[1]  = 0x260000;  /*slot 19*/ /*  V785 */
//	V785_BASE[2]  = 0x280000;  /*slot 20*/ /*  V785 */
	
#endif

}//VMEBASE_SETUP


/*  
BANK_LIST trigger_bank_list[] = {
	{"QDC0", TID_DWORD,100,NULL},
	{"ADC0", TID_DWORD,100,NULL},
	{"TDC0", TID_DWORD,400,NULL},
	{""},
};

BANK_LIST scaler_bank_list[] = {
	{"SCLR", TID_DWORD,200,NULL},
	{"SCLD", TID_INT,200,NULL},
	{""},
};
*/
EQUIPMENT equipment[] = {
#ifdef FE_WITH_TRIGGER
  {"WireChamber",                  /* equipment name */
    {1, 0,                   	    /* event ID, trigger mask */
     "K600SYSTEMS",               	/* event buffer */
     EQ_POLLED,                     /* equipment type */
     LAM_SOURCE(0, 0xFFFFFF),       /* event source crate 0, all stations */
     "MIDAS",                       /* format */
     TRUE,                          /* enabled */
     RO_RUNNING | 				    /* read only when running */
     RO_ODB,                        /* and update ODB */
    // 50,                            /* poll for 50ms */
     100,                            /* poll for 50ms */
     0,                             /* stop run after this event limit */
     0,                             /* number of sub events */
     0,                             /* don't log history */
     "k600vme2", 			        /* host on which front is running*/	
     "k600fevme", 		  	        /* frontend name*/
     "/root/frontends/k600_dma/k600fevme.c",}, /* source file used for user FE  */	
       read_trigger_event,         /* readout routine*/
    },
#endif
#ifdef FE_WITH_SCALAR
   {"Scaler",                     	/* equipment name */
    {2, 0,                        	/* event ID, trigger mask */
     "K600SYSTEMS",          		/* event buffer */
     EQ_PERIODIC,					/* equipment type */
     0,                           	/* event source */
     "MIDAS",                     	/* format */
     TRUE,                        	/* enabled */
     RO_RUNNING | RO_TRANSITIONS | 	    /* read when running and on transitions */
     RO_ODB,                      	/* and update ODB */
     500,                        	/* read every 500ms */
     0,                           	/* stop run after this event limit */
     0,                             	/* number of sub events */
     1,                           	/* log history */
     "k600vme2", 			/*host on which the front end is running*/
     "k600fevme",              		/*front end name*/
     "/root/frontends/k600_dma/k600fevme.c",}, /* source file used for user FE */
    read_scaler_event_dma,            	/* readout routine */
    },
#endif
#ifdef FE_WITH_DEBUG_SCALARS
   {"DiagScaler",                     	/* equipment name */
    {4, 0,                        	/* event ID, trigger mask */
     "K600SYSTEMS",          		/* event buffer */
     EQ_PERIODIC,					/* equipment type */
     0,                           	/* event source */
     "MIDAS",                     	/* format */
     TRUE,                        	/* enabled */
     RO_RUNNING | RO_TRANSITIONS | 	    /* read when running and on transitions */
     RO_ODB,                      	/* and update ODB */
     1000,                        	/* read every 1 sec */
     0,                           	/* stop run after this event limit */
     0,                             	/* number of sub events */
     1,                           	/* log history */
     "k600vme2", 			/*host on which the front end is running*/
     "k600fevme",              		/*front end name*/
     "/root/frontends/k600_dma/k600fevme.c",}, /* source file used for user FE */
    read_scaler_event_diag,            	/* readout routine */
    },
#endif

   {""}
};

#ifdef __cplusplus
}
#endif

int fpga_setG0(BOOL flag )
{
	int status=0,cmode;
	mvme_get_dmode(k600vme,&cmode);
	mvme_set_dmode(k600vme,MVME_DMODE_D16);

	if( flag ){ 
		//printf(" enabling trigger \n");
		status = mvme_write_value(k600vme, V1495_FPGA2+TRIGGER_REG, 1 );
		
	}else{
		//printf(" disabling trigger \n" );
		status = mvme_write_value(k600vme, V1495_FPGA2+TRIGGER_REG, 0 );
	}
	mvme_set_dmode(k600vme,cmode);
	
	if( status < 0 )
		return status;

	return CM_SUCCESS;
}

#if 0
void open_timers()
{
	GEF_STATUS GEF_STD_CALL result;
	/*  Open Timer */
	result = gefTimerOpen (&timer_handle);
    result = gefTimerGetMode (timer_handle, &opt_mode1); 
	cm_msg(MINFO, "open_timers"," Timer Handle %d , result %d \n",timer_handle,result);
}

void start_timer(int value)
{
	GEF_STATUS GEF_STD_CALL result;
	/*  set Timer Mode */
	opt_period.tv_sec = value;
    result = gefTimerSetPeriod (timer_handle, &opt_period);
}


void close_timers()
{
		GEF_STATUS GEF_STD_CALL   result;
		/*  Close timer */
        printf("Calling gefTimerClose \n");       
        result = gefTimerClose (timer_handle);
}


void checkTempReturns(int theresult )
{

	switch(theresult){
		case GEF_STATUS_BAD_PARAMETER_1: 
			cm_msg(MERROR,"TempSensors","not valid sensor handle\n");
		break;
		case GEF_STATUS_BAD_PARAMETER_2:
			cm_msg(MERROR,"TempSensors","temp paremeter invalid\n");
		break;
		case GEF_STATUS_DRIVER_ERR:
			cm_msg(MERROR,"TempSensors","unexpected driver error\n");
		break;
	}	




}


/*  Temp is returned by gefTempSensorGetTemperature in degrees Celsius. */
void do_temp_sensors()
{
	GEF_TEMP_HDL        handle;
	GEF_TEMP_SENSOR_HDL sensor;
	GEF_STATUS          result;
	unsigned int		sensorcount;
	double 				temp;

	result = gefTempOpen(&handle);
	result = gefTempGetSensorCount(handle,&sensorcount );
	if( sensorcount == 0 ){
		printf ("No sensors registered with the framework.\n");
	}
	/*  BOARD TEMP (internal probe) */
	result = gefTempOpenSensor(handle,0,&sensor);
	result = gefTempSensorGetTemperature(sensor,&temp);
        checkTempReturns(result);	
	diag_sclr[125] = (int)temp;
	temp = 0;
	/*  CPU Probe */
	result = gefTempOpenSensor(handle,1,&sensor);
	result = gefTempSensorGetTemperature(sensor,&temp); 
	checkTempReturns(result);	
	diag_sclr[126] = (int)temp;
	temp = 0;
	/*  Local Probe */
	result = gefTempOpenSensor(handle,2,&sensor);
	result = gefTempSensorGetTemperature(sensor,&temp); 
	checkTempReturns(result);	
	diag_sclr[127] = (int)temp;
	result = gefTempClose(handle);
}
#endif 

/*  TDC event stats. */
void reset_tdc_skip_stats()
{
	int i = 0;
	for(i=0;i<V1190Count;i++){
		tdc_skipped_events[i] = 0;
		tdc_skipped_events_counter[i] = 0;
	}
}


void reset_tdc_ea_stats()
{
	int i = 0;
	for(i=0;i<V1190Count;i++){
		tdc_ea_events[i] = 0;
		tdc_ea_events_counter[i] = 0;
	}
}


void do_tdc_skip_stats()
{
	int i = 0;
	for(i=0;i<V1190Count;i++){
		diag_sclr[170+i] = tdc_skipped_events[i];
	}
}

void do_tdc_ea_stats()
{
	int i = 0;
	for(i=0;i<V1190Count;i++){
		diag_sclr[168+i] = tdc_ea_events[i];
	}
}

void do_tdc_zero_stats()
{
	int i = 0;
	for(i=0;i<V1190Count;i++){
		diag_sclr[180+i] = global_tdc_evt_zero[i];
		global_tdc_evt_zero[i] = 0;
	}
}

void reset_qdc_ave_event()
{
	global_qdc_ave_event = 0;
	global_qdc_ave_counter = 0;
}


/*  QDC Poll timer reset [ Readout Thread ] */
void reset_qdc_poll_timer()
{
	global_qdc_poll_time = 0;
	global_qdc_poll_counter = 0;
}

/*  Readout Thread */
int get_qdc_poll_time_average()
{
	if( global_qdc_poll_counter != 0 )
		return( global_qdc_poll_time/global_qdc_poll_counter );
	return global_qdc_poll_time;
}

int get_qdc_poll_ave_event()
{
	return global_qdc_ave_event;
}

int get_qdc_poll_read_average()
{
	if( global_qdc_read_counter != 0 )
		return( global_qdc_read_time/global_qdc_read_counter );
	return global_qdc_poll_time;
}

/*  MIDAS Thread */
void do_qdc_poll_time()
{
	diag_sclr[20] = get_qdc_poll_read_average();
}

void do_qdc_ave_event()
{
	diag_sclr[115] = get_qdc_poll_ave_event();
}

void do_qdc_read_time()
{
	diag_sclr[21] = get_qdc_poll_read_average();
}

/*  QDC Poll read reset */
void reset_qdc_read_timer()
{
	global_qdc_read_time = 0;
	global_qdc_read_counter = 0;
}

/*  QDC buffer reset */
void reset_qdc_buf_counters(){
	global_qdc_buf_level=0;
	global_qdc_buf_counter=0;
}

/*  count number of events (relative) */
void tdc_event_counter( int ne, int tdc_mod ){
	global_tdc_ave_event[tdc_mod] += ne;
}

void gef_tdc_timer_reset(){
	int i;
	for( i = 0; i < NUM_OF_TDC_MODS;i++){
		gef_time_sum_tdc[i] = 0;
		gef_time_counter_tdc[i] = 0;
	}
}

/*  TDC timers */
void gef_tdc_timer_addcount( int value, int tdc_module )
{
	gef_time_sum_tdc[tdc_module] += value;
	gef_time_counter_tdc[tdc_module]++;
}

int gef_tdc_return_time(int tdc_module)
{
	if(gef_time_counter_tdc[tdc_module] != 0)
		return gef_time_sum_tdc[tdc_module]/gef_time_counter_tdc[tdc_module];
	return 0;
}

/* SCLR Buffer */
void sclr_buffer_level_counter( int blevel, int sclr_mod){
	global_sclr_buf_level[sclr_mod] += blevel;
	global_sclr_counter[sclr_mod]++;
}

int get_sclr_buflevel_average(int sclr_mod )
{
	if( global_sclr_counter[sclr_mod]!= 0 ){
		return (global_sclr_buf_level[sclr_mod]/global_sclr_counter[sclr_mod]);
	}
	return global_sclr_buf_level[sclr_mod];
}

void reset_sclr_bufferlevelcounter(){
	//mset(global_sis_buf_level,0,V785Count*sizeof(int));
	memset(global_sclr_buf_level,0,sizeof(global_sclr_buf_level[0])*NUM_OF_SCR_MODS);
	//mset(global_sis_counter,0,V785Count*sizeof(int));
	memset(global_sclr_counter,0,sizeof(global_sclr_counter[0])*NUM_OF_SCR_MODS);
}

void do_sclr_buf_diag()
{
	int j = 0;
	int gidx = 0;
	for(j=0;j<NUM_OF_SCR_MODS;j++){
			diag_sclr[190+gidx] = get_sclr_buflevel_average(j);
			gidx++;
	}
}


void sclr_trigger_counter( int trig_cnt , int sclr_mod){
	global_sclr_trigger_count[sclr_mod] += trig_cnt;
	global_sclr_trigger_count_cnt[sclr_mod]++;
}

int get_sclr_trigger_count_sec(int sclr_mod )
{
	if( global_sclr_trigger_count[sclr_mod] != 0 ){
		return (global_sclr_trigger_count[sclr_mod]/global_sclr_trigger_count_cnt[sclr_mod]);
	}
	return global_sclr_trigger_count[sclr_mod];
}

void reset_sclr_trig_counter(){
	//mset(global_sis_buf_level,0,V785Count*sizeof(int));
	memset(global_sclr_trigger_count,0,sizeof(global_sclr_trigger_count[0])*NUM_OF_SCR_MODS);
	//mset(global_sis_counter,0,V785Count*sizeof(int));
	memset(global_sclr_trigger_count_cnt,0,sizeof(global_sclr_trigger_count_cnt[0])*NUM_OF_SCR_MODS);
}

void do_sclr_trigger_diag()
{
	int j = 0;
	int gidx = 0;
	for(j=0;j<NUM_OF_SCR_MODS;j++){
			diag_sclr[192+gidx] = get_sclr_trigger_count_sec(j);
			gidx++;
	}
}





/*  SCLR timers */
void gef_sclr_timer_reset(){
	int i;
	for( i = 0; i < NUM_OF_SCR_MODS;i++){
		gef_time_sum_sclr[i] = 0;
		gef_time_counter_sclr[i] = 0;
	}
}

void gef_sclr_timer_addcount( int value, int sclr_module )
{
	gef_time_sum_sclr[sclr_module] += value;
	gef_time_counter_sclr[sclr_module]++;
}

int gef_sclr_return_time(int sclr_module)
{
	if(gef_time_counter_sclr[sclr_module] != 0)
		return gef_time_sum_sclr[sclr_module]/gef_time_counter_sclr[sclr_module];
	return 0;
}

/*  counter in bytes! */
void tdc_buffer_level_counter( int blevel, int tdc_mod ){
	global_tdc_buf_level[tdc_mod] += blevel;
	global_tdc_counter[tdc_mod]++;
}

void tdc_word_level_counter( int nword, int tdc_mod ){
	global_tdc_word_level[tdc_mod] += nword;
	global_tdc_word_counter[tdc_mod]++;
}

int get_tdc_event_average(int tdc_mod)
{
	return global_tdc_ave_event[tdc_mod];
}

int get_tdc_buflevel_average(int tdc_mod)
{
	if( global_tdc_counter[tdc_mod] != 0 ){
		return (global_tdc_buf_level[tdc_mod]/global_tdc_counter[tdc_mod]);
	}
	return global_tdc_buf_level[tdc_mod];
}

int get_tdc_word_average(int tdc_mod)
{
	if( global_tdc_word_counter[tdc_mod] != 0 ){
		return (global_tdc_word_level[tdc_mod]/global_tdc_word_counter[tdc_mod]);
	}
	return global_tdc_word_level[tdc_mod];
}

void reset_wordcounter(){
	memset(global_tdc_word_level,0,NUM_OF_TDC_MODS*sizeof(int));
	memset(global_tdc_word_counter,0,NUM_OF_TDC_MODS*sizeof(int));
}

void reset_bufferlevelcounter(){
	memset(global_tdc_buf_level,0,NUM_OF_TDC_MODS*sizeof(int));
	memset(global_tdc_counter,0,NUM_OF_TDC_MODS*sizeof(int));
}

void reset_tdcaveragecounter(){
	memset(global_tdc_ave_event,0,NUM_OF_TDC_MODS*sizeof(DWORD));
}

/* SIS buffer counters */
void sis_buffer_level_counter( int blevel, int adc_mod ,int adc_chan){
	global_sis_buf_level[adc_mod][adc_chan] += blevel;
	global_sis_counter[adc_mod][adc_chan]++;
}

int get_sis_buflevel_average(int adc_mod, int adc_chan )
{
	
	if( global_sis_counter[adc_mod][adc_chan] != 0 ){
		return (global_sis_buf_level[adc_mod][adc_chan]/global_sis_counter[adc_mod][adc_chan]);
	}
	return global_sis_buf_level[adc_mod][adc_chan];
}

void reset_sis_bufferlevelcounter(){
	//mset(global_sis_buf_level,0,V785Count*sizeof(int));
	memset(global_sis_buf_level,0,sizeof(global_sis_buf_level[0][0])*NUM_ADC_SIS_CHANNELS*NUM_OF_SIS_MODS);
	//mset(global_sis_counter,0,V785Count*sizeof(int));
	memset(global_sis_counter,0,sizeof(global_sis_counter[0][0])*NUM_ADC_SIS_CHANNELS*NUM_OF_SIS_MODS);
}


/* SIS Nw average scaler debug */
void sis_Nw_update( int Nw, int adc_mod ,int adc_chan){
	global_sis_Nw_average[adc_mod][adc_chan] += Nw;
	global_sis_Nw_counter[adc_mod][adc_chan]++;
}



int get_sis_Nw_average(int adc_mod, int adc_chan )
{
	if( global_sis_Nw_counter[adc_mod][adc_chan] != 0 ){
		return (global_sis_Nw_average[adc_mod][adc_chan]/global_sis_Nw_counter[adc_mod][adc_chan]);
	}
	return global_sis_Nw_average[adc_mod][adc_chan];
}


void reset_sis_Nw_counter(){
	//mset(global_sis_buf_level,0,V785Count*sizeof(int));
	memset(global_sis_Nw_average,0,sizeof(global_sis_Nw_average[0][0])*NUM_ADC_SIS_CHANNELS*NUM_OF_SIS_MODS);
	//mset(global_sis_counter,0,V785Count*sizeof(int));
	memset(global_sis_Nw_counter,0,sizeof(global_sis_Nw_counter[0][0])*NUM_ADC_SIS_CHANNELS*NUM_OF_SIS_MODS);
}


/*  ADC counter in bytes! */
void adc_buffer_level_counter( int blevel, int adc_mod ){
	global_adc_buf_level[adc_mod] += blevel;
	global_adc_counter[adc_mod]++;
}

int get_adc_buflevel_average(int adc_mod)
{
	if( global_adc_counter[adc_mod] != 0 ){
		return (global_adc_buf_level[adc_mod]/global_adc_counter[adc_mod]);
	}
	return global_adc_buf_level[adc_mod];
}

void reset_adc_bufferlevelcounter(){
	memset(global_adc_buf_level,0,V785Count*sizeof(int));
	memset(global_adc_counter,0,V785Count*sizeof(int));
}

void do_tdc_buf_diag()
{
	int k = 0;
	for(k = 0;k < V1190Count;k++){
		diag_sclr[7+k] = get_tdc_buflevel_average(k);
	}
}

/*  Gef TDC individual timing(average) */
void gef_tdc_timer_diag()
{
	int i = 0;
	for(i=0;i<V1190Count;i++){
		diag_sclr[142+i] = gef_tdc_return_time(i);
	}
}
/*  Gef Scalar individual timings */
void gef_sclr_timer_diag()
{
	int i = 0;
	for(i=0;i<2;i++){
		diag_sclr[155+i] = gef_sclr_return_time(i);
	}
}

void do_tdc_average_diag()
{
	int k = 0;
	for(k = 0;k < V1190Count;k++){
		diag_sclr[103+k] = get_tdc_event_average(k);
	}
}

void do_tdc_word_diag()
{
	int k = 0;
	for(k = 0;k < V1190Count;k++){
		diag_sclr[80+k] = get_tdc_word_average(k);
	}
}

void do_adc_buf_diag()
{
	int k = 0;
	for(k = 0;k < V785Count;k++){
		diag_sclr[70+k] = get_adc_buflevel_average(k);
	}
}


void do_sis_buf_diag()
{
	int k = 0;
	int j = 0;
	int gidx = 0;
	for(j=0;j<NUM_OF_SIS_MODS;j++){
		for(k = 0;k < SISCount;k++){
			diag_sclr[530+gidx] = get_sis_buflevel_average(j,k);
			gidx++;
		}
	}
}


void do_sis_Nw_diag()
{
	int k = 0;
	int j = 0;
	int gidx = 0;
	for(j=0;j<NUM_OF_SIS_MODS;j++){
		for(k = 0;k < SISCount;k++){
			diag_sclr[501+gidx] = get_sis_Nw_average(j,k);
			gidx++;
		}
	}
}



void do_qdc_buf_diag()
{
	if( global_qdc_buf_counter != 0 )
		diag_sclr[17] = global_qdc_buf_level/global_qdc_buf_counter;
	else
		diag_sclr[17] = 0;
}

int my_readout_enabled()
{
   return my_readout_enabled_flag;
}

void my_readout_enable(BOOL flag)
{
   my_readout_enabled_flag = flag;

      if (flag == 0)
         while (my_readout_thread_active)
            ss_sleep(10);
}



INT reset_vme_counters()
{
	//v792_DataClear(k600vme, V792_BASE);
	v792_EvtCntReset(k600vme,V792_BASE);
	/*  ADC */
#ifdef WITH_ADC_MODULE
	int adc_counter = 0;
	for( adc_counter = 0; adc_counter < V785Count; adc_counter++ ){
		//v792_DataClear(k600vme, V785_BASE[adc_counter]);
		v792_EvtCntReset(k600vme,V785_BASE[adc_counter]);
	}
#endif

#ifdef WITH_TDC_MODULE
	/*  TDC */
	int i=0;
	for(i=0;i<V1190Count;i++) {
		//v1190A_SoftClear(k600vme,V1190_BASE[i]);
		//v1190A_SoftReset(k600vme,V1190_BASE[i]);
		v1190A_SoftEventReset( k600vme, V1190_BASE[i] );
	}//FOR
#endif

	return SUCCESS;
}


/* resets modules output buffers, eventcount registers etc. */
INT reset_vme_modules( INT mode )
{
	int i = 0;

#ifdef WITH_SIS_MODULE
	if(mode==0 && (FRONTEND_NO_SIS == 0) ){
		int  k = 0;
		for( k = 0; k < NUM_OF_SIS_MODS; k++ ) {
		sis3302_Setup(0x1, SIS3302_BASE[k],k);
//		ss_sleep(100);
		/* vme reset after P and G updated, and before key arm issued */
		sis3320_RegisterWrite(k600vme,SIS3302_BASE[k], 0x410 ,0x0);
		sis3320_RegisterWrite(k600vme,SIS3302_BASE[k], 0x41C ,0x0);
		bank1_armed_flag[k] = 0;
		}
//		bank1_armed_flag = 0;
	}else if(mode == 1 && (FRONTEND_NO_SIS==0)){
		/* vme reset */
		//sis3320_RegisterWrite(k600vme,SIS3302_BASE, 0x400 ,0x0);

	}//if-else
	///reset timestamp clear
//	sis3320_RegisterWrite(k600vme,SIS3302_BASE,0x41c,0x0);

#endif

#ifdef WITH_V830_MODULE
	/*  SCLR */
	for(i=0;i<NUM_OF_SCR_MODS;i++) {
		v82X_SoftClear(k600vme,V830_BASE[i]);
	}
#endif
	//	v792_SoftReset(k600vme,V792_BASE);
	//	V792_SETUP();
	/*  QDC */	
	if( mode==0 ){
		v792N_ThresholdWrite(k600vme, V792_BASE,QDC_Thresholds);
		cm_msg(MINFO,"k600fevme","Setting QDC Thresholds \n");
	}


	v792_DataClear(k600vme, V792_BASE);
	v792_EvtCntReset(k600vme,V792_BASE);


//event clear	
	int ecnt = 0;
	DWORD mecnt = 0;
	v792_EvtCntRead( k600vme, V792_BASE , &mecnt );
	ecnt = (int)mecnt;
	prev_eventcount = 0;

#ifdef WITH_TDC_MODULE	
	/*  TDC */
	for(i=0;i<V1190Count;i++) {
		v1190A_SoftClear(k600vme,V1190_BASE[i]);
		//v1190A_SoftReset(k600vme,V1190_BASE[i]);
		v1190A_SoftEventReset( k600vme, V1190_BASE[i] );
		tdcmatchqdc[i] = -1;
		prevtdcmatchqdc[i] = -1;
	}//FOR
#endif

#ifdef WITH_ADC_MODULE
	/*  ADC */
	int adc_counter = 0;
	WORD adc_thold[32];
	for( adc_counter = 0; adc_counter < V785Count; adc_counter++ ){
		
		if( adc_counter < ADCN_START ){
		if( mode==0 ){
			v792_ThresholdWrite(k600vme, V785_BASE[adc_counter],ADC_Thresholds);
			cm_msg(MINFO,"k600fevme","Setting ADC 32 Chan Thresholds, Mod[%d] \n",adc_counter);

		}
		v792_DataClear(k600vme, V785_BASE[adc_counter]);
		v792_EvtCntReset(k600vme,V785_BASE[adc_counter]);
		}

#ifdef WITH_ADC_N_MODULE
		if( adc_counter >= ADCN_START ){
		if( mode==0 ){
			v792N_ThresholdWrite(k600vme, V785_BASE[adc_counter],ADC_Thresholds_N);
			cm_msg(MINFO,"k600fevme","Setting ADC 16 Chan Thresholds, Mod[%d] \n",adc_counter);

		}
		v792N_DataClear(k600vme, V785_BASE[adc_counter]);
		v792N_EvtCntReset(k600vme,V785_BASE[adc_counter]);
		}
#endif
	}//FOR
	
	for( i=0; i< V785Count; i++){
		prevadcmatchqdc[i] = -1;
		adcmatchqdc[i] = -1;
	}
#endif

	return SUCCESS;
}


INT daq_reset()
{
	//if( qevent_num_global == 4000000 ){
	if( qevent_num_global == 4000000 ){
		reset_vme_counters();
		//ss_sleep(5);
	}

	return 0;
}


int ringbuffer_flush(INT run_number, char *error )
{

	int i = 0;
	int status = 0;
	int qdc_blevel = 0;
	int post_qdc_blevel = 0;
	int post_sclr_blevel = 0;
	int sclr_blevel = 0;

#ifdef WITH_TDC_MODULE
	int tdc_blevel = 0;
	int post_tdc_blevel = 0;
#endif

#ifdef WITH_ADC_MODULE
	int post_adc_blevel = 0;
	int adc_blevel = 0;
#endif

#ifdef WITH_SIS_MODULE
	int post_sis_blevel = 0;
	int sis_blevel = 0;
#endif	
	void *ptr;
	void *qptr;

#ifdef WITH_TDC_MODULE
	/*  flush tdcs */
	for( i = 0; i < V1190Count; i++ ){
		tdc_blevel=0;
		rb_get_buffer_level(ringbh_1190[i],&tdc_blevel);
		if( tdc_blevel > 0 ){
			//cm_msg(MINFO,"k600fevme","Flushing TDC[%d], blevel [%d] \n",i,tdc_blevel);
			status = rb_get_rp(ringbh_1190[i], &ptr,0);
			memset(ptr,0,tdc_blevel);
			status=rb_increment_rp(ringbh_1190[i], tdc_blevel );
			rb_get_buffer_level(ringbh_1190[i],&post_tdc_blevel);
			//cm_msg(MINFO,"k600fevme","TDC[%d], blevel [%d] after flushing\n",i,post_tdc_blevel);
		}
	}
#endif

	/*  flush qdc */
	rb_get_buffer_level(ringbh1,&qdc_blevel);
	if( qdc_blevel > 0 ){
		//cm_msg(MINFO,"k600fevme","Flushing QDC, blevel [%d] \n",qdc_blevel);
		status = rb_get_rp(ringbh1, &qptr,0);
		memset(qptr,0,qdc_blevel);
		status=rb_increment_rp(ringbh1, qdc_blevel );
		rb_get_buffer_level(ringbh1,&post_qdc_blevel);
		//cm_msg(MINFO,"k600fevme","QDC , blevel [%d] after flushing\n",post_qdc_blevel);
	}

	ptr = NULL;
	/*  flush SCLRS */
	for( i = 0; i < 2; i++ ){
		sclr_blevel = 0;
		rb_get_buffer_level(ringbh_v830[i],&sclr_blevel);

		
		if( sclr_blevel > 0 ){
		//	cm_msg(MINFO,"k600fevme","Flushing SCLR[%d], blevel [%d] \n",i,sclr_blevel);
			status = rb_get_rp(ringbh_v830[i], &ptr,0);
			memset(ptr,0,sclr_blevel);
			status=rb_increment_rp(ringbh_v830[i], sclr_blevel );
			rb_get_buffer_level(ringbh_v830[i],&post_sclr_blevel);
		//	cm_msg(MINFO,"k600fevme","SCLR[%d], blevel [%d] after flushing\n",i,post_sclr_blevel);
		}

	}

#ifdef WITH_ADC_MODULE
	ptr = NULL;
	/*  flush ADC's */
	for( i = 0; i < V785Count; i++ ){

		adc_blevel = 0;
		rb_get_buffer_level(ringbh_v785[i],&adc_blevel);
		
		if( adc_blevel > 0 ){
		//	cm_msg(MINFO,"k600fevme","Flushing ADC[%d], blevel [%d] \n",i,adc_blevel);
			status = rb_get_rp(ringbh_v785[i], &ptr,0);
			memset(ptr,0,adc_blevel);
			status=rb_increment_rp(ringbh_v785[i], adc_blevel );
			rb_get_buffer_level(ringbh_v785[i],&post_adc_blevel);
		//	cm_msg(MINFO,"k600fevme","ADC[%d], blevel [%d] after flushing\n",i,post_adc_blevel);
		}

	}
#endif

#ifdef WITH_SIS_MODULE
	ptr = NULL;
	int k=0;
	for(k=0;k<NUM_OF_SIS_MODS;k++) {
		for(i=0;i<SISCount;i++) {
			sis_blevel = 0;
			rb_get_buffer_level(ringbh_sis3302[k][i],&sis_blevel);
			if( sis_blevel > 0 ){
				//cm_msg(MINFO,"k600fevme","Flushing QDC, blevel [%d] \n",qdc_blevel);
				status = rb_get_rp(ringbh_sis3302[k][i], &ptr,0);
				memset(ptr,0,sis_blevel);
				status=rb_increment_rp(ringbh_sis3302[k][i], sis_blevel );
				rb_get_buffer_level(ringbh_sis3302[k][i],&post_sis_blevel);
				//cm_msg(MINFO,"k600fevme","QDC , blevel [%d] after flushing\n",post_qdc_blevel);
			}//if
		}//for 2 --- over channels
	}//for 1 --- over modules
#endif	
	return CM_SUCCESS;
}

int frontend_start(INT run_number, char *error )
{
	//ADCBrEvntCnt = 0;
 	int i =0;
	//int status = 0;
	for(i=0;i<V785Count;i++)
	{
		ADCBrEvntCnt[i] = 0;
	}
	my_readout_enable(FALSE);
	reset_vme_modules(0);	

#ifdef WITH_SIS_MODULE	
	if( FRONTEND_NO_SIS == 0 ) {
		
//		if( NUM_OF_SIS_MODS > 1 ){
//			bank1_armed_flag = ArmSample(bank1_armed_flag,SIS3302_BASE_ADDR);
		
//		}else {
#if 1		
		int k=0;
		for(k=0;k<NUM_OF_SIS_MODS;k++){ 


			bank1_armed_flag[k] = ArmSample(bank1_armed_flag[k],SIS3302_BASE[k]);


			//		   status = DualBankArmDisarm();
			if( bank1_armed_flag[k] != 1 ){
				//			   //no reason to continue here
				//			   diag_sclr[20]++;
				//			   //   break;
				cm_msg(MINFO,"frontend_start","Could not arm bank1 for dual bank readout");
			}else if(bank1_armed_flag[k] == 1){
				//ss_sleep(10);
				cm_msg(MINFO,"frontend_start","Bank 1 Armed [%d] for Module [%d]",bank1_armed_flag[k],k);
				//fprintf(fp_thread,"frontend_start: Bank 1 Armed [%d], Module [%d], Base Addrress [0x%x]\n",bank1_armed_flag[k],k,SIS3302_BASE[k]);
			}
		}//FOR

#endif
//	}//if-elese

	}//if
#endif
	//int status = 0;	
	//status = fpga_setG0(1);
	//v82X_SoftClear(k600vme,V830_BASE[1]);
	//v82X_SoftClear(k600vme,V830_BASE[0]);



	gRunStopStatusFlag = FALSE;
	my_readout_enable(TRUE);
	scaler_readout_thread_active = 1;


	//return fpga_setG0(1);
	return CM_SUCCESS;
}

int frontend_stop(INT run_number, char *error)
{
	//int status = 0;
	//status = fpga_setG0(0);
	//ADCBrEvntCnt = 0;
 	int i =0;
	for(i=0;i<V785Count;i++)
	{
		ADCBrEvntCnt[i] = 0;
	}
	
	my_readout_enable(FALSE);
	reset_vme_modules(1);
#ifdef WITH_SIS_MODULE
	if( FRONTEND_NO_SIS == 0 ) {	

//		if( NUM_OF_SIS_MODS > 1 ){ 
//			bank1_armed_flag = DisarmSample(bank1_armed_flag,SIS3302_BASE_ADDR);
//		}else {

			int k = 0;
			for(k=0;k<NUM_OF_SIS_MODS;k++) {
				bank1_armed_flag[k] = DisarmSample(bank1_armed_flag[k],SIS3302_BASE[k]);
			}


//		}//if-else
	}//if
#endif
	gRunStopStatusFlag = TRUE;
    	//int status = 0;	
	//status = fpga_setG0(1);
	my_readout_enable(FALSE);
	scaler_readout_thread_active = 0;
	return CM_SUCCESS;	
}


int isADCReady()
{

	//int treg = 0;
	int t = 0;
	do{
		//treg = v1190A_AlmostFull(k600vme,V1190_BASE[t]);
		//evtst = v1190A_EvtStored(k600vme,V1190_BASE[t]);
		//int evtcnt = v1190A_EvtCounter(k600vme, V1190_BASE[t] );
		//int dready = v1190A_DataReady(k600vme,V1190_BASE[t]);
	    int dready = v792_DataReady(k600vme, V785_BASE[t]);

		//if( evtst >= TDC_HighWater && dready ){
		//int the_tdc_diff = evtcnt - tdc_prev_evnt_count;
		//if( the_tdc_diff >= TDC_HighWater && dready ){
		if(dready) {
			//tdc_prev_evnt_count = evtcnt;
			t++;
			//return 1;
		} //dready 

	}while( t < V785Count );

	return 1;
}
/*
 * 
 * v792 qdc module setup.
 *
 */
void V792_SETUP()
{
	v792_SoftReset(k600vme,V792_BASE);
	v792_Setup2(k600vme,V792_BASE,0x4,"QDC_0");
	cm_msg(MINFO,"k600fevme","Setting QDC , Readout Mode: 1\n");


}

/* 
 *  
 * v785 peak sensing adc module setups.
 *
 */
void V785_SETUP()
{
	int j = 0;
	for( j = 0; j < V785Count; j++ ){

		if( j < ADCN_START ){
		v792_SoftReset(k600vme,V785_BASE[j]);
		char mod_name[8];
		sprintf(mod_name,"ADC_%d",j);
		//v792_Setup2(k600vme,V785_BASE[j],0x3,mod_name);
		/*  Mode 0x4 is for no zero suppression,
		 *  and no overflow suppression. See 'ADC [adc_mod].log'*/
		//v792_Setup2(k600vme,V785_BASE[j],0x3,mod_name);		
		cm_msg(MINFO,"k600fevme","Setting ADC Mod[%d], Mode: 0x4 \n",j);
		v792_Setup2(k600vme,V785_BASE[j],0x4,mod_name);		

		}

#ifdef WITH_ADC_N_MODULE		
		if( j >= ADCN_START ){

		v792N_SoftReset(k600vme,V785_BASE[j]);
		char mod_name[8];
		sprintf(mod_name,"ADC %d",j);
		v792N_Setup(k600vme,V785_BASE[j],0x3);

		}
#endif
	}/* FOR */
	int i =0;
	for( i = 0; i < V785Count; i++ ){
		prevadcmatchqdc[i] = -1;
		adcmatchqdc[i] = -1;
	}
}/*  V785_SETUP  */


void V1190A_SETUP()
{
	int setupmode = 1;
	int counter = 0;
	for(counter=0;counter< V1190Count;counter++) {
		printf("\t###>>>>> TDC MODULE [%d] Initialization <<<<<#### base 0x%08x\n", counter,V1190_BASE[counter] );
		//sleep(10);
		v1190A_Setup(k600vme,V1190_BASE[counter],setupmode);
		//v1190A_GeoWrite(k600vme,V1190_BASE[counter],counter);
		prevtdcblevel[counter] = 0;
		prevtdceventcount[counter] = 0;
		tdcmatchqdc[counter] = -1;
		prevtdcmatchqdc[counter] = -1;

	}
}

void V830_SETUP()
{

	v830_Setup(k600vme,V830_BASE[0],0x8); // 32 channels (ALL) (enabled)
	
	v830_Setup(k600vme,V830_BASE[1],0x8); // 32 channels (all) (enabled)
		scaler_channels[0] = 32;
		scaler_channels[1] = 32;

	
	v82X_SoftClear(k600vme,V830_BASE[0]);
	v82X_SoftClear(k600vme,V830_BASE[1]);

		//int scalercount;
   	//for(scalercount=0;scalercount<NUM_OF_SCR_MODS;scalercount++){
		//v830_Setup(k600vme,V830_BASE[0],0x6); // 12 channels
		
		//v830_Setup(k600vme,V830_BASE[0],0x7); // lowest 20 channels (0-19)
		
		/* New Request for 11 Sept 2015 Weekend */
		
		
	//	v830_Setup(k600vme,V830_BASE[1],0x5); // 8 channels (0-7)
//		v830_Setup(k600vme,V830_BASE[1],0x8); // 32 channels (all) (enabled)
		
	//	v82X_SoftClear(k600vme,V830_BASE[0]);
	//	v82X_SoftClear(k600vme,V830_BASE[1]);
	//}
	//
	//
		/* New Request for 11 Sept 2015 Weekend */
//		v830_Setup(k600vme,V830_BASE[0],0x9); // 32 channels (ALL) (enabled)
		
		
//		 v830_Setup(k600vme,V830_BASE[0],0x5); // 8 channels (0-7)
		
}

/*-----------------------------------------------------------------------------
 *  Module Read Out Function Prototypes
 *-----------------------------------------------------------------------------*/

int read_tdc(DWORD base, DWORD *datareturned , int tdcmodulenum );
int read_buffer_v792(char *pevent, int n_events );
/*-- Sequencer callback info  --------------------------------------*/
void seq_callback(INT hDB, INT hseq, void *info)
{
  //printf("odb ... trigger settings touched\n");
  cm_msg(MINFO,"FRONTEND","odb... wirechamber settings touched ");

}



INT init_vme_modules(INT wcf )
{
	//int csr;
	//int status = 0;
	//	int ccmode;
	//      mvme_get_dmode(k600vme,&ccmode);
	///	mvme_set_dmode(k600vme,MVME_DMODE_D16);
	//	mvme_write_value(k600vme,0x000c800A,1);
	//	mvme_set_dmode(k600vme
	
	char str[256];
	char time_buffer[80];
	time_t t = time(NULL);
	//struct tm tm  =  *localtime(&t);
	struct tm *tm  =  localtime(&t);
	strftime(time_buffer,80,"%F:%H:%M:%S",tm);
	sprintf(str,"INITVME_%s_.log",time_buffer);


	FILE *fp = fopen(str,"a+");


	/* if called from frontend_init */
	//if( wcf == 1 ){
	V792_SETUP();

	//Setting Thresholds
	// WORD thold[16];
	//for (i=0;i<16;i++) {
	//  thold[i] = K600TriggerSettings.v792n_0.thresholds[i];
	//thold[i] = 0x;
	//}
	//v792N_ThresholdWrite(k600vme, V792_BASE,QDC_Thresholds);

	//  }


	v792_ThresholdWrite(k600vme, V792_BASE, QDC_Thresholds );

	//Clearing v792 and checking stat reg
	v792_DataClear(k600vme, V792_BASE);
	v792_EvtCntReset(k600vme,V792_BASE);
	//fprintf(fp,"soft reset v792 \n");

#ifdef WITH_ADC_MODULE
	/*  ADC SETUP */
	//if( wcf == 1 ){

	//v792_SoftReset(k600vme,V792_BASE);
	V785_SETUP();

	//}

	int adc_counter = 0;
	WORD adc_thold[32];
	for( adc_counter = 0; adc_counter < V785Count; adc_counter++ ){


		//  for (i=0;i<32;i++) {
		//	  adc_thold[i] = K600TriggerSettings.v785.thresholds[i];
		//	  //thold[i] = 0x;
		//  }

		if( adc_counter <  ADCN_START)
		{
			v792_ThresholdWrite(k600vme, V785_BASE[adc_counter],ADC_Thresholds);
			cm_msg(MINFO,"k600fevme","Setting ADC 32 Chan Thresholds, Mod[%d] \n",adc_counter);


			v792_DataClear(k600vme, V785_BASE[adc_counter]);
			v792_EvtCntReset(k600vme,V785_BASE[adc_counter]);
		}
#ifdef WITH_ADC_N_MODULE
		if( adc_counter >= ADCN_START ){

			v792N_ThresholdWrite(k600vme, V785_BASE[adc_counter],ADC_Thresholds_N);
			cm_msg(MINFO,"k600fevme","Setting ADC 16 Chan Thresholds, Mod[%d] \n",adc_counter);


			v792N_DataClear(k600vme, V785_BASE[adc_counter]);
			v792N_EvtCntReset(k600vme,V785_BASE[adc_counter]);

		}
#endif
	}//FOR

#endif

#ifdef WITH_TDC_MODULE
	/* if called from frontend_init */
	// if( wcf == 1 ){
	V1190A_SETUP();
	//  }

	int counter_v1190=0;
	// clear TDCS 
	for(counter_v1190=0;counter_v1190<V1190Count;counter_v1190++) {
		v1190A_SoftClear(k600vme,V1190_BASE[counter_v1190]);
		fprintf(fp," Softclear TDC [%d] \n",counter_v1190 );
		//v1190A_SoftReset(k600vme,V1190_BASE[counter_v1190]);
	}
#endif

#ifdef WITH_V830_MODULE
	//int counter_v830=0;

	/* if called from frontend_init */
	//  if( wcf == 1 ){
	V830_SETUP();
	//  }	
	// clear scalers
//	for(counter_v830=0;counter_v830<NUM_OF_SCR_MODS;counter_v830++) {
	//	v82X_SoftClear(k600vme,V830_BASE[counter_v830]);
	//	fprintf(fp," Soft Clear V830 [%d] \n",counter_v830 );
//	}


//	int j_cnt;
//	for( j_cnt = 0; j_cnt < NUM_OF_SCR_MODS; j_cnt++){
//		if( j_cnt == 0){
//			SCLR_CHANNELS[j_cnt] = M_CALLOC(MAXCHANNELS1,sizeof(INT));
//			memset(SCLR_CHANNELS[j_cnt],0,MAXCHANNELS1*sizeof(INT));
//		}else if(j_cnt == 1){
//			SCLR_CHANNELS[j_cnt] = M_CALLOC(MAXCHANNELS2,sizeof(INT));
//			memset(SCLR_CHANNELS[j_cnt],0,MAXCHANNELS2*sizeof(INT));
//		}//if-else
//	}//for
	
//	SCLR_CHANNELS = (**int)M_CALLOC(NUM_OF_SCR_MODS,sizeof(int*));
#endif

#ifdef WITH_SIS_MODULE
	//	init_vme_modules(1);
	/* Resets SIS3302 to Power Up state */
	int k = 0;



	for(k=0;k<NUM_OF_SIS_MODS;k++) {

		cm_msg(MINFO,"k600fevme","Initialize sis3302 ADC:[%d], BASE{0x%X}",k,SIS3302_BASE[k]);
		sis3320_RegisterWrite(k600vme,SIS3302_BASE[k], 0x400 ,0x0);
		ss_sleep(100);


		//	sis3302_ReadVersion();
		/* sis3302 Setup */	
		sis3302_Setup(0x1,SIS3302_BASE[k],k);


/*
		if( k == 0 ){
			sis3320_RegisterWrite(k600vme,SIS3302_BASE[k], 0x30 ,0x800030);
		}else {
			sis3320_RegisterWrite(k600vme,SIS3302_BASE[k], 0x30 ,0x800010);
		}
*/

	}//for

#endif


	fclose(fp);
	return SUCCESS;
}//init_vme_modules

int getSCLRWordCount( DWORD *ptr )
{
	DWORD a = (*ptr);
	int nw = (a>>18)&0x3f;
	return nw;
}

int getSCLRTriggerNum( DWORD *ptr )
{
	DWORD a = (*ptr);
	int trignum = (a&0xffff);
	return trignum;
}


	

int isSCLRHeader( DWORD *ptr )
{
	DWORD a=(*ptr);
	//DWORD trigger_number = a&0xffff;
	//DWORD trigger_number = (a)&0xFFFF;
	DWORD header2 = (a>>26)&0x1;
	DWORD TS = (a>>16)&0x3;
	DWORD geo = ((a>>27)&0x1f);
	
	if( header2 == 0x1 && geo == 0x1f && TS == 0x1 ){
	//if( header2 == 0x3F && TS == 0x1 ){
		return 1;
	}
	return 0;

}



/*  get converted channels from QDC Header word  */
int getConvChannels( DWORD *ptr )
{
	DWORD a=(*ptr); // create local copy
	int channels = (a>>8)&0x3f;
	return channels;
}



/*  event count in TDC Global Header */
int getEventCounter( DWORD *ptr )
{
  DWORD a=(*ptr);
  int eventcount = (a>>5)&0x3fffff;
  return eventcount;
}

int getTDCErrorGlobalTrailer( DWORD *ptr )
{
  DWORD a=(*ptr);
  int status_error = (a>>24)&0x7;

  return status_error;
}


/*  word count TDC Trailer */
int getWordCountTDC( DWORD *ptr )
{
  DWORD a=(*ptr);
  int wordcount = a&0xfff;

  return wordcount;
}

/*  word count GLOBAL Trailer */
int getWordCountGLOBAL( DWORD *ptr )
{
  DWORD a=(*ptr);
  int wordcount = (a>>5)&0xffff;

  return wordcount;
}


int getTDCNUMBER( DWORD *ptr )
{
  DWORD a=(*ptr);
  int tdc = (a>>24)&0x3;

  return tdc;
}

/*  is the word a TDC Global Header */
int isGlobalHeader( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD header = (a>>30)&0x3;
	DWORD geo = (a&0x1f);
	if( header == 0x1 && geo == 0x1f ){
		return 1;
	}
	return 0;
}

/*  is the word a TDC Data Header word */
int isTDCHeader( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD header = (a>>27)&0x7;

	if( header == 0x1 ){
		return 1;
	}
	return 0;
}

/*  is the word a QDC header word  */
int isHeader( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD header = (a&0x07000000)>>24;
	DWORD geo = (a>>27)&0xf;
	DWORD crate = (a>>16)&0x0;

	if( header == 0x02 &&
	    geo == 0x0f &&
	    crate == 0x00 ){
		return 1;
	}
	return 0;
}
/*  is the word a TDC Global Trailer */
int isGlobalTrailer( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD trailer = (a>>27)&0x1f;
	DWORD geo = a&0x1f;
	if( trailer == 0x10 && geo == 0x1f){
		return 1;
	}
	
	return 0;
}



/*  is the word a TDC Data Trailer */
int isTDCTrailer( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD trailer = (a>>27)&0x7;

	if( trailer == 0x3 ){
		return 1;
	}
	
	return 0;
}

/*  is the word a TDC Data Error */
int isTDCError( DWORD *ptr )
{
  	DWORD a=(*ptr);
	DWORD worderror = (a>>27)&0x7;

	if( worderror == 0x4 ){
	  return 1;
	}

	return 0;
}


void checkTDCErrorsLog( DWORD *ptr , int tdc_mod )
{
	DWORD a=(*ptr);

	char str[256];
	char time_buffer[80];
	time_t t = time(NULL);
	//struct tm tm  =  *localtime(&t);
	struct tm *tm  =  localtime(&t);
	
	strftime(time_buffer,80,"%F:%H:%M:%S",tm);
	
	sprintf(str,"TDC_MODULE_%d_%s_.debug",tdc_mod,time_buffer);


	FILE *logfp = fopen(str,"a+");



	int tdc_chip = (a>>24)&0x3;

	fprintf(logfp," Current vword: 0x%x \n",a);
	if( (a&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Hit lost in group 0 from read-out FIFO overflow TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>1)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR:  Hit lost in group 0 from L1 buffer overflow TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>2)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR:  Hit error have detected in group 0 TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>3)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Hit lost in group 1 from read-out FIFO overflow TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>4)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Hit lost in group 1 from L1 buffer overflow TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>5)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Hit error have been detected in group 1 TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>6)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Hit data lost in group 2 from read-out FIFO overflow TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>7)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Hit lost in group 2 from L1 buffer overflow TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>8)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Hit error have been detected in group 2 TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>9)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Hit lost in group 3 from read-out FIFO overflow TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>10)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Hit lost in group 3 from L1 buffer overflow TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}
	if( ((a>>11)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Hit error have been detected in group 3 TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>12)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Hits rejected because of programmed event size limit TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>13)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Event lost(trigger FIFO overflow) TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
	}

	if( ((a>>14)&0x1) == 1){
		fprintf(logfp, "TDC-ERROR: Internal fatal chip error has been dectected TDC Module[%d], TDC Chip[%d]\n",tdc_mod, tdc_chip);
		diag_sclr[40] = 1;
#ifdef TDC_CHIP_DEBUG

		if( FRONTEND_DEBUG_MODE_F == 1 ){
			int value;
			WORD code = 0x3A00;
			value = v1190A_MicroWrite(k600vme, V1190_BASE[tdc_mod], code);
			value = v1190A_MicroRead(k600vme, V1190_BASE[tdc_mod]);

			if( (value&0x1) == 1 )
			{
				fprintf(logfp, "TDC-ERROR: Internal Error Type: Vernier error (DLL unlocked or excessive jitter), DLL signals in a hit measurement indicates that the DLL is not in correct lock. [%d]\n",tdc_mod);
			}

			if( ((value>>1)&0x1) == 1 )
			{
				fprintf(logfp, "TDC-ERROR: Internal Error Type: Coarse error ( parity error on coarse count) [%d]\n",tdc_mod);
			}

			if( ((value>>2)&0x1) == 1 )
			{
				fprintf(logfp, "TDC-ERROR: Internal Error Type: Channel select error ( Synchronisation error ), detected in priority logic used to select channel being written into L1 buffer [%d]\n",tdc_mod);
			}


			if( ((value>>3)&0x1) == 1 )
			{
				fprintf(logfp, "TDC-ERROR: Internal Error Type: L1 buffer parity error [%d]\n",tdc_mod);
			}


			if( ((value>>4)&0x1) == 1 )
			{
				fprintf(logfp, "TDC-ERROR: Internal Error Type: Trigger fifo parity error [%d]\n",tdc_mod);
			}

			if( ((value>>5)&0x1) == 1 )
			{
				fprintf(logfp, "TDC-ERROR: Internal Error Type: Trigger matching error ( state error ), Illegal state detected in trigger matching logic [%d]\n",tdc_mod);
			}

			if( ((value>>6)&0x1) == 1 )
			{
				fprintf(logfp, "TDC-ERROR: Internal Error Type: Readout fifo parity error [%d]\n",tdc_mod);
			}

			if( ((value>>7)&0x1) == 1 )
			{
				fprintf(logfp, "TDC-ERROR: Internal Error Type: Readout state error, Illegal state detected in read-out logic [%d]\n",tdc_mod);
			}

			if( ((value>>8)&0x1) == 1 )
			{
				fprintf(logfp, "TDC-ERROR: Internal Error Type: JTAG Setup Data, Setup parity error [%d]\n",tdc_mod);
			}

			if( ((value>>9)&0x1) == 1 )
			{
				fprintf(logfp, "TDC-ERROR: Internal Error Type: JTAG Control Data, Control parity error [%d]\n",tdc_mod);
			}

			if( ((value>>10)&0x1) == 1 )
			{
				fprintf(logfp, "TDC-ERROR: Internal Error Type: Jtag instruction parity error [%d]\n",tdc_mod);
			}

		}//FRONTEND_DEBUG_MODE_F
#endif//TDC_CHIP_DEBUG
	
	}//TDC Internal Chip Error



	fclose(logfp);

}//checkTDCErrorsLog


void checkTDCErrors( DWORD *ptr , int tdc_mod )
{
	DWORD a=(*ptr);

	int tdc_chip = (a>>24)&0x3;

	if( (a&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Hit lost in group 0 from read-out FIFO overflow TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>1)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR:  Hit lost in group 0 from L1 buffer overflow TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>2)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR:  Hit error have detected in group 0 TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>3)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Hit lost in group 1 from read-out FIFO overflow TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>4)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Hit lost in group 1 from L1 buffer overflow TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>5)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Hit error have been detected in group 1 TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>6)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Hit data lost in group 2 from read-out FIFO overflow TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>7)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Hit lost in group 2 from L1 buffer overflow TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>8)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Hit error have been detected in group 2 TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>9)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Hit lost in group 3 from read-out FIFO overflow TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>10)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Hit lost in group 3 from L1 buffer overflow TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}
	if( ((a>>11)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Hit error have been detected in group 3 TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>12)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Hits rejected because of programmed event size limit TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>13)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Event lost(trigger FIFO overflow) TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
	}

	if( ((a>>14)&0x1) == 1){
		cm_msg(MERROR, "sortTDCData","TDC-ERROR: Internal fatal chip error has been dectected TDC Module[%d], TDC Chip[%d]",tdc_mod, tdc_chip);
		diag_sclr[40] = 1;
#ifdef TDC_CHIP_DEBUG

		if( FRONTEND_DEBUG_MODE_F == 1 ){
			int value;
			WORD code = 0x3A00;
			value = v1190A_MicroWrite(k600vme, V1190_BASE[tdc_mod], code);
			value = v1190A_MicroRead(k600vme, V1190_BASE[tdc_mod]);

			if( (value&0x1) == 1 )
			{
				cm_msg(MERROR, "sortTDCData","TDC-ERROR: Internal Error Type: Vernier error (DLL unlocked or excessive jitter), DLL signals in a hit measurement indicates that the DLL is not in correct lock. [%d]",tdc_mod);
			}

			if( ((value>>1)&0x1) == 1 )
			{
				cm_msg(MERROR, "sortTDCData","TDC-ERROR: Internal Error Type: Coarse error ( parity error on coarse count) [%d]",tdc_mod);
			}

			if( ((value>>2)&0x1) == 1 )
			{
				cm_msg(MERROR, "sortTDCData","TDC-ERROR: Internal Error Type: Channel select error ( Synchronisation error ), detected in priority logic used to select channel being written into L1 buffer [%d]",tdc_mod);
			}


			if( ((value>>3)&0x1) == 1 )
			{
				cm_msg(MERROR, "sortTDCData","TDC-ERROR: Internal Error Type: L1 buffer parity error [%d]",tdc_mod);
			}


			if( ((value>>4)&0x1) == 1 )
			{
				cm_msg(MERROR, "sortTDCData","TDC-ERROR: Internal Error Type: Trigger fifo parity error [%d]",tdc_mod);
			}

			if( ((value>>5)&0x1) == 1 )
			{
				cm_msg(MERROR, "sortTDCData","TDC-ERROR: Internal Error Type: Trigger matching error ( state error ), Illegal state detected in trigger matching logic [%d]",tdc_mod);
			}

			if( ((value>>6)&0x1) == 1 )
			{
				cm_msg(MERROR, "sortTDCData","TDC-ERROR: Internal Error Type: Readout fifo parity error [%d]",tdc_mod);
			}

			if( ((value>>7)&0x1) == 1 )
			{
				cm_msg(MERROR, "sortTDCData","TDC-ERROR: Internal Error Type: Readout state error, Illegal state detected in read-out logic [%d]",tdc_mod);
			}

			if( ((value>>8)&0x1) == 1 )
			{
				cm_msg(MERROR, "sortTDCData","TDC-ERROR: Internal Error Type: JTAG Setup Data, Setup parity error [%d]",tdc_mod);
			}

			if( ((value>>9)&0x1) == 1 )
			{
				cm_msg(MERROR, "sortTDCData","TDC-ERROR: Internal Error Type: JTAG Control Data, Control parity error [%d]",tdc_mod);
			}

			if( ((value>>10)&0x1) == 1 )
			{
				cm_msg(MERROR, "sortTDCData","TDC-ERROR: Internal Error Type: Jtag instruction parity error [%d]",tdc_mod);
			}

		}//FRONTEND_DEBUG_MODE_F
#endif//TDC_CHIP_DEBUG
	
	}//TDC Internal Chip Error





}




DWORD getTDCErrorFlag( DWORD *ptr )
{
	DWORD a=(*ptr);
	
	DWORD eflag = (a&0x7fff);

	return eflag;
}

/*  is the word a TDC Data Measurement */
int isTDCMeasurement( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD tdcm = (a>>27)&0x1f;

	if( tdcm == 0x0 ){
	  return 1;
	}

	return 0;
}

/*  is the word a TDC Filler word */
int isTDCFiller( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD filler = (a>>27)&0x1f;

	if( filler == 0x18 ){
	  return 1;
	}

	return 0;
}



/*  is TDC Data Measurement , trailing or leading. 
 *  inconjunction with above function */
int isTDCTrailingMeasurement( DWORD *ptr )
{
  	DWORD a=(*ptr);
	DWORD istrail = (a>>26);

	if( istrail == 0x1 )
	  return 1;

	return 0;
}


/*  is the word a QDC Trailer word */
int isTrailer( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD trailer = (a&0x07000000)>>24;
	DWORD geo = (a>>27)&0xf;

	if( trailer == 0x04 &&
	    geo == 0x0f
	    ){
		return 1;
	}
	
	return 0;
}

int getTDCChip( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD tdchip = (a>>24)&0x3;

	return tdchip;

}

int getTDCChannel( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD tdcchannel = (a>>19)&0x7f;

	return tdcchannel;

}
int isTDCData( DWORD *ptr )
{
  	DWORD a=(*ptr);
	DWORD tdcdata = (a>>27)&0x7;

	if( tdcdata == 0x0 ||
	    tdcdata == 0x3 ||
	    tdcdata == 0x4 ){

	  return 1;
	}

	return 0;
}


DWORD getTDCEventID( DWORD *ptr )
{

  DWORD a=(*ptr);


  return ((a>>12)&0xfff);
}


/*  is the word QDC EOB  */
int isEOBword( DWORD *ptr )
{

  DWORD a=(*ptr);

  DWORD EOBword = (a&0x07000000)>>24; 

  if( EOBword == 0x06  )
    return 1;

  return 0;
}

/*  is the word ADC Data */
int isDatawordADC( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD datum = (a&0x07000000)>>24;
	DWORD geo = (a>>27)&0xf;
	DWORD channel = (a>>16)&0x1f;

	if( datum == 0x0 &&
			geo == 0x0f
	  ){
		if( channel >= 0 && channel < 33 )
			return 1;
	}



	return 0;
}


/*  is the word QDC Data */
int isDataword( DWORD *ptr )
{
	DWORD a=(*ptr);
	DWORD datum = (a&0x07000000)>>24;
	DWORD geo = (a>>27)&0xf;
	DWORD channel = (a>>16)&0x1f;

	if( datum == 0x0 &&
			geo == 0x0f
	  ){
		if( channel >= 0 && channel < 33 )
			return 1;
	}



	return 0;
}

int getDataChannel( DWORD *ptr )
{
	DWORD a=(*ptr); // create local copy
	int channel = (a>>16)&0x1f;
	return channel;
}

/*  is the word QDC Not Valid Datum */
int notvalidatum( DWORD *ptr )
{
	DWORD a=(*ptr);

	//DWORD EOBword = (a&0x07000000)>>24; 
	DWORD EOBword = (a>>24)&0x7; 
	DWORD geo = (a>>27)&0xf;

	if( EOBword == 0x06  && geo == 0x0f )
		return 1;

	return 0;
}



/*  QDC Invalid */
int isInvalid( DWORD *ptr )
{
	DWORD a=(*ptr);

	//DWORD EOBword = (a&0x07000000)>>24; 
	DWORD EOBword = (a>>24)&0x7; 
	DWORD geo = (a>>27)&0xf;

	if( EOBword == 0x06  && geo == 0x0f )
		return 1;

	return 0;
}

/*  READ ADC */
int read_v785(int base, DWORD* returnData, mvme_size_t d_entries )
{
  	int wcount = 0;
	int w_entries = (int)d_entries;
	int cmode,dmode;
	mvme_get_am(k600vme,&cmode);
	mvme_get_dmode(k600vme,&dmode);
	mvme_set_dmode(k600vme,MVME_DMODE_D32);
	mvme_set_am(k600vme,MVME_AM_A24);
	mvme_set_blt(k600vme,MVME_BLT_BLT32);
	wcount = v792_DataRead(k600vme,base,returnData,&w_entries );
	mvme_set_dmode(k600vme,dmode);
	mvme_set_am(k600vme,cmode);


	if( wcount < 0 )
	  return 0;

	return wcount;
}

/*  READ ADC */
int read_v785_N(int base, DWORD* returnData, mvme_size_t d_entries , int mod)
{
  	int wcount = 0;
	int w_entries = (int)d_entries;
	int cmode;
	mvme_get_dmode(k600vme,&cmode);
	mvme_set_am(k600vme,MVME_AM_A24);
	mvme_set_dmode(k600vme,MVME_DMODE_D32);
	mvme_set_blt(k600vme,MVME_BLT_BLT32);
	//mvme_set_blt(k600vme,MVME_BLT_MBLT64);
	if( mod < ADCN_START ){
		wcount = v792_DataRead(k600vme,base,returnData,&w_entries );
	}
#ifdef WITH_ADC_N_MODULE
	if( mod >= ADCN_START ){
		wcount = v792N_DataRead(k600vme,base,returnData,&w_entries );
	}
#endif
	mvme_set_dmode(k600vme,cmode);

	if( wcount < 0 )
	  return 0;

	return wcount;
}


/*  READ QDC */
int read_v792(int base, DWORD* returnData, mvme_size_t d_entries )
{
  	int wcount = 0;
	int w_entries = (int)d_entries;
	int cmode,dmode;
	mvme_get_am(k600vme,&cmode);
	mvme_get_dmode(k600vme,&dmode);
	mvme_set_am(k600vme,MVME_AM_A24);
	mvme_set_dmode(k600vme,MVME_DMODE_D32);
	mvme_set_blt(k600vme,MVME_BLT_BLT32);
	
//	gefvme_set_dma_channel(0);

	//mvme_set_blt(k600vme,MVME_BLT_MBLT64);
	wcount = v792_DataRead(k600vme,base,returnData,&w_entries );
	mvme_set_dmode(k600vme,dmode);
	mvme_set_am(k600vme,cmode);

	if( wcount < 0 )
	  return 0;

	return wcount;
}

/*  READ TDC */
int read_v1190( int base, DWORD *returnData, mvme_size_t d_entries )
{
	int wcount = 0;
	int cmode,dmode;
	mvme_get_am(k600vme,&cmode);
	mvme_get_dmode(k600vme,&dmode);
	mvme_set_am(k600vme,MVME_AM_A24);
	mvme_set_dmode(k600vme,MVME_DMODE_D32);
	mvme_set_blt(k600vme,MVME_BLT_BLT32);
	
	wcount = v1190A_DataRead(k600vme, base , returnData ,d_entries);
	mvme_set_dmode(k600vme,dmode);
	mvme_set_am(k600vme,cmode);

	if( wcount < 0 )
	  return 0;

	return wcount;
}

/*  READ SCLR */
int read_v830( int base, DWORD* returnData, mvme_size_t d_entries )
{
  	int wcount = 0;
	int w_entries = (int)d_entries;
	int cmode,dmode;
	mvme_get_dmode(k600vme,&dmode);
	mvme_get_am(k600vme,&cmode);
	mvme_set_am(k600vme,MVME_AM_A24);
	mvme_set_dmode(k600vme,MVME_DMODE_D32);
	mvme_set_blt(k600vme,MVME_BLT_BLT32);
	
	wcount = v830_DataRead(k600vme,base,returnData, w_entries );
	mvme_set_dmode(k600vme,dmode);
	mvme_set_am(k600vme,cmode);

	if( wcount < 0 )
	  return 0;

	return wcount;
}

int sortSIS3302Data( DWORD *sorted, DWORD *notsorted , int Dsize , int *Psize, int adc_mod )
{
	int wordcounter = 0;
	DWORD vword;
	int acnt = 0;
	do{
		vword = notsorted[acnt++];
		//sorted[wordcounter++] = vword; //mvme_read does not do byteswapping
		sorted[wordcounter++] = __bswap_32(vword); //mvme_read does not do byteswapping
							   //byteswapping is required, since
							   //concurrentvme board cannot do this
							   //natively.
	//	printf("sortSIS3302Data : 0x%x \n",vword);
	}while(acnt < Dsize);
	*Psize = wordcounter;

	return SUCCESS;
}//sortSIS3302Data


//int sortTDCData( DWORD *sorted, DWORD *notsorted , int Dsize , int *Psize )
int sortTDCData( DWORD *sorted, DWORD *notsorted , int Dsize , int *Psize, int tdc_mod )
{

  DWORD vword;
  int sortindex = 0;
  int kcount = 0;
  int event_count = 0;

 //	FILE *myfp = fopen("SortTDCData.dump","a+");


	do{
		//	vword = notsorted[sortindex];
		vword = __bswap_32( notsorted[sortindex] );
		//cm_msg(MINFO, "sortTDCData","Current TDC sort word: 0x%X ",vword );

		if( vword != 0x0 && !isTDCFiller(&vword)) 
		{
			//if( isGlobalHeader(&vword) || isGlobalTrailer(&vword) || isTDCHeader(&vword) || isTDCTrailer(&vword) || isTDCMeasurement(&vword ) || isTDCError(&vword) && vword != 0x0 && !isTDCFiller(&vword)) 
			//( isGlobalHeader(&vword) || isGlobalTrailer(&vword) || isTDCHeader(&vword) || isTDCTrailer(&vword) || isTDCMeasurement(&vword ) || isTDCError(&vword)) 
			{



				//fprintf(myfp," vword swapped 0x%x, vword unswapped 0x%x \n",vword,notsorted[sortindex]);
				//	cm_msg(MINFO, "sortTDCData","Current TDC sort word: 0x%X ",vword );


				if(isTDCHeader(&vword))
				{
					sorted[kcount++] = vword;
				//		fprintf(myfp," TDC Header vword swapped 0x%x, vword unswapped 0x%x \n",vword,notsorted[sortindex]);
				//	cm_msg(MINFO, "sortTDCData","Current TDC sort word: 0x%X ",vword );


				}

				if(isTDCTrailer(&vword))
				{
						sorted[kcount++] = vword;
				//		fprintf(myfp," TDC Trailer vword swapped 0x%x, vword unswapped 0x%x \n",vword,notsorted[sortindex]);
				//	cm_msg(MINFO, "sortTDCData","Current TDC sort word: 0x%X ",vword );


				}

				if(isTDCMeasurement(&vword)){
						sorted[kcount++] = vword;
				//		fprintf(myfp," TDC Measurement vword swapped 0x%x, vword unswapped 0x%x \n",vword,notsorted[sortindex]);
				//	cm_msg(MINFO, "sortTDCData","Current TDC sort word: 0x%X ",vword );


				}
				if( isGlobalHeader(&vword) ){
					//int t_event = getWordCountGLOBAL(&vword);
					int t_event = getEventCounter(&vword);
					tdc_check_incr[tdc_mod] = 0;
					tdc_check_incr[tdc_mod] = t_event - tdc_check_counter[tdc_mod];

					if( t_event == 0 ){
						global_tdc_evt_zero[tdc_mod]++;
					}else{


						if( tdc_check_incr[tdc_mod] > 1 )
							//if( tdc_check_incr[tdc_mod] > 1 && !(tdc_check_incr[tdc_mod] < 0))
							//if( tdc_check_incr[tdc_mod] != 1 )
							cm_msg(MERROR, "sortTDCData"," TDC[%d] missing event dma transfer: prev [%d] , cur [%d] ",tdc_mod,tdc_check_counter[tdc_mod],t_event );

					}
					event_count++;
					tdc_event_counter( event_count , tdc_mod );
					tdc_check_counter[tdc_mod] = t_event; //last eventnumber
				//	fprintf(myfp," isGlobalheader vword 0x%x , eventnumber %d, last eventnumber %d\n",vword,t_event,tdc_check_counter[tdc_mod]);
					sorted[kcount++] = vword;
				}//If-Header


				if( isGlobalTrailer(&vword) ){
					/*  FUCKING ITALIANS */
					//int status_er_check = getTDCErrorGlobalTrailer(&vword);

					int trigger_lost_ = (vword>>26)&0x1;
					int output_overflow_ = (vword>>25)&0x1;
					int tdc_error_ = (vword>>24)&0x1;

					if(trigger_lost_ == 1){
						cm_msg(MERROR, "sortTDCData","GLOBAL STATUS: TDC [%d] TRIGGER LOST, at least 1 trigger->event lost",tdc_mod );
					}

					if(output_overflow_ == 1){
						cm_msg(MERROR, "sortTDCData","GLOBAL STATUS: TDC[%d] OUTPUT BUFFER OVERFLOW, possible data loss",tdc_mod);
					}

					if(tdc_error_ == 1){
						cm_msg(MERROR, "sortTDCData","GLOBAL STATUS: TDC[%d] ERROR, TDC chip error",tdc_mod );
					}

			//		fprintf(myfp," isGLobalTrailer 0x%x, \n",vword );

					/*  
						switch ( status_er_check ) {
						case 4:{
						cm_msg(MERROR, "sortTDCData","GLOBAL STATUS: TDC[%d] OUTPUT BUFFER OVERFLOW, possible data loss",tdc_mod);
					//cm_msg(MINFO, "sortTDCData","Current TDC sort word: 0x%X ",vword );
					break;
					}
					case 2:
					cm_msg(MERROR, "sortTDCData","GLOBAL STATUS: TDC[%d] ERROR, TDC chip error",tdc_mod );
					break;

					case 1:
					cm_msg(MERROR, "sortTDCData","GLOBAL STATUS: TDC [%d] TRIGGER LOST, at least 1 trigger->event lost",tdc_mod );
					break;

					default:	
					break;
					}				
					*/

					sorted[kcount++] = vword;

				}//ifglobaltrailer

				if( isTDCError(&vword) ){
			//		fprintf(myfp," isTDC Error word 0x%x, \n",vword );
					checkTDCErrors(&vword,tdc_mod);
					//checkTDCErrorsLog(&vword,tdc_mod);
					sorted[kcount++] = vword;
				}

				
			}//if-sort
		}//if-vword-zero
		sortindex++;
  }while( sortindex < Dsize );
  *Psize = kcount;
//   fclose(myfp);
  return SUCCESS;
}//END OF SORTTDCDATA

int qdc_event_incr = 0;
int qdc_event_check = 0;

int sortQDC( DWORD *sorted, DWORD *notsorted , int Dsize , int *Psize )
{
	int wordcounter = 0;
	DWORD vword;
	int acnt = 0;

	do{
		//vword = notsorted[acnt++];
		vword = __bswap_32(notsorted[acnt++]);
		

	//	cm_msg(MINFO, "sortQDC"," current vword 0x%08x, unswapped, 0x%08x",vword,notsorted[acnt-1] );
		//fprintf(fp_thread," current vword swapped 0x%08x, unswapped, 0x%08x\n",vword,notsorted[acnt-1] );
		//printf("vword = 0x%08x\n",vword);

		if( (isHeader(&vword) || isTrailer(&vword) || isDataword(&vword)) && (notvalidatum(&vword) == 0 ) ) 
		{

			sorted[wordcounter++] = vword;

		//	cm_msg(MINFO, "sortQDC"," current vword 0x%08x",vword );
			/*  debug: software event count check for qdc.
			 *  Event count register not usable from manual.*/
			if( isTrailer( &vword ) ){
				int eventnumber = (vword&0xFFFFFF);
				qdc_event_incr = 0;
				qdc_event_incr = eventnumber - qdc_event_check;

	//			cm_msg(MINFO,"sortQDC: Header","QDC data 0x%08x \n", vword );
				if(eventnumber == 0){
					global_qdc_evt_zero++;
				}

				if( qdc_event_incr > 1 && FRONTEND_DEBUG_MODE_E )
					cm_msg(MERROR, "sortQDC"," QDC missing event dma transfer: prev [%d] , cur [%d] ",qdc_event_check,eventnumber );

				qdc_event_check = eventnumber;

			}//if-trailer

			if( isHeader(&vword) ){
				global_qdc_ave_event++;
			}//if-headr

		}//if-check-vword

	}while(acnt < Dsize);
	*Psize = wordcounter;

	return SUCCESS;
}//sortQDC

int adc_event_incr = 0;
int adc_event_check[NUM_OF_ADC_MODS];
/*
 *  Tues, Sept 18, 2012.
 *  Changelog: Changes made here now will reflect a broken ADC module that doesn't have a
 *  correct eventnumber in it's trailer/EOB. the eventcounter[register] seems to be ok.
 *
 */

int sortADCData( DWORD *sorted, DWORD *notsorted , int Dsize , int *Psize, int adc_mod )
{
	int wordcounter = 0;
	DWORD vword;
	int acnt = 0;

	do{
//		vword = notsorted[acnt++];
		vword = __bswap_32(notsorted[acnt++]);
		
		if( (isHeader(&vword) || isTrailer(&vword) || isDatawordADC(&vword)) && (notvalidatum(&vword) == 0 ) ) 
	    {


 			/* :WORKAROUND:06/13/2014 07:54:14 AM:LCP:  
			 *  Here the trailer of all the ADC's within the crate is modified by a software counter. 
			 *  */
			if(FRONTEND_DEBUG_MODE_E){		
				if( isTrailer( &vword ) && ((adc_mod == 1) || (adc_mod == 3) || (adc_mod == 2) || (adc_mod == 0)) ){
					//			//DWORD temper = 0xfc000000 | ADCBrEvntCnt++;
					DWORD temper = 0xfc000000 | ADCBrEvntCnt[adc_mod]++;
					vword = temper;
				}
			}


			sorted[wordcounter++] = vword;

 /*   		  
		  if( isHeader(&vword) || isTrailer(&vword) ){
			 //  if( isHeader(&vword) ){
			//	  cm_msg(MINFO,"sortADCData: Header","ADC Module %d data 0x%08x \n",adc_mod,vword );
			 //  }

			   
			   if( isTrailer(&vword) ){
				  cm_msg(MINFO,"sortADCData: Trailer","ADC Module %d data 0x%08x \n",adc_mod,vword );
			   }
		  }
*/
		  /*  debug: software event count check for adc.
		   *  Event count register not usable from manual.*/
		  if( isTrailer( &vword ) ){
			  int eventnumber = (vword&0xFFFFFF);
			  adc_event_incr = 0;
			  adc_event_incr = eventnumber - adc_event_check[adc_mod];
			  
			  //diag_sclr[405] = eventnumber;

			  if( (adc_event_incr > 1) && FRONTEND_DEBUG_MODE_C ){
			  //if( adc_event_incr > 1 || adc_event_incr < 0){
			  //if( adc_event_incr != 1 )
				  cm_msg(MERROR, "sortADCData"," ADC[%d] M-E dma : prev [%d] , cur [%d], Trailer: 0x%08x",(adc_mod+1),adc_event_check[adc_mod],eventnumber,vword );
				}
			  adc_event_check[adc_mod] = eventnumber;
		  }

		
		}

	}while(acnt < Dsize);
	*Psize = wordcounter;

	return SUCCESS;
}//sortADCData



INT WaitDataReadyB( INT cnt , INT test )
{
	uint32_t armed=0x0;
	uint32_t status_threshold_flags;
	uint32_t stat_thres_adc1_flag ;
	uint32_t stat_thres_adc2_flag ;
	uint32_t stat_thres_adc3_flag ;
	uint32_t stat_thres_adc4_flag ;
	uint32_t stat_thres_adc5_flag ;
	uint32_t stat_thres_adc6_flag ;
	uint32_t stat_thres_adc7_flag ;
	uint32_t stat_thres_adc8_flag ;


	int poll_counter = 0;
	//int status = 0;
	int poll_success=0;
	int count_mods_success=NUM_OF_SIS_MODS;
	int k = 0;
	//	cm_msg(MINFO,"isDataReady", "Waiting for Threshold ... ");
	for(k=0;k<NUM_OF_SIS_MODS;k++) {
		do {
			armed = sis3320_RegisterRead(k600vme,SIS3302_BASE[k],0x10);
			if( armed < 0 )
			{
				//		cm_msg(MINFO,"WaitDataReady", " Things are fucked up: armed<0 : 0x%x ",armed)	;
				//		break;
			}else if( (armed& 0x80000) == 0x80000 ){
				//	cm_msg(MINFO,"WaitDataReady","End Address Threshold Reached : armed: 0x%x , poll_counter: %d ",armed,poll_counter);
				poll_success = 1;
				count_mods_success--;
				break;
			} 


			//printf("armed = 0x%08x " ,armed);
			//if( (armed &0x40000) == 0x40000)
			//	printf("I'm still busy ... \n");
			status_threshold_flags = (armed>>24)&0xff;
			stat_thres_adc1_flag = (status_threshold_flags&0x1);
			stat_thres_adc2_flag = (status_threshold_flags&0x2);
			stat_thres_adc3_flag = (status_threshold_flags&0x4);
			stat_thres_adc4_flag = (status_threshold_flags&0x8);
			stat_thres_adc5_flag = (status_threshold_flags&0x10);
			stat_thres_adc6_flag = (status_threshold_flags&0x20);
			stat_thres_adc7_flag = (status_threshold_flags&0x40);
			stat_thres_adc8_flag = (status_threshold_flags&0x80);

			if(stat_thres_adc1_flag==1){
				poll_success = 1;
				break;
			}

			if( poll_counter == 10000 ){
				cm_msg(MINFO,"WaitDataReadyB","Waiting for Threshold too long....");	
				poll_counter = 0;
			}
			poll_counter++;	
			//		status = cm_yield(0);
			//		ss_sleep(10);
			//	}while( ((armed & 0x80000) != 0x80000) && (gRunStopStatusFlag==FALSE) && (status != RPC_SHUTDOWN) && (status != SS_ABORT));
			//	}while( ((armed & 0x80000) != 0x80000) && my_readout_enabled() );
}while( ((armed & 0x80000) != 0x80000) && (!my_readout_enabled()) );
}//for

if(poll_success == 1 && count_mods_success==0){
	poll_success = 0;
	return 1;
}




return 0;

}//WaitDataReady




int sortSCLR( DWORD *sorted, DWORD *unsorted, int Dsize, int *Psize , int sclrmod )
{

	int wordcounter = 0;
	DWORD vword;
	int acnt = 0;
	//int event_counter=0;
	//int header_found = 0;
	//int sclrwords = 0;
//	FILE *myfp = fopen("SortSCLRData.dump","a+");
	do{
		vword = __bswap_32(unsorted[acnt++]); // checkword and increment
		//vword = (unsorted[acnt++]); // checkword and increment
		//fprintf(myfp,"module [%d] vword 0x%x, unswapped 0x%x\n",sclrmod, __bswap_32(vword),unsorted[acnt++]);
		//fprintf(myfp,"module [%d] vword 0x%x, unswapped 0x%x\n",sclrmod, vword,unsorted[acnt++]);
		if( isSCLRHeader(&vword) == 1 )
		{
			int nwords = getSCLRWordCount(&vword); //word count, excluding the header
			if( nwords == scaler_channels[sclrmod]  ) 
			{
				//int trigger_number = getSCLRTriggerNum(&vword); //add header back in
//				fprintf(myfp,"module [%d] ----- header vword 0x%x, trigger number %d \n",sclrmod, vword,trigger_number);
				sorted[wordcounter++] = vword; // add header, increment to next word
				int i_cnt = 0;
				do{
					vword = __bswap_32(unsorted[acnt++]); // checkword and increment
					//sorted[wordcounter++] = (unsorted[acnt++]);
//					fprintf(myfp,"module [%d] \t data vword 0%x, nwords %d, i_cnt %d\n",sclrmod,vword,nwords,i_cnt);
					sorted[wordcounter++] = vword;
					i_cnt++;
				}while( i_cnt < nwords && acnt < Dsize);
			  }//ifnwords

			//*pdata++ = (0xfd<<24) + adc_module;	
		}//if-header
	}while( acnt < Dsize );

	*Psize = wordcounter;

//   fclose(myfp);
	return SUCCESS;
}

void readADCEventCnt()
{

	int i;

	DWORD current_eventcount = 0;
	for(i=0;i<V785Count;i++){
			v792_EvtCntRead(k600vme, V785_BASE[i], &current_eventcount);

			if( current_eventcount == 0xffffff )
				current_eventcount = 0;
		
			diag_sclr[180+i] = (int)current_eventcount;

	}


}


int my_poll_eventB(INT source, INT count, BOOL test )
{
  DWORD current_eventcount = 0;
  int current_size = 0;
  DWORD the_diff = 0x0;
	
  int drdy = 0;

  if(FRONTEND_DEBUG_MODE){
	  v792_EvtCntRead(k600vme, V792_BASE, &current_eventcount);
	  if( current_eventcount == 0xffffff )
		  current_eventcount = 0;
	  the_diff = current_eventcount - prev_eventcount;
  }

  drdy = v792_DataReady(k600vme, V792_BASE);
// cm_msg(MINFO, "my_poll_event","QDC data ready %d \n",drdy );

 
   //if( current_eventcount >= QDC_HighWater && drdy ){
  //if( the_diff >= QDC_HighWater && drdy ){
  if( drdy ){
	  if(FRONTEND_DEBUG_MODE){
		  prev_eventcount = current_eventcount;
		  diag_sclr[186] = (int)current_eventcount;
		  current_size = the_diff;
		  current_eventcount = 0;
	  }

#if 0			  
		     int debug_qdc_evnt_cnt = 0;
			 v792_EvtCntRead(k600vme,V792_BASE,&debug_qdc_evnt_cnt);
			 cm_msg(MINFO,"readout_thread","QDC Module Event Counter %d\n",debug_qdc_evnt_cnt);
#endif

	return 1;
  }


  return 0;
}

int scaler_meb_counter[NUM_OF_SCR_MODS];
uint32_t sclr_pos_poll_cnt=0;
int poll_scalers()
{

	sclr_pos_poll_cnt++;
	int a = 0;
	int cnt = 0;
	int lcnt = 0;
	//	int cnt_check[NUM_OF_SCR_MODS];
	//	FILE *sfp = fopen("pollScalers.debug","a+");
	//	while( cnt < 2 ) {
	for( a=0;a<NUM_OF_SCR_MODS;a++){
		int drdy =0;
		drdy =  v830_isDataReady(k600vme,V830_BASE[a]);

		if(drdy == 1 ){
			//			cnt_check[a] = drdy;
			cnt++;
			//			fprintf(sfp,"SCLR MOD %d, data ready %d, pattern(0x%x) ,cnt %d \n",a,drdy,drdy,cnt);
		}
	}
	//	lcnt++;
	//	if(lcnt > 1000)
	//		break;
	//	}//while

	if( cnt == 2 ){
	//if( cnt > 0 ){ // Test Case
//		fprintf(sfp,"count = %d ,called [%u] \n",cnt,sclr_pos_poll_cnt);
//		fclose(sfp);

		return 1;
	}//if-cnt-2
//	fprintf(sfp,"count = %d ,called [%u] \n",cnt,sclr_pos_poll_cnt);
//	fclose(sfp);	
//	if( cnt > 0 ){
//		int myj = 0;
//		for(myj=0;myj<NUM_OF_SCR_MODS;myj++){
//			//cm_msg(MINFO,"poll_scaler","SCLR [%d] was not ready : drdy value = %d\n",myj, cnt_check[myj]);
//			if( cnt_check[myj] != 1 )
//			fprintf(sfp,"SCLR <%d> not ready  ,cnt [%d] \n",myj,cnt_check[myj]);
//	
//		}
//	}	
//	fclose(sfp);	
	
	return 0;
}

int my_readout_thread(void *param)
{
   int status, source;
   void *p;
   int qdata_size = 0;
   int pqdata_size = 0;
   int status_sort = 0; 
   int datacnt = 0;
   int tdcmodulecounter = 0;
   int tdcdata_size = 0;
#ifdef WITH_ADC_MODULE   
   int adcdata_size = 0;
   int adcmodulecounter = 0;
   int adc_datacnt = 0;
#endif   

#ifdef WITH_SIS_MODULE   
   int sisdata_size = 0;
   int sischannelcounter = 0;
   int sis_datacnt = 0;
   uint32_t dma_got_nof_lwords=0x0;
   uint32_t next_sample_addr=0x0;
   uint32_t prev_bank_sample_addr=0x0;
   uint32_t return_code=0;
   uint32_t calc_event_length=0;
   uint32_t bank_flag=0; 
   uint32_t adc_mem_bank2_addr_offset=0x0;
   DWORD *sisdata_before,*sisdata_after; //adc
#endif   
	   int sa = 0;
   //int post_data_size = 0;

   int data_ar_length = 0;
   mvme_size_t dma_trans_size = 0; //bytes - qdc
   mvme_size_t tdc_dmatranssize = 0; //bytes - tdc
   mvme_size_t adc_dmatranssize = 0; //bytes - adc

   data_ar_length = QDC_HighWater*34;
   ///dma_trans_size = QDC_HighWater*34*sizeof(DWORD);
   dma_trans_size = QDC_HighWater*34;

   //tdc_dmatranssize = TDC_HighWater*128 + 2;//bytes = 128 words
   adc_dmatranssize = dma_trans_size;//ADC
   tdc_dmatranssize = 512;//bytes = 128 words
   //int tdc_ar_length = TDC_HighWater*200;
   int tdc_ar_length = 2050;
   int adc_ar_length = data_ar_length; //ADC data aray length



   DWORD *qdata,*pqdata;	// qdc
   DWORD *tdcdata_before,*tdcdata_after; //tdc
   DWORD *adcdata_before,*adcdata_after; //adc
   DWORD *sdata,*usdata;
   
   qdata = M_CALLOC(data_ar_length,sizeof(DWORD));
   pqdata = M_CALLOC(data_ar_length,sizeof(DWORD));
   
   tdcdata_before = M_CALLOC(tdc_ar_length,sizeof(DWORD));
   tdcdata_after = M_CALLOC(tdc_ar_length,sizeof(DWORD));

   adcdata_before = M_CALLOC(adc_ar_length,sizeof(DWORD));
   adcdata_after = M_CALLOC(adc_ar_length,sizeof(DWORD));

   int sclr_dma = 33;
   int total_words_sclr = sclr_dma*NUM_OF_SCR_MODS;


    sdata = M_CALLOC(total_words_sclr,sizeof(DWORD));
    usdata = M_CALLOC(total_words_sclr,sizeof(DWORD));

	//GEF_STATUS GEF_STD_CALL   result;
//    struct timeval v1;
//	struct timeval v1_tdc;

	if( qdata == NULL  )
	   frontend_exit();


   p = param; /* avoid compiler warning */
   while (!my_stop_all_threads) {
      /* obtain buffer space */

      status = rb_get_wp(ringbh1, &p, 0);
      if (my_stop_all_threads)
         break;
      if (status == DB_TIMEOUT) {
         //ss_sleep(10);
         continue;
      }
      
      if (status != DB_SUCCESS){
         break;
      }

	  if (my_readout_enabled()) {
		  /* indicate activity for readout_enable() */
		  my_readout_thread_active = 1;

#ifdef WITH_V830_MODULE

		  /*  Poll And Readout Scalers  */
		  if( poll_scalers() == 1) {
//		  if(1) {
	
			   if(my_stop_all_threads){
					  break;
			 	}


			   for( sa = 0; sa < NUM_OF_SCR_MODS; sa++ ){

				   //			memset(sdata,0,total_words_sclr*sizeof(DWORD));
				   //			memset(usdata,0,total_words_sclr*sizeof(DWORD));
#if 0					
				   if(FRONTEND_DEBUG_MODE_TIMER){
					   start_timer(1);
				   }
#endif 
//				   int sclr_drdy = 0;
//				   sclr_drdy =  v830_isDataReady(k600vme,V830_BASE[sa]);

//				   if(sclr_drdy == 1) {
					   memset(sdata,0,total_words_sclr*sizeof(DWORD));
					   memset(usdata,0,total_words_sclr*sizeof(DWORD));

#if 0
		int trig_cnt = v830_ReadTriggerCounter(k600vme,V830_BASE[sa]);
	    int evt_cnt  = v830_EvtCounter(k600vme,V830_BASE[sa]);
        cm_msg(MINFO,"poll_scalers","Sclr Module %d , EveCnt[%d], TrigCnt[%d]\n",sa,evt_cnt,trig_cnt);
#endif					   


					   int wc = read_v830( V830_BASE[sa],usdata, sclr_dma );

#if 0
					   if(FRONTEND_DEBUG_MODE_TIMER){
						   result = gefTimerGetRemaining (timer_handle, &v1_tdc);
						   int gef_time_sum_tsclr = 0;
						   gef_time_sum_tsclr = (1000000 - v1_tdc.tv_usec);
						   gef_sclr_timer_addcount(gef_time_sum_tsclr,sa);
					   }
#endif
					   if( wc > 0 ){
						   int pwc = 0;
						   sortSCLR( sdata,usdata,wc,&pwc ,sa);


						   if( pwc > 0 ){
							   void *sclrwp = NULL;
							   rb_get_wp(ringbh_v830[sa], &sclrwp, 0);
							   memcpy( sclrwp , sdata, pwc*sizeof(DWORD) );
							   rb_increment_wp(ringbh_v830[sa], pwc*sizeof(DWORD) );
						   }
					   }
					   /*  DEBUG CODE [ NOT FOR LIVE RUNS] */
					   //if(FRONTEND_DEBUG_MODE){
					   if(0){
						   diag_sclr[145+sa] = v830_ReadTriggerCounter(k600vme,V830_BASE[sa]);
						   diag_sclr[147+sa] = v830_EvtCounter(k600vme,V830_BASE[sa]);
					   }
//				   }//If-data ready

			   }/* FOR---loop scaler readout */

		  }/*  IF-poll_scalers */
#endif
	
	  /* check for new event */
		  source = my_poll_eventB(0,0,FALSE);
		  
		  if (source > 0 ) {
			  diag_sclr[140] += source;
			  if(my_stop_all_threads){
				  break;
			  }
			  memset(pqdata,0,data_ar_length*sizeof(DWORD));
			  memset(qdata,0,data_ar_length*sizeof(DWORD));
			 
#if 0
			  if(FRONTEND_DEBUG_MODE_TIMER){
				  start_timer(1);
			  }
#endif

			  qdata_size = read_v792(V792_BASE, qdata ,dma_trans_size );


#if 0
			  /*  Temp Timer in History output */
			  if(FRONTEND_DEBUG_MODE_TIMER){
				  result = gefTimerGetRemaining (timer_handle, &v1);
				  //usec-resolution
				  gef_time_sum = (1000000 - v1.tv_usec);
				  diag_sclr[131] = gef_time_sum; //not average time


				  //gef_time_sum += ;
				  //diag_sclr[128] = v1.tv_usec-v2.tv_usec;
				  //cm_msg(MINFO, "readout_thread","qdc time, timer_handle %d, time %d \n",timer_handle,diag_sclr[128] );


				  gef_time_counter++; //average time SCLD[128] qdc in History output
			  }//FRONTEND_DEBUG_MODE_TIMER 
#endif 
			  if( qdata_size > 0  ){

				  status_sort = 0;	
				  status_sort = sortQDC(pqdata,qdata,qdata_size,&pqdata_size);

				  /* check event size */
				  if( pqdata_size + sizeof(EVENT_HEADER) > (DWORD) max_event_size) {
					  cm_msg(MERROR, "readout_thread",
							  "Event size %ld larger than maximum size %d",
							  (long) (pqdata_size + sizeof(EVENT_HEADER)),
							  max_event_size);
					  assert(FALSE);
				  }//if-check event size!
					
			 
#ifdef WITH_ADC_MODULE
	
				  //Do ADC READOUT
				  if(my_stop_all_threads){
					  printf("breaking \n");
					  break;
				  }


		  
				  //if( isADCReady() == 1 ){
				  for( adcmodulecounter = 0; adcmodulecounter < V785Count; adcmodulecounter++ ){
					  void *adcwp = NULL;

					  /* check if ring buffer handle is 0, if so exit entire frontend cant continue */
						  if( ringbh_v785[adcmodulecounter] == 0){
							  cm_msg(MERROR,"readout_thread","ADC ringbuffer handle %d, is zero\n",adcmodulecounter);
							  frontend_exit();
						  }

						  /* get the write pointer from the ring buffer */
						  status = rb_get_wp(ringbh_v785[adcmodulecounter], &adcwp, 0);
						  if( status == DB_TIMEOUT ){
							  //cm_msg(MINFO,"readout_thread","ADC %d, DB_TIMEOUT\n",adcmodulecounter);
							  //ss_sleep(10);
							  continue;
						  }

			//			 int debug_adc_evnt_cnt = 0;
		//				 v792_EvtCntRead(k600vme,V785_BASE[adcmodulecounter],&debug_adc_evnt_cnt);
	//					 cm_msg(MINFO,"readout_thread","ADC Module[%d] Event Counter %d\n",adcmodulecounter,debug_adc_evnt_cnt);

						  memset(adcdata_before,0,adc_ar_length*sizeof(DWORD));
						  memset(adcdata_after,0,adc_ar_length*sizeof(DWORD));
						  adcdata_size = 0;
						  adcdata_size = read_v785(V785_BASE[adcmodulecounter],adcdata_before,adc_dmatranssize);

						  /*  DEBUG: Check Event Counter */
						  if(FRONTEND_DEBUG_MODE) {
							  if( adcdata_size < 0 ){
								  int debug_adc_evnt_cnt = 0;
								  v792_EvtCntRead(k600vme,V785_BASE[adcmodulecounter],&debug_adc_evnt_cnt);
								  cm_msg(MINFO,"readout_thread","ADC Module Event Counter %d\n",debug_adc_evnt_cnt);
							  }
						  
						      readADCEventCnt();
						  }
	
						  diag_sclr[194] = adcdata_size;
						  /*  successfull dma transfer of tdc's data */
						  if( adcdata_size > 0 ){
							  adc_datacnt = 0;
							  int adcsort_status =  0; /*  to stop compiler warning */
							  
							  adcsort_status  = sortADCData( adcdata_after , adcdata_before , adcdata_size , &adc_datacnt, adcmodulecounter );

							  /* check event size */
							  if ( adc_datacnt + sizeof(EVENT_HEADER) > (DWORD) max_event_size) {
								  cm_msg(MERROR, "readout_thread",
										  "Event size %ld larger than maximum size %d",
										  (long) (adc_datacnt + sizeof(EVENT_HEADER)),
										  max_event_size);
								  assert(FALSE);
							  }
						  }//if-sort and size check

						  /* put adc data in ring buffer. Each adc has its own ringbuffer */
						  /* checks t//he buffer level for the specific tdc's ring buffer */
						  memcpy( adcwp , adcdata_after, adc_datacnt*sizeof(DWORD) );
						  rb_increment_wp(ringbh_v785[adcmodulecounter], adc_datacnt*sizeof(DWORD) );

				  }//FOR-ADC READOUT
#endif

				 //}//isADCREady

#ifdef WITH_TDC_MODULE				  
				  /*  DO HERE TDC READOUT */
			//	diag_sclr[141]++;
				  //printf("Entering CAEN v1190 Readout\n");
				  if(my_stop_all_threads){
					//  printf("breaking \n");
					  break;
				  }
				
				  //start_timer(1);
				  /*  Loop over all the tdcs */
				  for( tdcmodulecounter = 0; tdcmodulecounter < V1190Count; tdcmodulecounter++ ) {
					  
					  /* set write pointer */
					  void *tdcwp = NULL;	

					  /* get the write pointer from the ring buffer */
					  status = rb_get_wp(ringbh_1190[tdcmodulecounter], &tdcwp, 0);
					  if( status == DB_TIMEOUT )
						  continue;

					  /* check if ring buffer handle is 0, if so exit entire frontend cant continue */
					  if( ringbh_1190[tdcmodulecounter] == 0){
						  frontend_exit();
					  }

					  memset(tdcdata_before,0,tdc_ar_length*sizeof(DWORD));
					  memset(tdcdata_after,0,tdc_ar_length*sizeof(DWORD));

			  		  //printf(" does TDC Module[%d] have data ready %d \n",tdcmodulecounter,v1190A_DataReady(k600vme, V1190_BASE[tdcmodulecounter]));
					  tdcdata_size = read_v1190(V1190_BASE[tdcmodulecounter],tdcdata_before,tdc_dmatranssize);

					  /*  successfull dma transfer of tdc's data */
					  if( tdcdata_size > 0 ){
						  datacnt = 0;
						  diag_sclr[141]++;
						  diag_sclr[40] = 0; //TDC Internal chip error debug ( this sets a temp alarm )
						  int tdcsort_status =  0; /*  to stop compiler warning */
						  tdcsort_status  = sortTDCData( tdcdata_after , tdcdata_before , tdcdata_size , &datacnt, tdcmodulecounter );
						  
						  if(my_stop_all_threads){
							  printf("breaking \n");
							  break;
						  }

						  /* check event size */
						  if ( datacnt + sizeof(EVENT_HEADER) > (DWORD) max_event_size) {
							  cm_msg(MERROR, "readout_thread",
									  "Event size %ld larger than maximum size %d",
									  (long) (datacnt + sizeof(EVENT_HEADER)),
									  max_event_size);
							  assert(FALSE);
						  }
					  
						  if( datacnt > 0 ){	  
							   /* put tdc data in ring buffer. Each tdc has its own ringbuffer */
							  /* checks the buffer level for the specific tdc's ring buffer */
							  memcpy( tdcwp , tdcdata_after, datacnt*sizeof(DWORD) );
							  rb_increment_wp(ringbh_1190[tdcmodulecounter], datacnt*sizeof(DWORD) );

						  }

					  }//if-sort and size check

					  /* put tdc data in ring buffer. Each tdc has its own ringbuffer */
					  /* checks the buffer level for the specific tdc's ring buffer */
					  //memcpy( tdcwp , tdcdata_after, datacnt*sizeof(DWORD) );
					  //rb_increment_wp(ringbh_1190[tdcmodulecounter], datacnt*sizeof(DWORD) );

				  }//for loop over all tdc's
#endif

#ifdef WITH_SIS_MODULE
				  if(my_stop_all_threads){
					  //  printf("breaking \n");
					  break;
				  }

				  //Wating for End Threshold Flag 
				  //to be set
				  //if ( WaitDataReadyB(0,0)) {
				  int k_loop_sis_mod = 0;
				  int g_sis_chan_counter = 0;

				  for( k_loop_sis_mod=0; k_loop_sis_mod < NUM_OF_SIS_MODS; k_loop_sis_mod++) {
				
			  
			
			   	   //  bank1_armed_flag = ArmSample(bank1_armed_flag,SIS3302_BASE[k_loop_sis_mod]);


					 DWORD _armed_ = sis3320_RegisterRead(k600vme,SIS3302_BASE[k_loop_sis_mod],SIS3302_ACQUISTION_CONTROL);
					  
					// fprintf(fp_thread,"sis3302 check end threshold 0x%x , module [%d], at addr 0x%x \n",_armed_,k_loop_sis_mod,SIS3302_BASE[k_loop_sis_mod]); 
					  
					  if (  ((_armed_ & 0x80000) == 0x80000) && (FRONTEND_NO_SIS == 0)) {
					 // if (  ((_armed_ & 0x80000) == 0x80000) ) {
					//  fprintf(fp_thread,"sis3302 reached!! end threshold 0x%x , module [%d], at addr 0x%x \n",_armed_,k_loop_sis_mod,SIS3302_BASE[k_loop_sis_mod]); 
					
						   //printf("Entering SIS3302 Readout\n");
						  //returns bank1 flag 
						  //					  printf("DualBankArmDisarm\n");  
#ifdef DUALBANKREADOUT 
						  bank1_armed_flag[k_loop_sis_mod] = DualBankArmDisarm(bank1_armed_flag[k_loop_sis_mod],SIS3302_BASE[k_loop_sis_mod]);
#else					 
						  bank1_armed_flag[k_loop_sis_mod] = DisarmSample(bank1_armed_flag[k_loop_sis_mod],SIS3302_BASE[k_loop_sis_mod]);
#endif					 
						  //if( status == 1 ){
						  //no reason to continue here
						  //diag_sclr[20]++; //bank1 is armed
						  //   break;
						  //  }else{
						  //diag_sclr[40]++; //bank2 is armed
						  // }

						  //Here we have to wait and check if sampling has finished 
						  //for the specific bank
						  adc_mem_bank2_addr_offset=0;
#ifdef DUALBANKREADOUT
						  if(bank1_armed_flag[k_loop_sis_mod]==1){
							  adc_mem_bank2_addr_offset = 0x4;
						  }//if-check-bank1-armed

						  status = DualBankCheckIfSwapped(bank1_armed_flag[k_loop_sis_mod],SIS3302_BASE[k_loop_sis_mod]);
						  if(status != 1 ){
							  diag_sclr[341+k_loop_sis_mod]++;
						  }else{
							  //    ss_sleep(10);
						  }//if-bank-swap-check
#endif

						  //struct timeval t1,t2,t3,t4;
						  //int dt1,dt2,dt3;

						  for( sischannelcounter = 0; sischannelcounter < SISCount; sischannelcounter++ ){
							  void *siswp = NULL;

							  if( ringbh_sis3302[k_loop_sis_mod][sischannelcounter] == 0){
								  cm_msg(MERROR,"my_readout_thread","SIS[%d], ADC ringbuffer handle for Chan %d, is zero\n",k_loop_sis_mod,sischannelcounter);
								  frontend_exit();
							  }//if-ringbh

							  /* get the write pointer from the ring buffer */
							  status = rb_get_wp(ringbh_sis3302[k_loop_sis_mod][sischannelcounter], &siswp, 0);
							  if( status == DB_TIMEOUT ){
								  cm_msg(MINFO,"my_readout_thread","SIS[%d], readoutthread DB Timeout Chan %d", k_loop_sis_mod,sischannelcounter);
								  //ss_sleep(10);
								  continue;
							  }//if-rb-get-wp

							  //						  printf("Next Sample address Read\n");
							  next_sample_addr = 0x0;
							  switch(sischannelcounter) {
								  case 0: next_sample_addr = sis3320_RegisterRead(k600vme, SIS3302_BASE[k_loop_sis_mod], SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC1 );
										  //next_sample_addr = sis3320_RegisterRead(k600vme, SIS3302_BASE, SIS3302_ACTUAL_SAMPLE_ADDRESS_ADC1 );
										  //cm_msg(MINFO,"readout_thread","next sample address channel 0");
										  break;	
								  case 1: next_sample_addr = sis3320_RegisterRead(k600vme, SIS3302_BASE[k_loop_sis_mod], SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC2 );
										  //cm_msg(MINFO,"readout_thread","next sample address channel 1");
										  break;	
								  case 2: next_sample_addr = sis3320_RegisterRead(k600vme, SIS3302_BASE[k_loop_sis_mod], SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC3 );
										  break;	
								  case 3: next_sample_addr = sis3320_RegisterRead(k600vme, SIS3302_BASE[k_loop_sis_mod], SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC4 );
										  break;	
								  case 4: next_sample_addr = sis3320_RegisterRead(k600vme, SIS3302_BASE[k_loop_sis_mod], SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC5 );
										  break;	
								  case 5: next_sample_addr = sis3320_RegisterRead(k600vme, SIS3302_BASE[k_loop_sis_mod], SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC6 );
										  break;	
								  case 6: next_sample_addr = sis3320_RegisterRead(k600vme, SIS3302_BASE[k_loop_sis_mod], SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC7 );
										  break;	
								  case 7: next_sample_addr = sis3320_RegisterRead(k600vme, SIS3302_BASE[k_loop_sis_mod], SIS3302_PREVIOUS_BANK_SAMPLE_ADDRESS_ADC8 );
										  break;	
							  };
							  bank_flag = ((next_sample_addr>>24)&0x1);
							  if( bank_flag != bank1_armed_flag[k_loop_sis_mod] ) { // Disarmed
								  diag_sclr[320+k_loop_sis_mod]++;
								  //DualBankCheckIfSwapped(bank_flag);
							  }//if-bank-flag-check


							  //mask out bank bit
							  next_sample_addr = next_sample_addr & 0xffffff;
							  uint32_t max_event_saved = 0;
							  uint32_t max_event_use  = 0;
							  if( next_sample_addr > 0x3fffff ){
								  cm_msg(MINFO,"my_readout_thread","SIS[%d], Chan[%d], More than 1 Page mem used next_sample_addr: 0x%08x",k_loop_sis_mod,sischannelcounter,next_sample_addr&0xffffff);
								  max_event_saved = next_sample_addr / event_length_lwords;
								  next_sample_addr = 2 * (( max_nof_events - 1 ) *event_length_lwords);
								  max_event_use = next_sample_addr/event_length_lwords;
								  //printf(" greater than 1 page of mem used \n");
							  }//if-next-sample-size-check


							  //create buffer length to readout.
							  uint32_t  buffer_length = 0;
							  if(next_sample_addr != 0x0 ) {
								  if( next_sample_addr >= 0x400000 ){
									  buffer_length = 0x200000;
								  }else{	
									  buffer_length = ((next_sample_addr & 0x3ffffc) >> 1); // length in lwords 
									  uint32_t buffer_length_size_check = buffer_length*sizeof(DWORD); // bytes

									  if( buffer_length_size_check < 128 )								  
										  buffer_length = 34; // length in lwords
									  //vmic dma requires >= 128 bytes to start

#ifdef DEBUG_SIS_3
									  cm_msg(MINFO,"my_readout_thread","SIS[%d],Chan[%d],buffer_length before check: [%d]",k_loop_sis_mod,sischannelcounter,buffer_length);	
#endif		
									  int r = buffer_length % 4;
									  if( r != 0 ){
										  double d1 = (double)buffer_length;
										  double d2 = 4.0;
										  int t = ceil(d1/d2);
										  buffer_length = t * 4;

									  }
								
								  
								  }//IF-ELSE ( nextsameple >= 0x400000)

								  // printf("buffer length : %d \n",buffer_length);
								  // printf("next sample addr: 0x%x \n",next_sample_addr);
#ifdef DEBUG_SIS_3
								  cm_msg(MINFO,"my_readout_thread","SIS[%d], nextsample: 0x%x , buffer_length : [%d], Chan [%d] ",k_loop_sis_mod,next_sample_addr,buffer_length, sischannelcounter);
#endif
								  uint32_t buf_alloc = 6 + buffer_length; //lwords
								  DWORD buf_alloc_bytes = (6 + buffer_length)*sizeof(DWORD); //bytes

								  sisdata_before = (DWORD*)M_MALLOC(buf_alloc_bytes);
								  sisdata_after = (DWORD*)M_MALLOC(buf_alloc_bytes);

								  memset(sisdata_after,0,buf_alloc_bytes);
								  //blt_data = M_MALLOC(4096*sizeof(uint32_t));
								  //calc_event_length =  6 + (gRawDataSampleLength/2) + event_length_lwords;
								  if( bank1_armed_flag[k_loop_sis_mod] == bank_flag ) { // Disarmedi
									  //								 gefvme_set_dma_channel(1);
									  //								  
									  // printf("sis3302_read_A32BLT_ADC_MEMORY\n");
									  //gettimeofday(&t1,NULL);
									  return_code = sis3302_read_A32BLT_ADC_MEMORY( SIS3302_BASE[k_loop_sis_mod],sischannelcounter,adc_mem_bank2_addr_offset,
											  sisdata_before,buffer_length,&dma_got_nof_lwords);
									  
									  
									  diag_sclr[440+g_sis_chan_counter] += buffer_length;
									  
									  // gettimeofday(&t2,NULL);
									  // dt1 = t2.tv_usec - t1.tv_usec;

									  // cm_msg(MINFO,"my_readout_thread", " Returned dma_got_nof_lwords : [%d] , channel %d , time taken [%ld]",dma_got_nof_lwords, sischannelcounter,dt1);

#ifdef DEBUG_SIS_3							
									  cm_msg(MINFO,"my_readout_thread", "SIS[%d], Returned dma_got_nof_lwords : [%d] , channel %d ",k_loop_sis_mod,dma_got_nof_lwords, sischannelcounter);
#endif						
									  //does not work
									  //int trigger_counter = sis3320_RegisterRead(k600vme,SIS3302_BASE,0x24);
									  //cm_msg(MINFO,"my_readout_thread","Event Counter: %d",trigger_counter);		
								  }else{

									  // cm_msg(MERROR,"my_readout_thread", " sis3302_read_adc_channel: return_code = 0x%08x at addr = 0x%08x ",
									  //  return_code,SIS3302_BASE);
									  diag_sclr[318+k_loop_sis_mod]++;	
								  }//if-else

								  //if( return_code < 0 ){
								  //}

								  sis_datacnt = 0;
								  if( (dma_got_nof_lwords > 0) ){
									  int sissort_status =  0; /*  to stop compiler warning */
									  sissort_status  = sortSIS3302Data( sisdata_after , sisdata_before , buf_alloc, &sis_datacnt, sischannelcounter );

									  /* check event size */
									  if ( sis_datacnt + sizeof(EVENT_HEADER) > (DWORD) max_event_size) {
										  cm_msg(MERROR, "my_readout_thread",
												  "Event size %ld larger than maximum size %d for sis adc",
												  (long) (sis_datacnt + sizeof(EVENT_HEADER)),
												  max_event_size);
										  assert(FALSE);
									  }
								  }//if-sort and size check

								  /* put tdc data in ring buffer. Each tdc has its own ringbuffer */
								  /* checks t//he buffer level for the specific tdc's ring buffer */
								  memcpy( siswp , sisdata_after, sis_datacnt*sizeof(DWORD) );
#if 0
								  DWORD *sswp = (DWORD*)siswp;
								  int ii = 0;
								  for(ii=0;ii<sis_datacnt;ii+=sizeof(DWORD)){
									  DWORD tmp = (*sisdata_after)++;
									  (*sswp) = tmp;
									  (*sswp)++;

								  }
#endif 
								  rb_increment_wp(ringbh_sis3302[k_loop_sis_mod][sischannelcounter], sis_datacnt*sizeof(DWORD) );
								  //cm_msg(MINFO,"myreadout_thread","incrementing wp for channel %d, data cnt %d , BH[%d] ",
								  // 		  sischannelcounter,sis_datacnt,ringbh_sis3302[sischannelcounter]);
								  // printf("clearing mem\n");
								  M_FREE(sisdata_after);
								  M_FREE(sisdata_before);




							  }else{ 
								  diag_sclr[300+g_sis_chan_counter]++;
								  //cm_msg(MINFO,"myreadout_thread","next sample address == 0x0: channel %d",sischannelcounter);
							  	  //   printf("sis3302 check end threshold 0x%x , module [%d], at addr 0x%x \n",_armed_,k_loop_sis_mod,SIS3302_BASE[k_loop_sis_mod]); 
					 
							  }//if-next_sample-check


							  g_sis_chan_counter++; //global channel counter index
						  }//FOR-sis-channels

						  //bank1_armed_flag = ArmSample(bank1_armed_flag);

					  }else{ 
						  
						  diag_sclr[400+k_loop_sis_mod]++; //if not armed, scroll modules(not channels )
#if 0 					  	 
						  fprintf(fp_thread,"sis3302 Not Armed << 0x%x >> module [%d], at addr 0x%x \n",_armed_,k_loop_sis_mod,SIS3302_BASE[k_loop_sis_mod]); 
						  fprintf(fp_thread,"Status End of Threshold Flag %d,\n \
								  			 Status ADC Sample Logic Busy (Armed %d\n \
								   			 Status ADC Sample Logic Bank 2 Armed %d\n \
								   			 Status ADC Sample Logic Bank 1 Armed %d\n ",(_armed_>>19)&0x1,(_armed_>>18)&0x1,(_armed_>>17)&0x1,(_armed_>>16)&0x1);
#endif 
					 
					  } //IF-Armed
					  //else{ss_sleep(10); }//IF-WaitDataReady-SIS

					  }//FOR-sis-Modules
#endif
				  //result = gefTimerGetRemaining (timer_handle, &v1_tdc);

				  //gef_time_sum1 += (1000000 - v1_tdc.tv_usec);
			  	  //gef_time_counter1++;

				  /* ======= QDC Data memory buffer transfer =================== */
				//  memcpy(p,pqdata,pqdata_size*sizeof(DWORD));
				  /* put event into ring buffer */
				//  rb_increment_wp(ringbh1,pqdata_size*sizeof(DWORD));
				  /* QDC Ringbuffer update */
				  memcpy(p,pqdata,pqdata_size*sizeof(DWORD));
				  /* put event into ring buffer */
				  rb_increment_wp(ringbh1,pqdata_size*sizeof(DWORD));
		
 				  /* :WORKAROUND:03/14/2013 08:53:39 AM:LCP:  */
				  //reset_vme_modules(0);
				  //reset_vme_counters();
				  //daq_reset();
				  //ss_sleep(10);
			 
			  }// if qdc source 

		   
		  }else{
		  }//if-poll_event returns true

		  my_readout_thread_active = 0;


	  } else{ // readout_enabled

	  //ss_sleep(10);
	  ss_sleep(10);
      }//if-else readout enabled

   }//while

   my_readout_thread_active = 0;
 
   M_FREE(qdata);
   M_FREE(pqdata);
   M_FREE(tdcdata_before);
   M_FREE(tdcdata_after);
   M_FREE(sdata);
   M_FREE(usdata);

   return 0;
}

INT Thread_SETUP()
{
	/* Start the read out thread here, 
	 * and readout = 0, to wait for user input via odb/mhttp etc.
	 */
	my_readout_enable(FALSE);
	READOUT_THREAD_ID = ss_thread_create((void *)my_readout_thread,NULL);
	//rb_set_nonblocking();
	//	if( pthread_create( &READOUT_THREAD_ID, NULL,(void *)READOUT_Thread,NULL) ) {
	//		printf(" error creating thread! \n ");
	//		return FE_ERR_HW;
	//	}
	
	return SUCCESS;
}


INT Ringbuffer_SETUP()
{
	int status;
	/*  QDC V792 */
	if(!ringbh1){
		status = rb_create( event_buffer_size, max_event_size, &ringbh1 );
		if( status != DB_SUCCESS || status == DB_NO_MEMORY ) {
			printf("[frontend init] could not create rb \n");
			frontend_exit();		
		}
		printf("\n [frontend init] created a ringbuffer with handle %d, \
				buffer size %d and max event size %d! \n ",ringbh1, \
				event_buffer_size, \
				max_event_size );
	}

	int current_ringb = 0;
#ifdef WITH_TDC_MODULE
	/*  TDC V1190A */
	int tdc_module = 0;
	for( tdc_module = 0; tdc_module < V1190Count; tdc_module++ ){
		status = rb_create(event_buffer_size, max_event_size, &current_ringb );
		if( status != DB_SUCCESS || status == DB_NO_MEMORY){
			printf("[frontend init] could not create rb \n");
			frontend_exit();		
		}

		ringbh_1190[tdc_module]  = current_ringb;
		printf("[frontend_init] v1190A ringbuffer setup: module number [%d], \
				base address [0x%08x] , ring buffer handle [%d], \
				event buffer size [%d] \n",tdc_module,V1190_BASE[tdc_module], \
				ringbh_1190[tdc_module], \
										   event_buffer_size );
	}
#endif
#ifdef WITH_V830_MODULE
	/*  Scalers V830 */
	int sclr_module = 0;
	current_ringb = 0;
	for( sclr_module = 0; sclr_module < 2; sclr_module++ ){
		status = rb_create(event_buffer_size, max_event_size, &current_ringb );
		if( status != DB_SUCCESS || status == DB_NO_MEMORY){
			printf("[frontend init] could not create rb \n");
			frontend_exit();		
		}

		ringbh_v830[sclr_module]  = current_ringb;
	}
#endif

#ifdef WITH_ADC_MODULE
	/* ADC's V785 */
	int adc_module = 0;
	current_ringb = 0;
	for( adc_module = 0; adc_module < V785Count; adc_module++ ){
		status = rb_create(event_buffer_size, max_event_size, &current_ringb );
		if( status != DB_SUCCESS|| status == DB_NO_MEMORY ){
			printf("[frontend init] could not create rb \n");
			frontend_exit();		
		}

		ringbh_v785[adc_module]  = current_ringb;
	}
#endif

#ifdef WITH_SIS_MODULE
	int adc_sis_chan = 0;
	current_ringb = 0;
	int k = 0;
	for( k=0; k < NUM_OF_SIS_MODS; k++ ){ 
		for(adc_sis_chan = 0; adc_sis_chan < SISCount; adc_sis_chan++) {	
			status = rb_create( event_buffer_size, max_event_size, &current_ringb );
			if( status != DB_SUCCESS || status == DB_NO_MEMORY ) {
				printf("[frontend init] could not create rb for sis3302\n");
				frontend_exit();		
			}
			ringbh_sis3302[k][adc_sis_chan]  = current_ringb;

		}//FOR
	}//for
#endif
	return SUCCESS;
}

/*-- Frontend Init -------------------------------------------------*/
void wirechamber_settings_callback( INT hDB, INT hseq, void *info )
{

}

void f1_callback(INT hDB, INT hkey , void *info)
{
	//ADC
	if( exp_edit.debug_adc_rb >= 0 )
		FRONTEND_DEBUG_MODE_C = exp_edit.debug_adc_rb;

	//TDC
	if( exp_edit.debug_tdc_rb >= 0 )
		FRONTEND_DEBUG_MODE_D = exp_edit.debug_tdc_rb;

	//QDC
	if( exp_edit.debug_qdc_rb >= 0 )
		FRONTEND_DEBUG_MODE_B = exp_edit.debug_qdc_rb;

	//VME Timers
	if( exp_edit.debug_vme_timers >= 0 )
		FRONTEND_DEBUG_MODE_TIMER = exp_edit.debug_vme_timers;




	if( exp_edit.frontend_mode >= 0 && exp_edit.frontend_mode < 8 ){
		//printf("------->>>>> FRONTEND MODE is %d \n",exp_edit.frontend_mode );
		//cm_msg(MINFO,"f1_callback","\t  FRONTEND MODE is %d \n",exp_edit.frontend_mode );
		FRONTEND_MODE = exp_edit.frontend_mode;

		if( FRONTEND_MODE == 0 ){
#ifdef WITH_TDC_MODULE
			V1190Count = NUM_OF_TDC_MODS;
#endif
#ifdef WITH_ADC_MODULE
			V785Count = NUM_OF_ADC_MODS;
#endif
#ifdef WITH_SIS_MODULE
			SISCount = NUM_ADC_SIS_CHANNELS;
			FRONTEND_NO_SIS = 0;
#endif
			FRONTEND_DEBUG_MODE = 0;
			cm_msg(MINFO,"f1_callback","\t  FRONTEND MODE is %d [default mode]\n",exp_edit.frontend_mode );
		}

		if( FRONTEND_MODE == 1 ){
#ifdef WITH_TDC_MODULE
			V1190Count = NUM_OF_TDC_MODS;
#endif
#ifdef WITH_ADC_MODULE
			//V785Count = NUM_OF_ADC_MODS;
			V785Count = 4;

			//V785_BASE[0]  = 0x1e0000;  /*slot 15*/ /*  V785 */
			//V785_BASE[1]  = 0x200000;  /*slot 16*/ /*  V785 */
			//V785_BASE[2]  = 0x220000;  /*slot 17*/ /*  V785 */
			V785_BASE[0]  = 0x240000;  /*slot 18*/ /*  V785 */
			V785_BASE[1]  = 0x260000;  /*slot 19*/ /*  V785 */
			V785_BASE[2]  = 0x280000;  /*slot 20*/ /*  V785 */
			V785_BASE[3]  = 0x2A0000;  /*slot 21*/ /*  V785 */

#endif
			//FRONTEND_DEBUG_MODE_E = 0;			//Software counting the ADC's
			cm_msg(MINFO,"f1_callback","\t  FRONTEND MODE is %d [default mode], ADC last 4 Modules only. Check that Veto is correctly set.\n",exp_edit.frontend_mode );
		}

		if( FRONTEND_MODE == 2 ){
#ifdef WITH_TDC_MODULE
			V1190Count = 0;
#endif
#ifdef WITH_ADC_MODULE
			V785Count = NUM_OF_ADC_MODS;
#endif
			//FRONTEND_DEBUG_MODE = 0;
			cm_msg(MINFO,"f1_callback","\t  FRONTEND MODE is %d : NO TDC READOUT. ONLY QDC + ADC\n",exp_edit.frontend_mode );
		}

		if( FRONTEND_MODE == 3 ){
			//FRONTEND_DEBUG_MODE = 0;
#ifdef WITH_TDC_MODULE
			V1190Count = NUM_OF_TDC_MODS;
#endif
#ifdef WITH_ADC_MODULE
			V785Count = 0;
#endif
			
			cm_msg(MINFO,"f1_callback","\t  FRONTEND MODE is %d : NO ADC READOUT. ONLY QDC + TDC \n",exp_edit.frontend_mode );
		}

		/* Default mode + Debug Mode
		 *
		 * This mode includes timing values, and scalar MEB counts, TDC MEB counts.
		 *
		 */
		if( FRONTEND_MODE == 4 ){
			/*  default mode. */
			V1190Count = NUM_OF_TDC_MODS;

			V785_BASE[0]  = 0x1e0000;  /*slot 15*/ /*  V785 */

			V785Count = 1;

			//FRONTEND_DEBUG_MODE = 1;
			//FRONTEND_DEBUG_MODE_TIMER = 1;

			cm_msg(MINFO,"f1_callback","\t  FRONTEND MODE is %d , Only ADC module in slot 15!!! Check Veto. \n",exp_edit.frontend_mode );
		}

	//	if( FRONTEND_MODE == 4 ){
	//		FRONTEND_DEBUG_MODE_B = 2;
	//		FRONTEND_DEBUG_MODE_A = 1;
	//		FRONTEND_DEBUG_MODE_C = 3;
	//		FRONTEND_DEBUG_MODE_D = 3;
	//		FRONTEND_DEBUG_MODE = 3;
	//		cm_msg(MINFO,"f1_callback","\t  FRONTEND MODE is %d [DEBUG_MODE_B: Timers + Event Warnings]\n",exp_edit.frontend_mode );
	//	}


		if( FRONTEND_MODE == 5 ){
			cm_msg(MINFO,"f1_callback","\t  FRONTEND MODE is %d [DEBUG MODE F] Default setup, with TDC internal error readout (expect 1 second delay if error detected) \n",exp_edit.frontend_mode );
#ifdef WITH_TDC_MODULE
			V1190Count = NUM_OF_TDC_MODS;
#endif
#ifdef WITH_ADC_MODULE
			V785Count = NUM_OF_ADC_MODS;
#endif
			FRONTEND_DEBUG_MODE = 0;
			FRONTEND_DEBUG_MODE_F = 1;
		}

		if( FRONTEND_MODE == 6 ){
			cm_msg(MINFO,"f1_callback","\t  FRONTEND MODE is %d Default setup, with no SIS ADC  \n",exp_edit.frontend_mode );
#ifdef WITH_TDC_MODULE
			V1190Count = NUM_OF_TDC_MODS;
#endif
#ifdef WITH_ADC_MODULE
			V785Count = NUM_OF_ADC_MODS;
#endif
#ifdef WITH_SIS_MODULE
			SISCount = 0;
			FRONTEND_NO_SIS = 1;
#endif		
		}

	}else{
		cm_msg(MERROR,"f1_callback","FRONTEND MODE ERROR...[ %d ]\n",exp_edit.frontend_mode );
		frontend_exit();		// STOP RUN... DO NOT EXIT FRONTEND!
	}


}


/*
 *  Trigger Settings Hot Link Update
 */
void f2_callback(INT hDB, INT hkey , void *info)
{


  cm_msg(MINFO,"f2_callback","\t Setting QDC and ADC thresholds. \n");

  int i = 0;
  //Setting QDC Thresholds
  for (i=0;i<32;i++) {
    QDC_Thresholds[i] = K600TriggerSettings.v792n_0.thresholds[i];
    //thold[i] = 0x;
  }

#ifdef WITH_ADC_MODULE  
 //Setting ADC Thresholds
  for (i=0;i<ADC_THRESHOLD_CHANNELS;i++) {
    ADC_Thresholds[i] = K600TriggerSettings.v785thres.thresholds[i];
    //thold[i] = 0x;
  }
#endif

#ifdef WITH_ADC_N_MODULE
 //Setting ADC Thresholds
  for (i=0;i<ADC_THRESHOLD_CHANNELS_N;i++) {
    ADC_Thresholds_N[i] = K600TriggerSettings.v785thres.thresholdsn[i];
    //thold[i] = 0x;
  }
#endif

  //Set Number of TDC's
  //

//  V1190Count = K600TriggerSettings.numtdc;
//  cm_msg(MINFO,"f2_callback","\t  Number of TDC's in readout [%d] \n",V1190Count );

}
	
INT frontend_init()
{
	int status=0;
	//int size=0;
	//char set_str[80];
	int i = 0;

	//EXP_EDIT_STR(exp_edit_str);
	//char str[256];
	char time_buffer[80];
	time_t t = time(NULL);
	//struct tm tm  =  *localtime(&t);
	struct tm *tm  =  localtime(&t);
	
	strftime(time_buffer,80,"%F:%H:%M:%S",tm);
	
	//sprintf(str,"QDC_READOUT_%s_.log",time_buffer);


	//fp_thread = fopen(str,"a+");


	cm_get_experiment_database(&hDB,NULL);

	status = db_find_key(hDB,0,"/Experiment/edit on start/",&hkey);
	if( status != DB_SUCCESS ){
		cm_msg(MERROR,"frontend_init",
				"Cannot find Experiment Edit On Start in ODB");
		return -1;
	}

	int dbresult = db_open_record(hDB,hkey,
				&exp_edit,
				sizeof(exp_edit),
				MODE_READ,
				f1_callback,NULL); 
	if( dbresult != DB_SUCCESS )
	{
		cm_msg(MERROR,"frontend_init",
				"Cannot open Experiment Edit On Start in ODB, error[%d]",dbresult);
		return -1;
	}

	
	db_find_key(hDB,0,"/Equipment/WireChamber/Settings",&hkey);


	if( db_open_record(hDB,hkey,
				&K600TriggerSettings,
				sizeof(K600TriggerSettings),
				MODE_READ,
				f2_callback,NULL) != DB_SUCCESS )
	{
		cm_msg(MERROR,"frontend_init",
				"Cannot open WireChamber Settings");
		return -1;
	}else{

#ifdef QDC_32		
		//Setting QDC Thresholds
		for (i=0;i<QDC_THRESHOLD_CHANNELS;i++) {
			QDC_Thresholds[i] = K600TriggerSettings.v792n_0.thresholds[i];
			//thold[i] = 0x;
		}
#endif

#ifdef QDC_16		
		//Setting QDC Thresholds
		for (i=0;i<QDC16_THRESHOLD_CHANNELS;i++) {
			QDC_Thresholds[i] = K600TriggerSettings.v792n_0.thresholds[i];
			//thold[i] = 0x;
		}
#endif
	
#ifdef WITH_ADC_MODULE	
		//Setting ADC Thresholds
		for (i=0;i<ADC_THRESHOLD_CHANNELS;i++) {
			ADC_Thresholds[i] = K600TriggerSettings.v785thres.thresholds[i];
			//thold[i] = 0x;
		}
#endif

#ifdef WITH_ADC_N_MODULE
		//Setting ADC Thresholds
		for (i=0;i<ADC_THRESHOLD_CHANNELS_N;i++) {
			ADC_Thresholds_N[i] = K600TriggerSettings.v785thres.thresholdsn[i];
			//thold[i] = 0x;
		}
#endif
	}//Setting Thresholds

	if (cm_register_transition(TR_START,frontend_start, 600) != CM_SUCCESS ||
	    cm_register_transition(TR_STOP,ringbuffer_flush, 650) != CM_SUCCESS ||
		cm_register_transition(TR_STOP,frontend_stop, 400) != CM_SUCCESS ){
			ss_sleep(4000);
			printf("Couldn't register transitions!!!!");
			frontend_exit();
   	}

	cm_msg(MINFO,"k600fevme","Frontend Hardware initialisation of VME Window");
	status = mvme_open(&k600vme,0);
	if (status!=1) { 
		cm_msg(MERROR,"Frontend Init","Could Not open mvme device!");
		cm_disconnect_experiment();
		exit(1);
	}

#if 0 
	open_timers();
#endif 

	cm_msg(MINFO,"k600fevme","Initialize VME modules");
	VMEBASE_SETUP();
	init_vme_modules(1);

	/* CREATE READOUT AND SYNC THREAD AND RINGBUFFERS */
	cm_msg(MINFO,"k600fevme","Ringbuffer setup");
	status = Ringbuffer_SETUP();
	
	cm_msg(MINFO,"k600fevme","Thread started");
	status = Thread_SETUP();
	scaler_readout_thread_active = 0;

	if( status == 1 )
		cm_msg(MINFO,"k600fevme","Frontend initialisation is complete, you may start....");

	if(status!=1)
		return FE_ERR_HW;
	else 
		return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
   
   cm_msg(MINFO,"k600fevme"," Doing Frontend_exit");
#if 0 
   if(FRONTEND_DEBUG_MODE_TIMER){
   close_timers();
  }   
#endif 

  /* stop readout thread */
   my_readout_enable(FALSE);
   my_stop_all_threads = 1;
   scaler_readout_thread_active = 0;

   // kill readout thread
   rb_set_nonblocking();
   while (my_readout_thread_active)
      ss_sleep(100);
   
   
   //fpga_setG0(0);	

   // delete qdc ringbuffer
   rb_delete(ringbh1);

   // delete tdc ringbuffers
   int i = 0;
#ifdef WITH_TDC_MODULE   
   for(i = 0; i < V1190Count; i++ )
     rb_delete(ringbh_1190[i]);
#endif  
#ifdef WITH_V830_MODULE   
   // delete sclr/v830 ringbuffers
   for(i=0;i<NUM_OF_SCR_MODS;i++)
     rb_delete(ringbh_v830[i]);
#endif
#ifdef WITH_ADC_MODULE
  // delete adc/v785 ringbuffers
   for(i=0;i<V785Count;i++)
     rb_delete(ringbh_v785[i]);
#endif

#ifdef WITH_SIS_MODULE
   int k = 0;
   for(k=0;k<NUM_OF_SIS_MODS;k++){ 
	   for(i=0;i<SISCount;i++){
		   rb_delete(ringbh_sis3302[k][i]);
	   }
   }
     //ss_mutex_delete(&f_mutex);
#endif

#ifdef FE_WITH_SCALAR
//	int jj = 0;
//	for(jj=0;jj<NUM_OF_SCR_MODS;jj++){
//		M_FREE(SCLR_CHANNELS[jj]);
//	}

#endif

     //fclose(fp_thread); 
     return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{
   /* put here clear scalers etc. */

	//int i = 0;
	//int csr;

	memset(prevscalercount,0,32*sizeof(DWORD));
	memset(scaler_prev_buf,0,NUM_OF_SCR_MODS*sizeof(int));
	memset(global_sclr_prev_trigcnt,0,sizeof(int)*NUM_OF_SCR_MODS);	
	
	reset_sclr_bufferlevelcounter();
	reset_sclr_trig_counter();
	reset_bufferlevelcounter();
	reset_adc_bufferlevelcounter();
	reset_sis_bufferlevelcounter();	
	reset_sis_Nw_counter();	
	reset_qdc_buf_counters();
	reset_wordcounter();
	reset_tdcaveragecounter();
    	reset_qdc_ave_event();	
	reset_tdc_skip_stats();
	reset_tdc_ea_stats();	

#if 0 
	if(FRONTEND_DEBUG_MODE_TIMER){
		gef_tdc_timer_reset();
	}
#endif 

		//ArmSample
	//my_readout_enable(FALSE);
	//reset_vme_modules(0);	
	//my_readout_enable(TRUE);
    //scaler_readout_thread_active = 1;
	return SUCCESS;
}


/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
   scaler_readout_thread_active = 0;
   my_readout_enable(FALSE);
   //reset_vme_modules(0);	
   FRONTEND_MODE = 0;
   FRONTEND_DEBUG_MODE_E = 0;
   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
   my_readout_enable(FALSE);
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
    my_readout_enable(TRUE);
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
   /* if frontend_call_loop is true, this routine gets called when
      the frontend is idle or once between every event */
   return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/
void SkipData(char *text){
  int status=rb_increment_rp(ringbh1, 1*sizeof(DWORD) );
  if(status != DB_SUCCESS){
   printf("[SkipDate2][ERROR] failed to advance read pointer on not valid datum with return %d\n",status);
   }
  if( FRONTEND_DEBUG_MODE_B == 2 ){
	  cm_msg(MINFO,"k600frontend"," qdc skip data: reason %s ",text );
  }
}

void SkipDataTDC(int rb_handle){
  int status=rb_increment_rp(rb_handle, 1*sizeof(DWORD) );
  if(status != DB_SUCCESS){
   printf("[SkipDateTDC][ERROR] failed to advance read pointer on not valid datum with return %d\n",status);
   }
}

int findSISADCEvent( DWORD *ptr ,int Nw ,int sischan) 
{
	int cntnumber  = 0;
 	DWORD oldval = 0x0;

//	DWORD headword = *ptr; // save proposed header word
//	cm_msg(MINFO,"findSISADCEvent","[%d] ----Header Word: 0x%x",my_global_test_counter,headword);
	do {
		oldval=*ptr++;//make local copy
		if(isSIS3302Header(&oldval,sischan) 
	    	    && (cntnumber > 1) ){ //skip first header word. check for header words inside data
#ifdef DEBUG_SIS_2
		      cm_msg(MINFO,"findSISADCEvent","Header word in between another header and trailer word: 0x%x",oldval);
#endif	      
		  break;
		}
		cntnumber += 1;
	}while(  !isSIS3302Trailer( &oldval ) && (cntnumber < Nw ));
		       //	&&  (cntnumber < event_length_lwords)  ); // copy until the trailer
#ifdef DEBUG_SIS_2
	cm_msg(MINFO,"findSISADCEvent","event_length_lwords [%d]",event_length_lwords);
	cm_msg(MINFO,"findSISADCEvent","cntnumber  [%d]",cntnumber);
#endif
	if( isSIS3302Trailer( &oldval ) && cntnumber == event_length_lwords){
//		cm_msg(MINFO,"findSISADCEvent","[%d] ----TRailer Word: 0x%x",my_global_test_counter,oldval);
		return 1;
	}

//	cm_msg(MINFO,"findSISADCEvent","[%d] -----Did Not find Trailler Word: 0x%x, cntnumber %d ",my_global_test_counter,oldval,cntnumber);	
	return -1;
}


int findQDCEventNumber ( DWORD *ptr ) 
{
	int cntnumber  = 0;
	int eventnumber = -1;
 	DWORD oldval = 0x0;

	DWORD headword = *ptr; // create local copy of header
	int convchannels = getConvChannels(&headword);  // get number of converted channels
	int totalchannels = convchannels + 2; // add the header and trailer words
	do {
		oldval=*ptr++;//make local copy
		if(isHeader(&oldval) && cntnumber > 1 ){ //skip first header word. check for header words inside data
		  break;
		}
		cntnumber += 1;
	}while(  !isTrailer( &oldval ) && cntnumber < totalchannels  ); // copy until the trailer

	if( isTrailer( &oldval ) ){
	 	eventnumber = (oldval&0xFFFFFF);
	}else{
		return -1;
	}
	return eventnumber;
}

#ifdef WITH_ADC_MODULE
int findADCEventNumber ( DWORD *ptr ) 
{
	int cntnumber  = 0;
	int eventnumber = -1;
 	DWORD oldval = 0x0;

	DWORD headword = *ptr; // create local copy of header
	int convchannels = getConvChannels(&headword);  // get number of converted channels
	int totalchannels = convchannels + 2; // add the header and trailer words
	do {
		oldval=*ptr++;//make local copy
		if(isHeader(&oldval) && cntnumber > 1 ){ //skip first header word. check for header words inside data
		  break;
		}
		cntnumber += 1;
	}while(  !isTrailer( &oldval ) && cntnumber < totalchannels  ); // copy until the trailer

	if( isTrailer( &oldval ) ){
	 	eventnumber = (oldval&0xFFFFFF);
	}else{
		return -1;
	}
	return eventnumber;
}
#endif

#ifdef WITH_TDC_MODULE
int findTDCEventNumber( DWORD *ptr, int Nw,int tdcmod )
{
	int windex = 0;
	int wcnt = 0;
	DWORD vword = 0x0;
	DWORD *lptr = NULL;
	int tdcevntnum = -1;
	
	/* First word will be Global TDC Header */
	lptr = ptr; //create local copy
	vword = *lptr;
	tdcevntnum = getEventCounter(&vword);   // Get EventCount 
	windex = 0;
	wcnt = 0;  // Global Header
	do{
		vword = *lptr++;
		wcnt++;
		windex++;
		int wc = getWordCountGLOBAL(&vword);
		if( wcnt == wc ){
			return tdcevntnum;
		}
			
		
		
	}while( windex < Nw );
	return -1; 
}
#endif

DWORD *prev_vword;

INT isDataready(int count)
{

	/*  QDC */
	int blevel = 0;
	void *p;
	int status = 0;
	DWORD *vword;
	DWORD qevent_num = 0;

	/*  TDC */
	int tdc_buffer_level = 0;
	void *tdc_p;
	DWORD *tdc_vword;
	int tdc_eventcount = -1;
	int tdcm = 0;
	//int i = 0;

	int poll_tdc = -1;
	int tdc_poll_correct = 7;

	/* ADC */
#ifdef WITH_ADC_MODULE
	int poll_adc = -1;
	int adc_poll_correct = 2;
	int adc_buffer_level = 0;
	void *adc_p;
	DWORD *adc_vword;
	//int adc_eventcount = -1;
	int adc_mod = 0;
	int adc_eventnumber = 0;
#endif

#ifdef WITH_SIS_MODULE
	int poll_sis_adc = -1;
	int sis_poll_correct = 2;
	int sis_buffer_level = 0;
	void *sis_p;
	DWORD *sis_vword;
	//int adc_eventcount = -1;
	int sis_chan = 0;
	int sis_mod = 0;
	int sis_findstatus = 0;
#endif


	/* get qdc ring buffer level */
	rb_get_buffer_level(ringbh1,&blevel);
	if( blevel != oldblevel && blevel > 0 ){
		oldblevel = blevel;

		/*  get qdc event from ringbuffer */
		status = rb_get_rp(ringbh1, &p, 0 );
		if( status == DB_TIMEOUT ){
			return 0;
		}

		vword = (DWORD*)p;
		
		if( isHeader(vword) && notvalidatum(vword) == 0 ){
			qevent_num = findQDCEventNumber(vword); // find event number corresponce to this header

			//if( qevent_num >= qevent_num_global && qevent_num != -1){
			if( qevent_num != -1){
				prev_qq = qevent_num - qevent_num_global; //must be 1
				qevent_num_global = qevent_num;

				/*  DEBUG */
				if( FRONTEND_DEBUG_MODE_B ){
					if( prev_qq > 1 ){
						int error_blevel = 0;
						rb_get_buffer_level(ringbh1,&error_blevel);
						cm_msg(MINFO,"k600fevme", "QDC Event Jump, prev %d, current %d, rb level (bytes) %d ",prev_qdc_event_num,
								qevent_num,
								error_blevel);
					}

				}//FRONTEND_DEBUG_MODE_B

				if( qevent_num != prev_qdc_event_num )
					prev_qdc_event_num = qevent_num;	 	
#ifdef WITH_TDC_MODULE				
				poll_tdc = 1;
#endif

#ifdef WITH_ADC_MODULE				
				poll_adc = 1;
#endif
#ifdef WITH_SIS_MODULE
				poll_sis_adc = 1;
#endif
				qdc_poll_successfull = 1;
				diag_sclr[432]++;
				diag_sclr[101]++; // QDC successfull poll
			}else{
				qdc_poll_successfull = 0;
				diag_sclr[102]++; // QDC bad poll
			}//IF_ELSE
		}else if( isTrailer(vword) && notvalidatum(vword) == 0 ){
			SkipData("[isDataReady]isTrailer");
		}else if( notvalidatum(vword) ){
			SkipData("[isDataReady]isInvalid/EOB");
		}else if( isDataword(vword) && (notvalidatum(vword) == 0) ){
			SkipData("[isDataReady] Datum Word ");
		}else{
			SkipData("[isDataReady]Default/Unknowndata");
		}//if-else isHeader/isTrailer/isInvalid/Default


		if( skip_current_qdc_event == 1 ){
			SkipData("[isDataReady]Skip this event");
			diag_sclr[4]++;
			skip_current_qdc_event = 0;
		}

		
	}//if-getbufferlevel

#ifdef WITH_ADC_MODULE


		/*  POLL ADC's */
		if( poll_adc == 1 ){
			adc_poll_correct = V785Count;
			for( adc_mod = 0; adc_mod < V785Count; adc_mod++ ){
				adc_buffer_level = 0;	
				rb_get_buffer_level(ringbh_v785[adc_mod],&adc_buffer_level);
				if(FRONTEND_DEBUG_MODE_C==3){
					cm_msg(MINFO,"isdataready"," ADC %d, buffer level(bytes) %d \n",adc_mod,adc_buffer_level );
				}//FRONTEND_DEBUG_MODE_C
				//if( adc_buffer_level != adc_oldblevel[adc_mod] && adc_buffer_level > 0 ){
				if( adc_buffer_level > 0 && adcmatchqdc[adc_mod] == -1  ){
			
					adc_oldblevel[adc_mod] = adc_buffer_level;
					
					int current_adc_blevel = 0; 
					rb_get_buffer_level(ringbh_v785[adc_mod],&current_adc_blevel);
					
					int adc_Nw = abs(current_adc_blevel)/sizeof(DWORD);
					
					int adc_counter = 0;
					do{
						status = rb_get_rp(ringbh_v785[adc_mod], &adc_p, 0 );
						if( status == DB_TIMEOUT ){
							//return 0;
							break;
						}

						adc_vword = (DWORD*)adc_p;
						if( isHeader(adc_vword) && notvalidatum(adc_vword) == 0 ){
							adc_eventnumber = findADCEventNumber(adc_vword); // find event number corresponce to this header
							

							if( adc_eventnumber != -1 ){
							if( adc_eventnumber == qevent_num && qevent_num >=  0 ){
								/* DEBUG */
								if(FRONTEND_DEBUG_MODE_C==3) {
								cm_msg(MINFO,"k600fevme","Event MATCHED: Module[%d] ADC %d, QDC %d, prev ADC[%d] ",(adc_mod+1),
										adc_eventnumber,
										qevent_num,
										prevadcmatchqdc[adc_mod]);
								}//FRONTEND_DEBUG_MODE_C
								if( adc_eventnumber != prevadcmatchqdc[adc_mod] && adc_eventnumber != -1 )
										prevadcmatchqdc[adc_mod] = adc_eventnumber;

								diag_sclr[431]++;
								adcmatchqdc[adc_mod] = 1;
								adc_poll_correct--;
								break;
							}else if( adc_eventnumber < qevent_num && qevent_num >=  0 ){
								/* DEBUG */
								if(FRONTEND_DEBUG_MODE_C){
								cm_msg(MINFO,"k600fevme","RB-E-Skip, ADC-Mod[%d]: ADC-E %d < QDC-E %d, Prev-ADC-E[%d] ",(adc_mod+1),
										adc_eventnumber,
										qevent_num,
										prevadcmatchqdc[adc_mod]);
								}//FRONTEND_DEBUG_MODE_C
								if( adc_eventnumber != prevadcmatchqdc[adc_mod] && adc_eventnumber != -1 )
										prevadcmatchqdc[adc_mod] = adc_eventnumber;


							}else if( adc_eventnumber > qevent_num && qevent_num >=  0 ) {
								/* DEBUG */
								if(FRONTEND_DEBUG_MODE_C){
								if( adc_gimp_counter++ < 5 ){
									cm_msg(MINFO,"k600fevme","RB-E-Skip, ADC-Mod[%d]: ADC-E %d > QDC-E %d, Prev-ADC-E[%d] ",(adc_mod+1),
											adc_eventnumber,
											qevent_num,
											prevadcmatchqdc[adc_mod]);
								}	
								}//FRONTEND_DEBUG_MODE_C	
								if( adc_eventnumber != prevadcmatchqdc[adc_mod] && adc_eventnumber != -1 )
										prevadcmatchqdc[adc_mod] = adc_eventnumber;

								


								/*  increment the QDC */
								skip_current_qdc_event = 1;
								break;
	
							}//if-else - match adc to qdc
						}//IF- adc_eventnumber != -1

						}//Header


						status=rb_increment_rp(ringbh_v785[adc_mod], 1*sizeof(DWORD) );
						adc_counter++;
					}while(adc_counter < adc_Nw);


				}else{
						if( adcmatchqdc[adc_mod] == 1 )
							adc_poll_correct--;

						if(FRONTEND_DEBUG_MODE_C==2){
							if( adc_buffer_level <= 0 )
								cm_msg(MINFO,"k600fevme","ADC RB Buffer Level <= 0: ADC_MOD[%d], Level[%d] : ",(adc_mod+1),adc_buffer_level );
						}//FRONTEND_DEBUG_MODE_C

						/*  Check Use of this? Not needed? */
						//ss_sleep(10);
					
				}//if else -IF-ADC-BUFFER_LEVEL
			}//FOR-ADC-LOOP

			poll_adc = 0;
		}//if-ADC_poll

#endif

#ifdef WITH_TDC_MODULE
	if( poll_tdc == 1 ) {
			tdc_poll_correct = V1190Count;
			/*  scroll through all the tdc modules.  */
			for( tdcm = 0; tdcm < V1190Count; tdcm++ ){
				/*  get the buffer level of a tdc module. */
				rb_get_buffer_level(ringbh_1190[tdcm],&tdc_buffer_level);

				if(my_stop_all_threads){
						  printf("breaking \n");
						  break;
				 }


				//int the_diff = abs( tdc_buffer_level - prevtdcblevel[tdcm] );

				if(FRONTEND_DEBUG_MODE_D==3){
					cm_msg(MINFO,"isDataready"," TDC [%d] buffer level bytes [%d] , matched? [%d] ",(tdcm+1),tdc_buffer_level,tdcmatchqdc[tdcm] );
				}//FRONTEND_DEBUG_MODE_DN
				
				//if( tdc_buffer_level > 0 && tdc_buffer_level != prevtdcblevel[tdcm] && tdcmatchqdc[tdcm] == -1  ){
				if( tdc_buffer_level > 0 && tdcmatchqdc[tdcm] == -1  ){
					//if( tdc_buffer_level > 0 && tdcmatchqdc[tdcm] == -1  ){
					//if( tdc_buffer_level > 0 && the_diff > 0 && tdcmatchqdc[tdcm] == -1  ){
					prevtdcblevel[tdcm] = tdc_buffer_level;


					/*  non-blocking rb_get_rp */
					//    	 status = rb_get_rp(ringbh_1190[tdcm], &tdc_p,0);

					//	 if( status == DB_TIMEOUT ){
					//		 continue; //skip this buffer
					//	 }

					int current_tdc_blevel = 0;  /* :WORKAROUND:03/08/2013 06:36:12 AM:LCP:  */
					
					rb_get_buffer_level(ringbh_1190[tdcm],&current_tdc_blevel);

					int Nw = abs(current_tdc_blevel)/sizeof(int);  /* :WORKAROUND:03/08/2013 06:36:21 AM:LCP:  */

					//int Nw = get_tdc_buflevel_average(tdcm);
					//int Nw = the_diff/sizeof(DWORD);
					//myglobalcounter += Nw;

					//	if( Nw > max_event_size/sizeof(DWORD) ){
					//    		Nw = max_event_size/sizeof(DWORD);
					//	}	

					//tdc_vword = (DWORD*)tdc_p;	//local copy
					int mycounter = 0;
					do{
						/*  non-blocking rb_get_rp */
						status = rb_get_rp(ringbh_1190[tdcm], &tdc_p,0); //has size max_event_size

						if( status == DB_TIMEOUT ){
							// continue; //skip this buffer
							break;
						}
						tdc_vword = (DWORD*)tdc_p;	//local copy
						prev_vword = tdc_vword;

						if( isGlobalHeader(tdc_vword) ){

							tdc_eventcount = findTDCEventNumber(tdc_vword,Nw,tdcm); 
							

							if( tdc_eventcount != -1 ){
							if( tdc_eventcount == qevent_num && qevent_num >=  0 ){ //NB needs correction!
								/* DEBUG */
								if(FRONTEND_DEBUG_MODE_D==3){
									cm_msg(MINFO,"k600fevme","Event MATCHED: Module[%d] TDC %d, QDC %d, prev TDC[%d] ",(tdcm+1),
											tdc_eventcount,
											qevent_num,
											prevtdcmatchqdc[tdcm]);	
								}//FRONTEND_DEBUG_MODE_D
								if( tdc_eventcount != prevtdcmatchqdc[tdcm] && tdc_eventcount != -1 )
										prevtdcmatchqdc[tdcm] = tdc_eventcount;

								tdcmatchqdc[tdcm] = 1;
								tdc_poll_correct--;
								tdc_poll_gimp_counter = 0;
								break;
							}else if( tdc_eventcount < qevent_num && qevent_num >=  0 ){

//								/* DEBUG */
								if(FRONTEND_DEBUG_MODE_D==3){								
								cm_msg(MINFO,"k600fevme","RB-E-Skip:TDC Mod.[%d], TDC-E[%d] < QDC-E[%d],prev TDC-E[%d] ",(tdcm+1),
										tdc_eventcount,
										qevent_num,
										prevtdcmatchqdc[tdcm]);
								}//FRONTEND_DEBUG_MODE_D

								if( tdc_eventcount != prevtdcmatchqdc[tdcm] && tdc_eventcount != -1 )
										prevtdcmatchqdc[tdcm] = tdc_eventcount;
								tdc_poll_gimp_counter = 0;

								tdc_skipped_events[tdcm]++;

							}else if( tdc_eventcount > qevent_num && qevent_num >=  0 ) {

								tdc_ea_events[tdcm]++;
//								/* DEBUG */
								if(FRONTEND_DEBUG_MODE_D==3){
									if( gimp_counter++ < 5 ){
										cm_msg(MERROR,"k600fevme","RB-E-Skip: TDC Mod[%d], TDC event[%d] > QDC event[%d], prev TDC[%d] ",(tdcm+1),
												tdc_eventcount,
												qevent_num,
												prevtdcmatchqdc[tdcm]);
									}	
								}//FRONTEND_DEBUG_MODE_D
								if( tdc_eventcount != prevtdcmatchqdc[tdcm] && tdc_eventcount != -1 )
										prevtdcmatchqdc[tdcm] = tdc_eventcount;

								
								diag_sclr[0] = tdc_eventcount;
								diag_sclr[1] = qevent_num;
								diag_sclr[2] = prevtdcmatchqdc[tdcm];
								diag_sclr[3] = tdcm;
								diag_sclr[5] = mycounter;
							
								tdc_poll_gimp_counter = 0;

								/*  increment the QDC */
								skip_current_qdc_event = 1;
								break;
							}// if-else - match tdc to qdc

							}//if-tdceventnum != -1

							}//else{ // IF -else isglobalheader

							status=rb_increment_rp(ringbh_1190[tdcm], 1*sizeof(DWORD) );
												//}//if-else

							//*tdc_vword++; //[next word in rb] scroll ringbuffer
							mycounter++;  //[cannot exceed current rb size] increase word counter
						}while( mycounter < Nw ); // while loop

						//}while( !isGlobalTrailer(&vword) ); // while loop

						//			 status=rb_increment_rp(ringbh_1190[tdcm], mycounter*sizeof(DWORD) );

					}// IF-tdc_buffer_level
					else{
						if( tdcmatchqdc[tdcm] == 1 )
							tdc_poll_correct--;

						if( tdc_buffer_level <= 0 ){

							if(FRONTEND_DEBUG_MODE_D==3){
								cm_msg(MINFO,"k600fevme","TDC RB Buffer Level <= 0: TDC_MOD[%d], Level [%d] ",(tdcm+1),tdc_buffer_level );
							}
							/*  Check Use if this? Not needed? */
							ss_sleep(10);
						}
					}// if else



				}//FOR-tdcm

				poll_tdc=0;
			}//If- Poll_tdc
#endif


#ifdef WITH_SIS_MODULE
			if(poll_sis_adc == 1 && (FRONTEND_NO_SIS == 0)) {
				/*  POLL SIS ADC Channels */
				sis_poll_correct = NUM_OF_SIS_MODS*SISCount;
				int gdax = 0; // global array diag_sclr index: mod + channels 
				for(sis_mod=0;sis_mod < NUM_OF_SIS_MODS;sis_mod++){
					for( sis_chan = 0; sis_chan < SISCount; sis_chan++ ){
						sis_buffer_level = 0;	
						rb_get_buffer_level(ringbh_sis3302[sis_mod][sis_chan],&sis_buffer_level);
#ifdef DEBUG_SIS_4
						cm_msg(MINFO,"isDataready","Getting SIS[%d] buffer level: isDataReady blevel: %d , channel %d , BH [%d]",
								sis_mod,sis_buffer_level,sis_chan,ringbh_sis3302[sis_mod][sis_chan]);
#endif
						//If there is data, and buffer level != last read buffer level
						if( sis_buffer_level > 0 ){
							//		if( adc_buffer_level > 0 && adc_buffer_level != adc_oldblevel[adc_mod]){
							//cm_msg(MINFO,"isdataready"," ADC %d, buffer level(bytes) %d \n",sis_chan,adc_buffer_level );

							sis_oldblevel[sis_mod][sis_chan] = sis_buffer_level;
							int current_sis_blevel = 0; 
							rb_get_buffer_level(ringbh_sis3302[sis_mod][sis_chan],&current_sis_blevel);

							int sis_Nw =0;
							if( sizeof(DWORD) != 0 ){
								sis_Nw = abs(current_sis_blevel)/sizeof(DWORD);
							}
							
							// Sums the number of words per second per channel
							//diag_sclr[501+gdax] += sis_Nw;
							sis_Nw_update( sis_Nw, sis_mod ,sis_chan);

							int sis_word_counter = 0;
							do{
								//printf(" getting ring buffer rp \n");
								status = rb_get_rp(ringbh_sis3302[sis_mod][sis_chan], &sis_p, 0 );
								if( status == DB_TIMEOUT ){
									cm_msg(MINFO,"isDataready","ringbuffer timeout for buffer [%d] , channel %d ",ringbh_sis3302[sis_mod][sis_chan],sis_chan);
									//printf("isDataReady , DB_TIMEOUT\n");
									break;
								}

								sis_vword = (DWORD*)sis_p;
								sis_findstatus = 0;
								if( isSIS3302Header(sis_vword,sis_chan)  ){
									my_global_test_counter++;
									sis_findstatus = findSISADCEvent(sis_vword,sis_Nw,sis_chan); // find event number corresponce to this header
									//sis_findstatus = findSISADCEvent(sis_vword,event_length_lwords+1,sis_chan); // find event number corresponce to this header
#ifdef DEBUG_SIS_4
								 cm_msg(MINFO,"isdataready","Header 0x%x : SIS[%d], Chan[%d], Nw[%d] ",(*sis_vword),sis_mod,sis_chan,sis_Nw);
#endif
									if(sis_findstatus == 1) // found valid event
									{
										//sums the number of events (1) per channel per second
										diag_sclr[580+gdax]++;
										//cm_msg(MINFO,"isdataready","Event Found....");
										//		adcmatchqdc[adc_mod] = 1;
										sis_poll_correct--;
										break;
									}else{
										//diag_sclr[401]++;
									}

								}//Heade
								//else if(isSIS3302ErrorWord(sis_vword)){  }

								status=rb_increment_rp(ringbh_sis3302[sis_mod][sis_chan], 1*sizeof(DWORD) );
								sis_word_counter++;
							}while(sis_word_counter < sis_Nw);


						}else{
							diag_sclr[520+gdax]++;
							//	ss_sleep(10);
							//	cm_yield(10);
						}//if else -IF-ADC-BUFFER_LEVEL



						gdax = gdax + 1;
						}//FOR-ADC-Channel-LOOP
					}//FOR-ADC-Module_loop

					poll_sis_adc = 0;
			}//IF--poll_sis_adc
#endif

#if  defined(WITH_SIS_MODULE) && defined(WITH_ADC_MODULE) && defined(WITH_TDC_MODULE)

				if( FRONTEND_NO_SIS == 0 ) {
					if( sis_poll_correct == 0 && (qdc_poll_successfull  == 1 && adc_poll_correct == 0 && tdc_poll_correct == 0 ) ){


						int jebus = 0;
						for( jebus = 0; jebus < V1190Count; jebus++)
							tdcmatchqdc[jebus] = -1;

						int jebus2 = 0;
						for( jebus2 = 0; jebus2 < V785Count; jebus2++ )
							adcmatchqdc[jebus2] = -1;

						qdc_poll_successfull = 0;
						//poll_tdc = -1;
						//poll_adc = -1;
						return 1;
					}// successfull event constructed qdc+tdc

				}else {
					if(  (qdc_poll_successfull  == 1 && adc_poll_correct == 0 && tdc_poll_correct == 0 ) ){


						int jebus = 0;
						for( jebus = 0; jebus < V1190Count; jebus++)
							tdcmatchqdc[jebus] = -1;

						int jebus2 = 0;
						for( jebus2 = 0; jebus2 < V785Count; jebus2++ )
							adcmatchqdc[jebus2] = -1;

						qdc_poll_successfull = 0;
						//poll_tdc = -1;
						//poll_adc = -1;
						return 1;
					}// successfull event constructed qdc+tdc

				}//if-else
#elif  defined(WITH_SIS_MODULE) && defined(WITH_ADC_MODULE) && !defined(WITH_TDC_MODULE)
 		if( sis_poll_correct == 0 || (qdc_poll_successfull  == 1 && adc_poll_correct == 0) ){


						int jebus = 0;
						for( jebus = 0; jebus < V1190Count; jebus++)
							tdcmatchqdc[jebus] = -1;

						int jebus2 = 0;
						for( jebus2 = 0; jebus2 < V785Count; jebus2++ )
							adcmatchqdc[jebus2] = -1;

						qdc_poll_successfull = 0;
						//poll_tdc = -1;
						//poll_adc = -1;
						return 1;
					}// successfull event constructed qdc+tdc
#endif//if-sis


/* ADC MODULE + TDC MODULE + QDC MODULE */
#if defined(WITH_ADC_MODULE) && defined(WITH_TDC_MODULE) && !defined(WITH_SIS_MODULE)		
				if( tdc_poll_correct == 0 && adc_poll_correct == 0 && qdc_poll_successfull  == 1){

					int jebus = 0;
					for( jebus = 0; jebus < V1190Count; jebus++)
						tdcmatchqdc[jebus] = -1;

					int jebus2 = 0;
					for( jebus2 = 0; jebus2 < V785Count; jebus2++ )
						adcmatchqdc[jebus2] = -1;

					qdc_poll_successfull = 0;
					//poll_tdc = -1;
					//poll_adc = -1;
					return 1;
				}// successfull event constructed qdc+tdc

#endif//if-adc-qdc-only

/* QDC MODULE + SIS Module*/
#if !defined(WITH_ADC_MODULE) && !defined(WITH_TDC_MODULE) && defined(WITH_SIS_MODULE)		
				if( sis_poll_correct == 0 && qdc_poll_successfull  == 1){

					int jebus = 0;
					for( jebus = 0; jebus < V1190Count; jebus++)
						tdcmatchqdc[jebus] = -1;

					int jebus2 = 0;
					for( jebus2 = 0; jebus2 < V785Count; jebus2++ )
						adcmatchqdc[jebus2] = -1;

					qdc_poll_successfull = 0;
					//poll_tdc = -1;
					//poll_adc = -1;
					return 1;
				}// successfull event constructed qdc+tdc

#endif//if-adc-qdc-only


/* ADC MODULE + QDC MODULE */
#if defined(WITH_ADC_MODULE) && !defined(WITH_TDC_MODULE) && !defined(WITH_SIS_MODULE)		
				if( adc_poll_correct == 0 && qdc_poll_successfull  == 1){

					int jebus = 0;
					for( jebus = 0; jebus < V1190Count; jebus++)
						tdcmatchqdc[jebus] = -1;

					int jebus2 = 0;
					for( jebus2 = 0; jebus2 < V785Count; jebus2++ )
						adcmatchqdc[jebus2] = -1;

					qdc_poll_successfull = 0;
					//poll_tdc = -1;
					//poll_adc = -1;
					return 1;
				}// successfull event constructed qdc+tdc

#endif//if-adc-qdc-only

/* TDC MODULE + QDC MODULE */
#if defined(WITH_TDC_MODULE) && !defined(WITH_ADC_MODULE) && !defined(WITH_SIS_MODULE)
				if( tdc_poll_correct == 0  && qdc_poll_successfull  == 1){
					int jebus = 0;
					for( jebus = 0; jebus < V1190Count; jebus++)
						tdcmatchqdc[jebus] = -1;

					qdc_poll_successfull = 0;
					//poll_tdc = -1;
					//poll_adc = -1;
					return 1;
				}// successfull event constructed qdc+tdc
#endif//if-tdc-qdc-only
	
/* QDC MODULE ONLY */
#if !defined(WITH_ADC_MODULE) && !defined(WITH_TDC_MODULE) && !defined(WITH_SIS_MODULE)		
				if( qdc_poll_successfull  == 1){

					int jebus = 0;
					for( jebus = 0; jebus < V1190Count; jebus++)
						tdcmatchqdc[jebus] = -1;

					int jebus2 = 0;
					for( jebus2 = 0; jebus2 < V785Count; jebus2++ )
						adcmatchqdc[jebus2] = -1;

					qdc_poll_successfull = 0;
					//poll_tdc = -1;
					//poll_adc = -1;
					return 1;
				}// successfull event constructed qdc+tdc

#endif//if--qdc-only
				
		return 0;
}/*  END OF isdatareadyB */


INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
	int i;
	for(i = 0; i < count; i++ ) {
		if( isDataready(count) ){
			if(!test){
				
				//Positive Polls
				if(FRONTEND_DEBUG_MODE){
					diag_sclr[54]++;
				}
				return 1;
			}
		}
	}
	//Negative Polls
	if(FRONTEND_DEBUG_MODE){
		diag_sclr[55]++;
	}
	return 0;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

INT read_trigger_event(char *pevent, INT off)
{
	int status= 0;
	void *p;
	DWORD *pdata;
	DWORD vword;
	int tdc_module = 0;
	void *currentp = 0;
	int cntnum = 0;
#ifdef WITH_ADC_MODULE	
	int adc_cntnum = 0;
	int adc_module = 0;
	void *adc_currentp = 0;
#endif	
#ifdef WITH_SIS_MODULE
	int sis_cntnum = 0;
	int sis_chan = 0;
	int sis_module = 0;
	void *sis_currentp = 0;
#endif
	int totalconvchan = 0;	

	bk_init(pevent);
	diag_sclr[100]++;

	/*  QDC */
	status = rb_get_rp(ringbh1, &p,0);
	if (status == DB_TIMEOUT){
		//cm_yield(1);
		//ss_sleep(5);
	}//IF-DB_TIMEOUT

	DWORD *checkword = (DWORD*)p;
	bk_create(pevent,"QDC0",TID_DWORD, &pdata );
	if( isHeader(checkword) ){
		totalconvchan = getConvChannels(checkword)+2;
		vword = 0x0;
		do{
			vword = *checkword;
			*checkword = 0x0; 		//kill data
			checkword++; 			//increment ringbuffer
			*pdata++ = vword;
			cntnum++;
		}while( !isTrailer(&vword) && cntnum < totalconvchan );
	}//if-header

	diag_sclr[90] = cntnum;
	status = rb_increment_rp(ringbh1, cntnum*sizeof(DWORD));
	int current_qdc_blevel = 0; 
	rb_get_buffer_level(ringbh1,&current_qdc_blevel);
	global_qdc_buf_level += current_qdc_blevel;
	global_qdc_buf_counter++;

	bk_close(pevent,pdata);

#ifdef WITH_ADC_MODULE
	/*  ADC */
	bk_create(pevent,"ADC0",TID_DWORD,&pdata );
	for( adc_module = 0; adc_module < V785Count; adc_module++ ){
		adc_currentp = NULL;
		adc_cntnum = 0;
		status = rb_get_rp(ringbh_v785[adc_module], &adc_currentp,0 );
		if( status == DB_TIMEOUT ){
			//ss_sleep(10);
			continue;
		}
		DWORD *checkword3 = (DWORD*)adc_currentp;
		vword = *checkword3;
		if( isHeader(checkword3) ){
			//*pdata++ = adc_module+(0x1f<<27);
			*pdata++ = (0xfd<<24) + adc_module;								//Changed Made on Retiefs
			int adc_totalconvchan = getConvChannels(checkword3)+2;
			vword = 0x0;
			do{
				vword = *checkword3;
				*checkword3 = 0x0; 		//kill data
				checkword3++; 			//increment ringbuffer
				*pdata++ = vword;
				adc_cntnum++;
			}while( !isTrailer(&vword) && adc_cntnum < adc_totalconvchan );
		}//if-header
		diag_sclr[91+adc_module] = adc_cntnum;
		status = rb_increment_rp(ringbh_v785[adc_module], adc_cntnum*sizeof(DWORD));
		int current_adc_blevel = 0; 
		rb_get_buffer_level(ringbh_v785[adc_module],&current_adc_blevel);
		adc_buffer_level_counter(abs(current_adc_blevel),adc_module);
	}//For - ADC
	bk_close(pevent,pdata);
#endif

#ifdef WITH_TDC_MODULE
	/*  TDC */
	bk_create(pevent,"TDC0",TID_DWORD,&pdata );
	for( tdc_module = 0; tdc_module < V1190Count; tdc_module++ ){
		status = rb_get_rp(ringbh_1190[tdc_module], &currentp,0 );
		if( status == DB_TIMEOUT ){
			ss_sleep(10);
			continue;
		}
		DWORD *checkword2 = (DWORD*)currentp;
		int wordcounter = 0;
		vword = *checkword2;
		if( isGlobalHeader(&vword) ){
			*pdata++ = tdc_module+(0x1f<<27);
			wordcounter = 0;
			do{
				vword = *checkword2;
				*pdata++ = vword;
				*checkword2 = 0x0; 		// kill data
				checkword2++;			// increment ringbuffer
				wordcounter++;
			}while( !isGlobalTrailer(&vword) );
			status = rb_increment_rp(ringbh_1190[tdc_module], wordcounter*sizeof(DWORD));
		}//If isGLobalHEader
		else{
			cm_msg(MINFO,"read_trigger_event","first word of tdc [%d], not a header word 0x%08x ",tdc_module,vword );
			frontend_exit();
		}
		int current_tdc_blevel = 0; 
		rb_get_buffer_level(ringbh_1190[tdc_module],&current_tdc_blevel);
		tdc_buffer_level_counter(abs(current_tdc_blevel),tdc_module);
		tdc_word_level_counter( wordcounter,tdc_module );
	}//For - TDC
	bk_close(pevent,pdata);
#endif

#ifdef WITH_SIS_MODULE
	/*  SIS */
	if(FRONTEND_NO_SIS==0) 
	{

		bk_create(pevent,"SIS0",TID_DWORD,&pdata );
		for(sis_module=0; sis_module < NUM_OF_SIS_MODS;sis_module++){

			*pdata++ = (0x1f<<24) + sis_module;
			
			for( sis_chan = 0; sis_chan < SISCount; sis_chan++ ){
				sis_currentp = NULL;
				status = rb_get_rp(ringbh_sis3302[sis_module][sis_chan], &sis_currentp,0 );
#ifdef DEBUG_SIS_1
				cm_msg(MINFO,"read_trigger_event","SIS[%d], Chan[%d], ringb [%d] ",sis_module,sis_chan,ringbh_sis3302[sis_module][sis_chan]);
#endif	
		
				if( status == DB_TIMEOUT ){
					//ss_sleep(10);
					
#ifdef DEBUG_SIS_1					
					cm_msg(MINFO,"read_trigger_event","SIS[%d],Chan[%d] Ringbuffer get read pointer DB Timeout", sis_module,sis_chan);
#endif					
					//printf("read_trigger_Event DB_TIMEOUT \n");
					continue;
				}
		
				DWORD *checkword4 = (DWORD*)sis_currentp;
				vword = *checkword4;
				sis_cntnum = 0;
				if( isSIS3302Header(&vword,sis_chan) ){
					do{
						vword = *checkword4;
						*pdata++ = vword;
						*checkword4 = 0x0; 		//kill data
						checkword4++; 			//increment ringbuffer
						sis_cntnum++;
#ifdef DEBUG_SIS_1
						cm_msg(MINFO,"read_trigger_event","SIS[%d], Chan[%d]  insert vword : 0x%x, sis_cntnum[%d] ",sis_module,sis_chan,vword,sis_cntnum);	
#endif
						//		}while( !isTrailer(&vword) && sis_cntnum < event_length_lwords )
				}while( !isSIS3302Trailer(&vword) );

				if( sis_cntnum > event_length_lwords)
					cm_msg(MINFO,"read_trigger_event"," stored SIS event longer than event length lwords, cnt[%d]",sis_cntnum);
				//	printf("Event Uploaded\n");
				//if( isSIS3302Trailer(&vword) == 1 ){
				status = rb_increment_rp(ringbh_sis3302[sis_module][sis_chan], sis_cntnum*sizeof(DWORD));
#ifdef DEBUG_SIS_1
				cm_msg(MINFO,"read_trigger_event","SIS[%d], Chan[%d],  Trailler word found readtiggerevent",sis_module,sis_chan);
				cm_msg(MINFO,"read_trigger_event","incrementing rp with sis_cntnum %d",sis_cntnum,sis_chan);
#endif
				//	}
			}//if-header
			int current_sis_chan_blevel = 0; 
			rb_get_buffer_level(ringbh_sis3302[sis_module][sis_chan],&current_sis_chan_blevel);
			sis_buffer_level_counter(abs(current_sis_chan_blevel),sis_module,sis_chan);

		}//For-ADC-CHANNEL-LOOP
	}//For-ADC-MODULE-LOOP
	bk_close(pevent,pdata);
}//IF-FRONTEND_NO_SIS
#endif

	return bk_size(pevent);
}

INT read_scaler_event_dma(char *pevent, INT off)
{
	int status= 0;
	
	void *p;
	
	DWORD *pdata;
	DWORD *checkword = NULL;
	
	DWORD vword;
	int sclr_mod  = 0;
	int j_cnt=0;

	INT scaler_temp[28];
	memset(scaler_temp,0,28*sizeof(INT));
	

	int i_cnt = 0;
	bk_init(pevent);
	
	
#ifdef WITH_V830_MODULE	
	for( j_cnt = 0; j_cnt < NUM_OF_SCR_MODS; j_cnt++){
		if( j_cnt == 0){
			SCLR_CHANNELS[j_cnt] = M_CALLOC(MAXCHANNELS1,sizeof(INT));
			memset(SCLR_CHANNELS[j_cnt],0,MAXCHANNELS1*sizeof(INT));
		}else if(j_cnt == 1){
			SCLR_CHANNELS[j_cnt] = M_CALLOC(MAXCHANNELS2,sizeof(INT));
			memset(SCLR_CHANNELS[j_cnt],0,MAXCHANNELS2*sizeof(INT));
		}//if-else
	}//for
	
//	bk_create(pevent, "SCLR", TID_DWORD, &pdata);

	int trigger_incr = 0;
	int trigger_cnt = 0;
	//memset(sclr_event_match,0,2*sizeof(INT));
	int event_sclr_counter = 0;
	for( sclr_mod = 0; sclr_mod < NUM_OF_SCR_MODS; sclr_mod++){

		/*  Get ringbuffer readpointer */
		status = rb_get_rp(ringbh_v830[sclr_mod], &p, 0);
		if (status == DB_TIMEOUT){
			continue;
		}//IF-DB_TIMEOUT

		int current_blevel = 0; 
		rb_get_buffer_level(ringbh_v830[sclr_mod],&current_blevel);

		int Nw = current_blevel/sizeof(DWORD); // Number of Words in buffer
		trigger_incr = 0;
		trigger_cnt = 0;

		if( current_blevel > 0 ) { //Check Buffer Level
			
			//if( sclr_mod == 0 ){
			//	diag_sclr[190] = current_blevel;
				//diag_sclr[192] = Nw;
			//}
			//else{
			//	diag_sclr[191] = current_blevel;
				//diag_sclr[193] = Nw;
			//}

			sclr_buffer_level_counter( current_blevel, sclr_mod);

			int wordcounter = 0;
			checkword = (DWORD*)p;
			//int hh = 0;
			do{
				vword = *checkword;
				//printf("vword: 0x%X \n",vword);
				*checkword = 0x0;

				//*checkword++; //increment buffer
				checkword++; //increment buffer

				wordcounter++; //total counter
				i_cnt = 0;
				if( isSCLRHeader(&vword) == 1 ){
					int number_words = getSCLRWordCount(&vword); //Get Number of Words for this Event
					event_sclr_counter++;	
					//sclr_event_match[sclr_mod] = getSCLRTriggerNum(&vword);
					int current_trig_num  = getSCLRTriggerNum(&vword);
					//diag_sclr[192+sclr_mod] = sclr_event_match[sclr_mod];
					//*pdata++ = sclr_mod+(0xbeef<<27);
					if( global_sclr_prev_trigcnt[sclr_mod] > 0 && current_trig_num > 0){
						trigger_incr = current_trig_num - global_sclr_prev_trigcnt[sclr_mod];
						trigger_cnt++;
						if( trigger_incr > 1 ){
								cm_msg(MINFO,"k600fevme","SCLR[%d], blevel [%d] , Current trig cnt [%d], Prev Trig cnt [%d], Trigger Increment greater than 1 \n",sclr_mod,current_blevel,current_trig_num,global_sclr_prev_trigcnt[sclr_mod]);
						}
					}else if(current_trig_num > 0){
						trigger_incr = current_trig_num;
						trigger_cnt++;
					}

					global_sclr_prev_trigcnt[sclr_mod] = current_trig_num;
					do{
						vword = *checkword;
						//*pdata++ = vword -	prevscalercount[sclr_mod][i_cnt];
						if( vword != 0x0 ){
							SCLR_CHANNELS[sclr_mod][i_cnt] = vword - prevscalercount[sclr_mod][i_cnt];
							//scaler_temp[hh++] = vword -	prevscalercount[sclr_mod][i_cnt];
						}else{
							SCLR_CHANNELS[sclr_mod][i_cnt] = 0;
						}

						prevscalercount[sclr_mod][i_cnt] = vword;
						*checkword = 0x0;

						//*checkword++;
						checkword++;

						wordcounter = wordcounter + 1;
						i_cnt = i_cnt + 1;

						if(sclr_mod == 0 && i_cnt > MAXCHANNELS1){ 
								break;
						}else if(sclr_mod == 1 && i_cnt > MAXCHANNELS2){
							   break;
						}
					}while( i_cnt < number_words );

					//ONLY FOR TESTING 
					//*pdata++ = (0xfd<<24) + sclr_mod;	

					//break; // Only 1 SCLR 'event' per second  /* :TRICKY:04/30/2013 09:55:36 AM:LCP:  */
				}//if-Header-Word
			}while( wordcounter < Nw ); // loop over words in current buffer

			status = rb_increment_rp(ringbh_v830[sclr_mod], wordcounter*sizeof(DWORD));
			sclr_trigger_counter( trigger_cnt,sclr_mod);		
		}//If-bufferlevel-check
	}/*  FOR-loop */


	if(event_sclr_counter > 0 ) {	
		bk_create(pevent, "SCLR", TID_DWORD, &pdata);
		int jj = 0;
		for(jj=0;jj<NUM_OF_SCR_MODS;jj++){
			int r_count = 0;

			if( jj == 0 ){
				r_count = MAXCHANNELS1;
			}else if( jj == 1 ){
				r_count = MAXCHANNELS2;
			}
			int k=0;
			for(k=0;k<r_count;k++){
				*pdata++ = SCLR_CHANNELS[jj][k];;
			}

			//M_FREE(SCLR_CHANNELS[jj]);
		}

		bk_close(pevent,pdata);
	}

	int jb = 0;
	for(jb=0;jb<NUM_OF_SCR_MODS;jb++){
			M_FREE(SCLR_CHANNELS[jb]);
	}
#endif

	return bk_size(pevent);
}
INT read_scaler_event_diag(char *pevent, INT off)
{
	int status= 0;
	
	void *p;
	
	DWORD *pdata;
	DWORD *checkword = NULL;
	
	DWORD vword;
	int sclr_mod  = 0;
	int j_cnt=0;

	

	int i_cnt = 0;
	bk_init(pevent);
	
#ifdef FE_WITH_DEBUG_SCALARS

	bk_create(pevent, "SCLD", TID_INT, &pdata);

	/*  gefanuc (new) */
	//do_temp_sensors();

	do_tdc_buf_diag();	
	reset_bufferlevelcounter();
	do_qdc_buf_diag();
	reset_qdc_buf_counters();
   	do_adc_buf_diag();
	reset_adc_bufferlevelcounter();
	do_sis_buf_diag();
	reset_sis_bufferlevelcounter();
	do_sis_Nw_diag();
	reset_sis_Nw_counter();
	do_tdc_word_diag();
	reset_wordcounter();
	do_tdc_average_diag();
	reset_tdcaveragecounter();
	do_qdc_ave_event();
	reset_qdc_ave_event();
		
	do_tdc_skip_stats();
	reset_tdc_skip_stats();
	do_tdc_ea_stats();
	reset_tdc_ea_stats();

	do_sclr_buf_diag();
	reset_sclr_bufferlevelcounter();
	
	do_sclr_trigger_diag();
	reset_sclr_trig_counter();

	do_tdc_zero_stats();

	//qdc zero counting
	diag_sclr[188] = global_qdc_evt_zero;
	global_qdc_evt_zero = 0;

#if 0	
	if(FRONTEND_DEBUG_MODE_TIMER){
		/*  qdc readout time */
		if( gef_time_counter != 0 ){
			//diag_sclr[128] = gef_time_sum/gef_time_counter;
			diag_sclr[128] = gef_time_sum;
		}else{
			diag_sclr[128] = 0;
		}

		/*  tdc readout time */
		if( gef_time_counter1 != 0 ){
			diag_sclr[130] = gef_time_sum1/gef_time_counter1;
		}else{
			diag_sclr[130] = 0;
		}

		gef_tdc_timer_diag();
		gef_tdc_timer_reset();

		gef_sclr_timer_diag();
		gef_sclr_timer_reset();


	}//FRONTEND_DEBUG_MODE_TIMER
#endif 

	/*  transfer diag_sclr to midas bank then zero diag_sclr */
	int k = 0;
	for( k =0; k < NUMBER_SCALARS;k++){
		*pdata++ = diag_sclr[k];
		diag_sclr[k] = 0;
	}

	bk_close(pevent,pdata);
#endif

	return bk_size(pevent);
}

/* #####   FUNCTION DEFINITIONS - Misc. Functions -  ##################### */

